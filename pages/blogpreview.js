import Link from 'next/link';
import ErrorPage from 'next/error'
import React from "react";
import {connect} from "react-redux";
import Layout from '../components/Layout';
import PostCarousel from "../components/PostCarousel";
import {fetchPreviewBlog} from '../store/actions/actions';
import {getCategoryNames} from '../store/functions';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

class SinglePost extends React.Component {
    render() {
        return (
            <div className={"single-card large wow fadeIn fadeUp delay-" + this.props.delay + this.props.style}>
                <Link href={"/blog/" + this.props.id}>
                    <div className="title-content">
                        <strong className="overline">{this.props.category}</strong>
                        <h3 className="h3">{this.props.title}</h3>
                    </div>
                </Link>
                <img className="background" src={this.props.background} alt="Card"/>
                <Link href={"/blog/" + this.props.id}><div className="link arrow"><i className="fas fa-chevron-right"></i></div></Link>
            </div>
        );
    }
}

class RelatedPosts extends React.Component {
    render() {
        const {blog} = this.props;

        if (!blog) {
            return (
                <div className="related-posts content-section background-grey no-min-height ">
                    <div className="container">
                        <h3 className="container-title">Other articles not available</h3>
                    </div>
                </div>
            )
        }

        let content = blog.map((item, key) => {
            return (
                <SinglePost id={item.slug} style={' light'} delay={'25'} title={item.title} category={getCategoryNames(item.categories)} background={item.image}/>
            )
        })

        return (
            <div className="related-posts content-section background-grey no-min-height ">
                <div className="container">
                    <h3 className="container-title">Other articles you might like</h3>
                    <div className="card-container resources">
                        {content}
                    </div>
                </div>
            </div>
        );
    }
}

class BlogDetail extends React.Component {

    static async getInitialProps({req, reduxStore, pathname, params, query}) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here dfgd

        // get preview blog
        let postData = {
            blog_id: query.id,
            secret: query.secret
        }
        await reduxStore.dispatch( fetchPreviewBlog (postData) );

        return {};
    }

    componentDidMount() {
        new WOW(
            {
                boxClass:     'wow',      // default
                animateClass: 'animated', // default
                offset:       75,          // default
                mobile:       true,       // default
                live:         true        // default
            }
        ).init();
    }
    render() {

        const { blog } = this.props;

        if (typeof (blog.title) === 'undefined') {
            return <ErrorPage statusCode={404} />
        }

        let topics = blog.categories.map((item, key) => {
            return (
                <div className="topic">{item.name}</div>
            )
        })

        return (
            <Layout>
                <div className="page detailed-post">
                    <div className="container">
                        <div className="wow fadeIn fadeUp">
                            <PostCarousel background={blog.image} />
                        </div>
                        <div className="title-container ">
                            <div className="small-container wow fadeIn fadeUp">
                                <h3>{ blog.title }</h3>
                                <div className="post-date wow fadeIn fadeUp delay-50">{blog.publish_date}</div>
                            </div>
                        </div>
                        <div className="body-content wow fadeIn" dangerouslySetInnerHTML={{__html: blog.description}}>
                        </div>
                        <div className="article-like wow fadeIn">
                            <strong>Did you like this article?</strong>
                            <button className="like"></button>
                        </div>
                    </div>
                    <div className="topics wow fadeIn">
                        <div className="container">
                            <div className="topic title">Topics</div>
                            { topics }
                        </div>
                    </div>
                    <div className="wow fadeIn">
                        <RelatedPosts blog={blog.related_blog}/>
                    </div>
                </div>
            </Layout>
        );
    }
}

const mapStateToProps = state => ({
    // Only map state needed in this component,
    blog: state.blog.blog,
})

// add comments functionality later
const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(BlogDetail)