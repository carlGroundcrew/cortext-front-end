import Layout from '../components/Layout';
import SignupBanner from '../components/SignupBanner';
import { Controller, Scene } from 'react-scrollmagic';
import { Tween, Timeline } from 'react-gsap';


const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Science extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
      <Layout page="science">
          <div className="page science animating-articles">
            <div className="content-section no-min-height background-grey with-profile-image">
              <div className="container">
                  <div className="title-container"> 
                    <h3 className="h2 wow fadeIn">Our content is backed by decades of science</h3>
        
                  </div>
                  <div className="p tagline wow fadeIn">
                      <div className='inner'>
                        One of our key values is to maintain a strong scientific integrity in everything we do.
                        Explore some of the fundamental concepts that underpin the Methodology.
                      </div>
                  </div>
              </div>
            </div>
            <div className="content-section">
              <Controller>

                {/* Circle to Eye animation */}

                <Scene triggerHook="0.2"
                  duration={2}
                  pin={false}
                  reverse={true}
                  indicators={false}
                  offset={120} > 
                  {(progress, event) => {
                    return (
                      <article className="fadeUp wow fadeIn no-padding-bottom no-padding-top-mobile"> 
                        <div className="container-st">
                          <div className="half"> 
                            <div className="image-container animation-container-1 left">
                              <Tween duration={3}
                                  from={{borderRadius:'100%', rotation:'0'}}
                                  to={{borderRadius:'55% 46% 52% 48%', rotation:'160'}} 
                                  paused
                                  playState={
                                    (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                    (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                    <div className="circle one x-large with-gradient secondary-gradient gradient-200"></div> 
                             </Tween>
                             <Tween duration={2}
                                from={{borderRadius:'100%', background:'#fff'}}
                                to={{scaleX:'1.5', scaleY:'1.5'}} 
                                paused
                                playState={
                                  (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                  (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                  <div className="circle two x-large"></div> 
                              </Tween>
                            </div>
                          </div>
                          <div className="half content-area">
                            <div className="inner">
                              <h3 className="h2 title">Military Application of Performance-Enhancement Psychology</h3>
                              <div className="excerpt">
                                (Zinsser et al.,2004).<br/>“Performance enhancement is the deliberate cultivation of an effective perspective on achievement and the systematic use of effective cognitive skills. A soldier can maximize performance by mastering thinking habits and emotional and physical states. These training methods, derived from applied sport psychology used in training professional and Olympic athletes, are also applicable in other human performance contexts...
                                <br/>
                                <a target="_blank" href="http://www.au.af.mil/au/awc/awcgate/milreview/zinsser.pdf"><button>Read more</button></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </article>
                    );
                  }}
                </Scene> 
                  
                {/* Pendulum animation */}

                <Scene triggerHook="0.2"
                    duration={100}
                    pin={false}
                    reverse={true}
                    indicators={false}
                    offset={50} > 
                    {(progress, event) => { 
                      return (
                      <article className="rhs wow fadeIn fadeUp no-padding-top-mobile no-padding-bottom-mobile">
                        <div className="container-st">
                         <div className="half content-area with-margin mobile-hide">
                            <div className="inner">
                              <h3 className="h2 title">A Psychological Success Cycle And Goal Setting: Goals, Performance, And Attitudes</h3>
                              <div className="excerpt">
                                (Hall & Foster., 2017)<br />“Setting goals initiates a success cycle, engaging goals increase your efforts, efforts increase the likelihood of success, success improves self esteem, self efficacy and desire for involvement which leads to desire to set bigger goals”             
                                <br/>
                                <a target="_blank" href="https://journals.aom.org/doi/10.5465/255401"><button>Read more</button></a>
                              </div>
                            </div>
                          </div>
                          <div className="half mobile-right padding-left-large">
                            <div className="image-container extended pendulum">
                                <Tween duration={1} delay={0}
                                  from={{ left: '80%', top: '35%', rotation:'160', transformOrigin: '120%'}}
                                  to={{ backgroundPosition:'44% 50%', left: '51%', top: '35%', rotation:'270', transformOrigin: '120%'}} 
                                  paused
                                  playState={
                                    (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                    (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                    <div className="circle pendulum-1 medium with-gradient with-rotation secondary-gradient secondary-three mobile-small"></div> 
                                </Tween>
                                <Tween duration={.7} delay={1}
                                  from={{ left: '21%', top: '35%', rotation:'270', transformOrigin: '120%'}}
                                  to={{ backgroundPosition:'44% 50%', left: '21%', top: '35%', rotation:'275', transformOrigin: '120%'}} 
                                  paused
                                  playState={
                                    (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                    (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                    <div className="circle pendulum-2 medium with-gradient with-rotation secondary-gradient secondary-three mobile-small"></div> 
                                </Tween>
                                <Tween duration={.7} delay={1.3}
                                  from={{ left: '-10%', top: '35%', rotation:'270', transformOrigin: '120%'}}
                                  to={{ backgroundPosition:'44% 50%', left: '-10%', top: '35%', rotation:'275', transformOrigin: '120%'}} 
                                  paused
                                  playState={
                                    (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                    (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                    <div className="circle pendulum-3 medium with-gradient with-rotation secondary-gradient secondary-three mobile-small"></div> 
                                </Tween>
                            </div>
                          </div>
                          <div className="half content-area with-margin mobile-showing">
                            <div className="inner">
                              <h3 className="h2 title">A Psychological Success Cycle And Goal Setting: Goals, Performance, And Attitudes</h3>
                              <div className="excerpt">
                                (Hall & Foster., 2017)<br />“Setting goals initiates a success cycle, engaging goals increase your efforts, efforts increase the likelihood of success, success improves self esteem, self efficacy and desire for involvement which leads to desire to set bigger goals”             
                                <br/>
                                <a target="_blank" href="https://journals.aom.org/doi/10.5465/255401"><button>Read more</button></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </article>
                      );
                    }}
                </Scene>

                  {/* 2 Circle rotation animation */}

                <Scene triggerHook="0.2"
                  duration={200}
                  pin={false}
                  reverse={true}
                  indicators={false}
                  offset={0} > 
                  {(progress, event) => {
                    return (
                      <article className="lhs wow fadeIn fadeUp no-padding-top-mobile">
                            <div className="container-st inner-content">
                              <div className="half centered"> 
                                <div className="image-container"></div>
                              </div>
                              <div className="half content-area  with-background">
                              <div className="inner">
                                <h3 className="h2 title">Psychological skills training as a way to enhance an athlete’s performance in high-intensity sports</h3>
                                <div className="excerpt">
                                  (Birrer & Morgan., 2010)<br/>
                                  “In today's professional and semi‐professional sports, the thin line between winning and losing is becoming progressively thinner. At the Beijing Olympic Games in 2008, the difference between first and fourth places in the men's rowing events averaged 1.34%, with the equivalent for women being a mere 1.03%. This increasing performance density creates massive pressure. Thus, it is not surprising that in recent years, the importance of psychological skills training (PST) has been recognized, and the number of athletes using psychological training strategies has increased.”
                                </div>
                                <a target="_blank" href="https://onlinelibrary.wiley.com/doi/full/10.1111/j.1600-0838.2010.01188.x"><button>Read more</button></a>
                              </div>
                            </div>
                            </div>
                            <div className="background-container">
                              <Tween duration={1.5}
                                from={{ rotation: '210deg'}}
                                to={{ rotation: '272deg'}}
                                paused
                                playState={
                                  (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                  (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                <div className="background">
                                  <div className="image-container">
                                      <Tween delay={1.3}
                                        from={{ backgroundPosition:'51% 50%'}}
                                        to={{ backgroundPosition:'46% 50%'}} paused
                                        playState={
                                          (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                          (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} > 
                                        <div className="with-gradient secondary-gradient secondary-four circle medium will-rotate">
                                          <Tween delay={1.3} duration={0.1}
                                            from={{ opacity:0}}
                                            to={{ opacity:'1'}} 
                                            paused playState={ (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} > 
                                            <span className="percentage">1.34</span>
                                          </Tween>
                                        </div>
                                      </Tween>
                                    </div>
                                  </div>
                                </Tween>
                              </div>
                          </article>
                          );
                        }}
                </Scene>

                {/* 2 into 1 increase animation */}

                <Scene triggerHook="0.2"
                    duration={100}
                    pin={false}
                    reverse={true}
                    indicators={false}
                    offset={50} > 
                    {(progress, event) => {
                      return (
                      <article className="rhs wow fadeIn fadeUp no-padding-top-mobile no-padding-bottom-mobile">
                        <div className="container-st">
                         <div className="half content-area mobile-hide">
                          <div className="inner">
                            <h3 className="h2 title">The relationship between social identity and test anxiety in university students</h3>
                            <div className="excerpt">
                                (Zwettler et al., 2018)<br/>
                                “Membership in groups is a significant feature of human existence. Belongingness to groups is not only an external factor that influences our behavior, but it also affects our experiences and cognition. Identification with a group can provide an individual with meaning, stability, and purpose, which supports their mental well-being (Haslam et al., 2009). This means that a person’s self-concept often depends on the state of the groups that define the self (i.e. in-groups).”
                                <br />
                                <a target="_blank" href="https://journals.sagepub.com/doi/full/10.1177/2055102918785415"><button>Read more</button></a>
                            </div>
                          </div>
                          </div>
                          <div className="half margin-top-mobile padding-left-mobile">
                            <div className="image-container">
                              <timeline>
                                <Tween duration={1}
                                  from={{ left: '0%', top: '0%', backgroundPosition:'50% 50%' }}
                                  to={{ left: '30%', top: '30%', backgroundPosition:'50% 50%' }}
                                  paused
                                  playState={
                                    (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                    (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                    <div className="circle small with-gradient no-shadow"></div> 
                                </Tween>
                                <Tween duration={1}
                                  to={{ delay:'0.5', backgroundPosition:'94% 50%', left: '45%', top: '50%', scale:'1.1', rotation:'270', borderRadius: '50% 50% 50% 50%' }}
                                  from={{backgroundPosition:'90% 50%', left: '45%', top: '50%', scale:'1', rotation:'270', borderRadius: '50% 35% 50% 50%' }}
                                  paused
                                  playState={
                                    (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                    (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                    <div className="circle x-large with-gradient gradient-100 secondary-gradient secondary-two"></div> 
                                </Tween>
                              </timeline>
                             </div>  
                          </div>
                          <div className="half content-area mobile-showing margin-top-mobile">
                          <div className="inner">
                            <h3 className="h2 title">The relationship between social identity and test anxiety in university students</h3>
                            <div className="excerpt">
                                (Zwettler et al., 2018)<br/>
                                “Membership in groups is a significant feature of human existence. Belongingness to groups is not only an external factor that influences our behavior, but it also affects our experiences and cognition. Identification with a group can provide an individual with meaning, stability, and purpose, which supports their mental well-being (Haslam et al., 2009). This means that a person’s self-concept often depends on the state of the groups that define the self (i.e. in-groups).”
                                <br />
                                <a target="_blank" href="https://journals.sagepub.com/doi/full/10.1177/2055102918785415"><button>Read more</button></a>
                            </div>
                          </div>
                          </div>
                        </div>
                      </article>
                      );
                    }}
                </Scene>

                  {/* 1 to 3 animation circles */}

                  <Scene triggerHook="0.2"
                    duration={100}
                    pin={false}
                    reverse={true}
                    indicators={false}
                    offset={50} > 
                    {(progress, event) => {
                      return (
                      <article className="lhs wow fadeIn fadeUp no-padding-top-mobile ">
                        <div className="container-st">
                          <div className="half padding-right-mobile ">
                            <div className="image-container left">
                                <Tween duration={1}
                                  from={{ left: '0%', top: '35%' }}
                                  to={{ left: '0%', top: '35%' }}
                                  paused
                                  playState={
                                    (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                    (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                    <div className="circle with-gradient no-shadow secondary-gradient secondary-three"></div> 
                                </Tween>
                                <Tween duration={0.7} delay={0.7}
                                  from={{ backgroundPosition:'92% 50%', left: '0%', top: '35%', scale:1 }}
                                  to={{ backgroundPosition:'96% 50%', left: '40%', top: '35%', scale:1.2 }}
                                  paused
                                  playState={
                                    (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                    (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                    <div className="circle with-gradient no-shadow secondary-gradient secondary-four no-shadow"></div> 
                                </Tween>
                                <Tween duration={0.7} 
                                  from={{ backgroundPosition:'92% 50%', left: '0%', top: '35%', scale:1 }}
                                  to={{ backgroundPosition:'98% 50%', left: '85%', top: '35%', scale:1.4}}
                                  paused

                                  playState={
                                    (event.type === 'enter' && event.scrollDirection === 'FORWARD') ? 'play' : 
                                    (event.type === 'enter' && event.scrollDirection === 'REVERSE') ? 'reverse' : null} >        
                                    <div className="circle with-gradient no-shadow secondary-gradient secondary-four no-shadow"></div> 
                                </Tween>
                            </div>
                          </div>
                          <div className="half content-area">
                            <div className="inner">
                              <h3 className="h2 title">Mindfulness to Enhance Athletic Performance</h3>
                              <div className="excerpt">
                                (Birrer & Morgan., 2012)<br/>
                                “Athletes with a higher degree in mindfulness practice and dispositional mindfulness will enhance the level of several required psychological skills including attention, experiential acceptance, decision making, self-regulation, negative emotion regulation, and behavioural flexibility.”
                              </div>
                              <a target="_blank" href="https://link.springer.com/article/10.1007/s12671-012-0109-2"><button>Read more</button></a>
                            </div>
                          </div>
                        </div>
                      </article>
                      );
                    }}
                  </Scene>

                  <div className="section" />
                </Controller>
            </div>
            <SignupBanner title={'Subscribe for our exclusive pre-launch offer, 20% off for life'} background={'/static/img/banners/banner-6.jpg'}/>
          </div>
      </Layout>
    );
  }
}

export default Science