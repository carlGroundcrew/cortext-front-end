import Layout from '../components/Layout';
import BlogTabs from '../components/BlogTabs';
import BlogCarousel from '../components/BlogCarousel';
import SignupBanner from '../components/SignupBanner';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

import {fetchBlogs} from '../store/actions/actions';

class Blog extends React.Component {

    static async getInitialProps ({ req, reduxStore, pathname, params, query }) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here

        // get blogs
        await reduxStore.dispatch( fetchBlogs ({}) );

        return { };
    }

  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="blog">
      <div className="page" >
      	<div id="featured" className="content-section slider-extended no-padding-bottom">
	  	 	<div className="carousel-container">
	  	 		<div className="wow fadeIn fadeLeft">
	  	 			<BlogCarousel />
	  	 		</div>
		   	</div>
        <div className="content-section no-padding-bottom mobile-no-padding-top">
          <BlogTabs />
        </div>
      </div>
      <SignupBanner title={'Subscribe today and receive Our 20% discount. For life.'} background={'/static/img/banners/banner-6.jpg'}/>
		</div>
    </Layout>
    );
  }
}


export default Blog