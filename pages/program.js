import Layout from '../components/Layout';
import PricingPanels from '../components/pricing/Panels';
import Signup from '../components/Signup'; 
import Link from 'next/link'; 

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Pricing extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="program">
      <div className="page">
      	<div className="content-section no-min-height text-banner padding-right-mobile">
      	 	<div className="container">
      	 		<div className="container-md left-align">
      	 			<h2 className="h2 title double-height"><span className="wow fadeIn fadeUp">We have designed 2 programs aimed to give you the tools, perspectives and practices to reach your goals.</span></h2>
				    </div> 
    	   	</div>
    	  </div>
        <div className="content-section background-grey no-min-height">
        	<div className="container">
            <PricingPanels />
      	  </div>
          <div className="content-section no-min-height with-tagline">
            <div className="container">
              <div className="half full-tablet">
                <h3>Interested in a Corporate Package?</h3>
              </div>
              <div className="half full-tablet">
                <div className="signup-form dark">
                  <Signup />
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </Layout> 
    );
  }
}

export default Pricing