import Layout from '../components/Layout';
import PricingCards from '../components/pricing/Panels';
import Link from 'next/link'; 

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Blog extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout title='About Cortex' page="about">
      <div className="page">
        <div className="content-section no-min-height background-grey with-profile-image">
            <div className="container">
                <div className="title-container"> 
                  <h3 className="h2 wow fadeIn">Meet Leanne.</h3>
                  <div className="subtitle wow fadeIn">
                    Human performance expert and <br/> 
                    the voice of Cortex method. 
                  </div>
                </div>
              <img className="profile-image wow fadeIn delay-50" src="/static/img/about/profile.png" alt="Profile Image"  />
            </div>
        </div>
        <div className="content-section no-min-height user-content">
            <div className="container">
              <div className="container-rg left-align wow fadeIn fadeUp delay-100">
                  <h2>Her background</h2>
                  <br/>
                  In her early twenties, midway through her first degree (and a little lost), Leanne found her purpose in Psychology. An ‘ah ha’ moment mid ‘intro to psych’ lecture that went something like “everyone should know this. Everyone should know how they operate and what actually helps to improve themselves”
                  <br/><br/>  
                  This developed into an obsession to apply psychology.
                  <br/><br/>  
                  Leanne then gained her Masters in Performance Psychology from the University of Edinburgh and studied Business at London School of Business and Finance. She then went on to study Performance Coaching at the London Coaching Academy.
                  <br/><br/>  
                  Her career has taken her to Performance Psychology roles within Hedge Funds and Trading Firms, where the difference between success and failure often comes down to a clear mind and a split second decision. 
                  <br/><br/>  
                  The goal - developing world class performers.
                  <br/><br/><br/>
                  <h2>Skip forward 5 years</h2>
                  <br/>
                  Leanne connects with friends Drew and Brodie Haupt - business owners and entrepreneurs. Inspired by their personal mission for optimal performance. 
                  <br/><br/>
                  Together they saw the opportunity to create a performance psychology program for everyday people. Thats when Cortex Method was born.
                  <br/><br/><br/>
                  <h2>Introducing Cortex Method</h2>
                  <br/>
                  A methodology developed to give people the tools, perspectives and practices to reach and then challenge their capabilities.
                  <br/><br/>
                  <strong>The aim</strong>
                  <br/> 
                  • To create a society made up of empowered, highly fulfilled, challenged and capable people.
                  <br/>
                  • To equip people with the psychological capital to be able to adapt, deliver and ultimately perform.
                  <br/>
                  • To give people an edge that differentiates high performers from those who fall by the wayside.
              </div>
            </div>
        </div>
      	<div className="content-section no-min-height no-padding-bottom background-purple">
      	 	<div className="container">
      	 		<div className="title-with-link wow fadeIn">
      	 			<h2 className="h1 title"><span className="wow fadeIn fadeUp">Our Plans</span></h2>
              <Link href="program"><button className="mobile-white">Read More</button></Link>
				    </div>
    	   	</div>
    	  </div>
        <div className="content-section text-banner no-min-height no-padding-top background-purple">
        	<div className="container">
            <PricingCards />
      	  </div>
        </div>
      </div>
    </Layout> 
    );
  }
}

export default Blog