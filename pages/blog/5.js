import React from 'react'
import Link from 'next/link'
import Layout from '../../components/Layout';
import PostCarousel from "../../components/PostCarousel";
import { getPost } from '../../api/posts';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class SinglePost extends React.Component {
  render() {
    return (
    <div className={"single-card large wow fadeIn fadeUp delay-" + this.props.delay + this.props.style}>
          <Link href={"/blog/" + this.props.id}>
            <div className="title-content">
              <strong className="overline">{this.props.category}</strong>
              <h3 className="h3">{this.props.title}</h3>
            </div>
          </Link>
          <img className="background" src={this.props.background} alt="Card"/>
          <Link href={"/blog/" + this.props.id}><div className="link arrow"><i className="fas fa-chevron-right"></i></div></Link>
    </div>
   );
  }
}

class RelatedPosts extends React.Component {
  render() {
    return (
    <div className="related-posts content-section background-grey no-min-height "> 
        <div className="container">
          <h3 className="container-title">Other articles you might like</h3>
          <div className="card-container resources">
            <SinglePost id={'6'} style={' light'} delay={'25'} title={'Building Psychological Momentum'} category={'Mindfulness '} background={'/static/img/blog/blog-6.jpg'}/>
            <SinglePost id={'1'} style={' light'} delay={'50'} title={'Mental Health Now An Issue In Australian Sport'} category={'Performance'} background={'/static/img/blog/blog-1.jpg'}/>
            <SinglePost id={'2'} style={' light'} delay={'100'} title={'The Trap of Feeling “Good Enough”'} category={'Performance'} background={'/static/img/blog/blog-2.jpg'}/>
          </div>
        </div>
    </div>
   );
  } 
}

class RelatedPost extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout>
      <div className="page detailed-post">
      	<div className="container">
          <div className="wow fadeIn fadeUp">
      		  <PostCarousel background={'/static/img/blog/blog-5.jpg'} />
          </div>
	      	<div className="title-container ">
    	    	<div className="small-container wow fadeIn fadeUp">
    	    		<h3>The Trap of Feeling “Good Enough”</h3>
    	    		<div className="post-date wow fadeIn fadeUp delay-50">26 May 2018</div>
    	    	</div>
      		</div>
      		<div className="body-content wow fadeIn">
      			Commentary: Sarah Pavan, Canadian beach volleyball Olympian explains from an athletes perspective how complacency becomes a trap and why it's a death warrant for performance.
            <br/><br/> 
            Performance means the PROCESS of achievement, not the achievement alone. To truly be high performance involves relentless and continual improvement of yourself, your approach and your results, regardless of whether you're best in the world or unranked.
            Otherwise, while we are congratulating ourselves, our competitors are training, planning and preparing.
            <br/><br/> 
            Read more at <a href="http://sarahpavan.com/trap-feeling-good-enough/">here</a>
            <br/><br/> 
            <i>Pavan, S. (2017, March 2). The Trap of Feeling "Good Enough": Complacency in Athletes - Sarah Pavan. Retrieved from http://sarahpavan.com/trap-feeling-good-enough/</i>
      		</div>
      		<div className="article-like wow fadeIn">
      			<strong>Did you like this article?</strong>
      			<button className="like"></button>
      		</div>
      	</div>
      	<div className="topics wow fadeIn">
      		<div className="container">
      			<div className="topic title">Topics</div>
      			<div className="topic">Performance</div>	
      			<div className="topic">Achievement</div>	
      			<div className="topic">Process</div>	
      			<div className="topic">Results</div>	
      			<div className="topic">Perspective</div>	
      		</div>
      	</div>
        <div className="">
          <RelatedPosts />
        </div>
      </div>
    </Layout>
 
    );
  }
}

export default RelatedPost