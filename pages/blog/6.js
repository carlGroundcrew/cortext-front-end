import React from 'react'
import Link from 'next/link'
import Layout from '../../components/Layout';
import PostCarousel from "../../components/PostCarousel";
import { getPost } from '../../api/posts';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class SinglePost extends React.Component {
  render() {
    return (
    <div className={"single-card large wow fadeIn fadeUp delay-" + this.props.delay + this.props.style}>
          <Link href={"/blog/" + this.props.id}>
            <div className="title-content">
              <strong className="overline">{this.props.category}</strong>
              <h3 className="h3">{this.props.title}</h3>
            </div>
          </Link>
          <img className="background" src={this.props.background} alt="Card"/>
          <Link href={"/blog/" + this.props.id}><div className="link arrow"><i className="fas fa-chevron-right"></i></div></Link>
    </div>
   );
  }
}

class RelatedPosts extends React.Component {
  render() {
    return (
    <div className="related-posts content-section background-grey no-min-height "> 
        <div className="container">
          <h3 className="container-title">Other articles you might like</h3>
          <div className="card-container resources">
            <SinglePost id={'1'} style={' light'} delay={'25'} title={'Why Mindfulness is a Performance Superpower'} category={'Mindfulness '} background={'/static/img/blog/blog-1.jpg'}/>
            <SinglePost id={'2'} style={' light'} delay={'50'} title={'10 Sports Psychology Mental Training Tips'} category={'Performance'} background={'/static/img/blog/blog-2.jpg'}/>
            <SinglePost id={'3'} style={' light'} delay={'100'} title={'How Mindfulness Can Drastically Improve Your Business'} category={'Mindfulness'} background={'/static/img/blog/blog-3.jpg'}/>
          </div>
        </div>
    </div>
   );
  } 
}

class RelatedPost extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout>
      <div className="page detailed-post">
      	<div className="container">
          <div className="wow fadeIn fadeUp">
      		  <PostCarousel background={'/static/img/blog/blog-6.jpg'} />
          </div>
	      	<div className="title-container ">
    	    	<div className="small-container wow fadeIn fadeUp">
    	    		<h3>Building Psychological Momentum</h3>
    	    		<div className="post-date wow fadeIn fadeUp delay-50">21 June 2018</div>
    	    	</div>
      		</div>
      		<div className="body-content wow fadeIn">
            <strong>Written by Leanne Huggett</strong>
            <br/><br/>
      			In sport, this concept is highly emphasised. In fact, 92% of football coaches believe that their success is crucially determined by this. And this concept is psychological momentum.
            <br/><br/> 
            Also known as the ‘hot handed fallacy’, it describes the belief that initial success leads to more success.
            A team who has a few successful plays early on builds positive momentum, which is difficult to stop - however if they make a mistake in the play, that error can compound and lead to the rest of the game falling apart.
            <br/><br/> 
            Psychological momentum, of course, is all in your head. The momentum has no basis in reality. A correct coin toss doesn’t create a greater likelihood for the next one to be correct. However, it does create real and tangible benefits in confidence and approach oriented behaviour. Perceived momentum leads to increased confidence which leads to more activity and better performance. Where teams feel unstoppable.
            <br/><br/> 
            So, how can we create this psychological momentum and capitalise on the benefits for our performance?
            <br/><br/> 
            1.  Use the ‘as-if’ principle. Your emotions don’t control your behavior; rather, your behavior controls your emotions. Conduct yourself as though you are already succeeding in a manner than exudes confidence, composure, well rested and energized.
            <br/><br/> 
            2.  Celebrate the small wins. By celebrating the small wins we develop the ability to see progress, no matter how small. This is very handy when things are moving slowly. It also develops your ability to see the positive and look for opportunities for greatness, regardless of the situation.
            <br/><br/> 
            Building and sustaining momentum is something you can take on with both hands, however no on can do it for you. It isn’t always easy, but to push forward and create your own success - you cannot let your mindset be the victim of circumstance.
      		</div>
      		<div className="article-like wow fadeIn">
      			<strong>Did you like this article?</strong>
      			<button className="like"></button>
      		</div>
      	</div>
      	<div className="topics wow fadeIn">
      		<div className="container">
      			<div className="topic title">Topics</div>
      			<div className="topic">Sport</div>	
      			<div className="topic">Success</div>	
      			<div className="topic">Mental</div>	
      			<div className="topic">Momentum</div>	
      			<div className="topic">Win</div>	
      			<div className="topic">Mindset</div>	
      		</div>
      	</div>
        <div className="wow fadeIn">
          <RelatedPosts />
        </div>
      </div>
    </Layout>
    );
  }
}

export default RelatedPost