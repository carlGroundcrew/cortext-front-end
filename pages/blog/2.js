import React from 'react'
import Link from 'next/link'; 
import Layout from '../../components/Layout';
import PostCarousel from "../../components/PostCarousel";
import { getPost } from '../../api/posts';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class SinglePost extends React.Component {
  render() {
    return (
    <div className={"single-card large wow fadeIn fadeUp delay-" + this.props.delay + this.props.style}>
          <Link href={"/blog/" + this.props.id}>
            <div className="title-content">
              <strong className="overline">{this.props.category}</strong>
              <h3 className="h3">{this.props.title}</h3>
            </div>
          </Link>
          <img className="background" src={this.props.background} alt="Card"/>
          <Link href={"/blog/" + this.props.id}><div className="link arrow"><i className="fas fa-chevron-right"></i></div></Link>
    </div>
   );
  }
}

class RelatedPosts extends React.Component {
  render() {
    return (
    <div className="related-posts content-section background-grey no-min-height "> 
        <div className="container">
          <h3 className="container-title">Other articles you might like</h3>
          <div className="card-container resources">
            <SinglePost id={'3'} style={' light'} delay={'25'} title={'How Mindfulness Can Drastically Improve Your Business'} category={'Mindfulness '} background={'/static/img/blog/blog-3.jpg'}/>
            <SinglePost id={'4'} style={' light'} delay={'50'} title={'Mental Health Now An Issue In Australian Sport'} category={'Performance'} background={'/static/img/blog/blog-4.jpg'}/>
            <SinglePost id={'5'} style={' light'} delay={'100'} title={'The Trap of Feeling “Good Enough”'} category={'Performance'} background={'/static/img/blog/blog-5.jpg'}/>
          </div>
        </div>
    </div>
   );
  } 
}

class RelatedPost extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout>
      <div className="page detailed-post">
      	<div className="container">
          <div className="wow fadeIn fadeUp">
      		  <PostCarousel background={'/static/img/blog/blog-2.jpg'} />
          </div>
	      	<div className="title-container ">
    	    	<div className="small-container wow fadeIn fadeUp">
    	    		<h3>10 Sports Psychology Mental Training Tips</h3>
    	    		<div className="post-date wow fadeIn fadeUp delay-50">26 May 2018</div>
    	    	</div>
      		</div>
      		<div className="body-content wow fadeIn">
      			<strong>
            We need to train our mind with the same emphasis that we train our body. Here are a few mental training tips from Dr. JoAnn Dahlkoetter that we think really hit it on the nose:  
      			</strong>
      			<br/><br/>
            Want to know how you can use sports psychology and mental training to reach your health, fitness and sports goals faster, easier, and get the results you want? Here are my top 10 mindset tips to help you build new motivation, confidence and major breakthroughs in your fitness, your training and in your life.
            <br/><br/>
            <strong>10. Positive Images:</strong>
            <br/>
            When your are exercising, use your positive mental images throughout your workout to create feelings of speed and power. (e.g., If  you’re walking or running and you come to an unexpected hill visualize a magnet pulling you effortlessly to the top). Use visualization before, during and after your training to build confidence and new motivation.
            <br/><br/>
            <strong>9. Power Words:</strong>
            <br/>
            Make positive self-statements continually.  Negative thinking is common; everyone has an inner critic. Become aware of these thoughts early on. Don’t fight with them; simply acknowledge their presence, and then substitute positive power words. (e.g., When you’re thinking: “This hurts too much, I want to lay down and die”; say to yourself: “This feeling is connected with getting healthier and doing my absolute best.”)
            <br/><br/>
            <strong>8. Present Focus:</strong>
            <br/>
            Practice being in the present moment. Remind yourself to stay in the here and now. Instead of replaying past mistakes, or worrying about the future, let past and future events fade into the background. Be right on, right here, right now.
            <br/><br/>
            <strong>7. Advantage:</strong>
            <br/>
            Use everything in your workout to your advantage. For example, if another person passes you, tuck in behind and go with his or her energy for as long as possible. You may catch a “second wind” and be carried on to a personal record.
            <br/><br/>  
            <strong>6. Chunking Goals:</strong>
            <br/>
            Focus on your immediate target. Break your training goals down into small, manageable pieces and begin to focus only on the first portion, not the entire workout (e.g., Say to yourself: “I’m just relaxing and getting my rhythm during the first part, or the first workout session”).
            <br/><br/>  
            <strong>5. Body Scan:</strong>
            <br/>
            Pay close attention to your tension level and training form. Do a body scan while working out and relax your tight muscles frequently. Ask yourself: “Are my shoulders and neck relaxed; how does this pace feel; how much energy is left in my legs?”
            <br/><br/>  
            <strong>4. Pain as Effort:</strong>
            <br/>  
            If you have “good pain,” the pain of effort, that is not seriously damaging your body, just shift attention to your breathing or cadence of movement, and let the discomfort fade into the background. You can also use the pain as feedback. Register it not as pain but as effort level. Say: “Now I know exactly how hard I’m working. I know how this pace feels. My body is doing what it should be doing.”
            <br/><br/> 
            
            <strong>3. Detach From Outcome:</strong>
            <br/>  
            Look only at what you need to do right now (e.g., your pace, your breathing, your concentration); your final time, place, or score will take care of itself.
            <br/><br/> 

            <strong>2. Focused Attention: </strong>
            <br/>  
            Be aware of distractions. Breathe out unwanted thoughts with your next exhale and re-focus your attention instantly on what is important right now, at this moment.
            <br/><br/> 

            <strong>1. Celebration:</strong>
            <br/>  
            Enjoy and appreciate your fitness and strength. When you exercise, relax and let your body do what you’ve trained it to do.  Remember that your goals are realistic. All you need to do is perform up to your capabilities.
            <br/><br/>
            <i>Dahlkoetter, J. (2013, January 6). 10 Sports Psychology Mental Training Tips. Retrieved from https://www.huffingtonpost.com/dr-joann-dahlkoetter/sports-psychology_b_2062354.html</i>
            <br/><br/>
      		</div>
      		<div className="article-like wow fadeIn">
      			<strong>Did you like this article?</strong>
      			<button className="like"></button>
      		</div>
      	</div>
      	<div className="topics wow fadeIn">
      		<div className="container">
      			<div className="topic title">Topics</div>
      			<div className="topic">Sport</div>	
      			<div className="topic">Training</div>	
      			<div className="topic">Tips</div>	
      			<div className="topic">Mental</div>	
      			<div className="topic">Health</div>	
      			<div className="topic">Fitness</div>	
      		</div>
      	</div>
        <div className="wow fadeIn">
          <RelatedPosts />
        </div>
      </div>
    </Layout>
    );
  }
}

export default RelatedPost