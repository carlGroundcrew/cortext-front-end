import React from 'react'
import Link from 'next/link'
import Layout from '../../components/Layout';
import PostCarousel from "../../components/PostCarousel";
import { getPost } from '../../api/posts';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class SinglePost extends React.Component {
  render() {
    return (
    <div className={"single-card large wow fadeIn fadeUp delay-" + this.props.delay + this.props.style}>
          <Link href={"/blog/" + this.props.id}>
            <div className="title-content">
              <strong className="overline">{this.props.category}</strong>
              <h3 className="h3">{this.props.title}</h3>
            </div>
          </Link>
          <img className="background" src={this.props.background} alt="Card"/>
          <Link href={"/blog/" + this.props.id}><div className="link arrow"><i className="fas fa-chevron-right"></i></div></Link>
    </div>
   );
  }
}

class RelatedPosts extends React.Component {
  render() {
    return (
    <div className="related-posts content-section background-grey no-min-height "> 
        <div className="container">
          <h3 className="container-title">Other articles you might like</h3>
          <div className="card-container resources">
            <SinglePost id={'4'}style={' light'} delay={'25'} title={'Mental Health Now An Issue In Australian Sport'} category={'Mental '} background={'/static/img/blog/blog-4.jpg'}/>
            <SinglePost id={'5'} style={' light'} delay={'50'} title={'The Trap of Feeling “Good Enough”'} category={'Mental'} background={'/static/img/blog/blog-5.jpg'}/>
            <SinglePost id={'6'} style={' light'} delay={'100'} title={'Building Psychological Momentum”'} category={'Performance'} background={'/static/img/blog/blog-6.jpg'}/>
          </div>
        </div>
    </div>
   );
  } 
}

class RelatedPost extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout>
      <div className="page detailed-post">
      	<div className="container">
          <div className="wow fadeIn fadeUp">
      		  <PostCarousel background={'/static/img/blog/blog-3.jpg'} />
          </div>
	      	<div className="title-container ">
    	    	<div className="small-container wow fadeIn fadeUp">
    	    		<h3>How Mindfulness Can Drastically Improve Your Business</h3>
    	    		<div className="post-date wow fadeIn fadeUp delay-50">26 May 2018</div>
    	    	</div>
      		</div>
      		<div className="body-content wow fadeIn">
      			<strong>
            Just a few ways mindfulness training impacts business success. Simple and massively effective. Mindfulness training works on the neurological level to restructure how your brain functions, in ways that benefit focus, emotion regulation and memory. This is why mindfulness training is such a central component of the Cortex Performance Methodology.      			
            </strong>
      			<br/><br/>
            By Kumar Arora, a serial entrepreneur turned investor. 
            <br/><br/>
            You may not think of hustling entrepreneurs as those who carve out time for mindfulnessmeditation. However, you would be surprised how widely mindfulness is practiced among the most innovative minds today. As the years have gone by, I have been practicing meditation every day. Mindfulness opens up new mental changes conducive to entrepreneurship. In fact, various studies have found that its possible that mindfulness can reduce cortisol levels as well as thicken grey matter in the brain, improving the ability to handle information.
            <br/><br/>
            When your are exercising, use your positive mental images throughout your workout to create feelings of speed and power. (e.g., If  you’re walking or running and you come to an unexpected hill visualize a magnet pulling you effortlessly to the top). Use visualization before, during and after your training to build confidence and new motivation.
            <br/><br/>
            This sounds exactly what entrepreneurs need and can utilize to make the most of their intense daily routines. I've seen too many entrepreneurs burn out and lose focus because they choose to hustle first and not think about their own well-being.
            <br/><br/>
            <strong>How Does Mindfulness Meditation Change Your Brain?</strong>
            <br/><br/>
            This mind-altering positive practice is nothing new. Buddhist monks have been using it for centuries. By entering a catatonic state of mindfulness, they are able to experience tranquility, insight and inhibited focus.
            There are plenty of benefits behind mindfulness meditation. And more often than not, those who practice have greater control over stress and anxiety. It can improve your energy levels, give you new insights into your ideas and allow you to remain calm throughout the day.
            <br/><br/>
            <strong>Improve Your Focus</strong>
            <br/><br/>
            If one cannot completely focus, their idea mill may be clouded with other daily issues, thus impeding their creative flow. One of the most effective benefits of mindfulness meditation is gaining a higher level of focus. Buzzes, vibrations and dings of incoming emails, social media and chat platforms can all distract you from what is most important. The good news is that mindfulness can help.
            <br/><br/>  
            Meditating regularly will help you to develop an inner voice that will help you shut out those distractions. Over time, you will even notice that your inner focus coach is becoming much louder than those dinging notifications. I've found personally that when I make the habit of practicing meditation before I begin a challenging project, my mind is not only clearer, but I'm able to process and complete the task more efficiently. When it comes to owning multiple businesses, it often is difficult to switch directions. Meditation is a great way to reset.  
            <br/><br/>  
            <strong>Awaken a Deeper Creativity</strong>
            <br/><br/>
            If you could tap into a deeper creative self, your entrepreneurial endeavors would be far more powerful and successful. This is another way mindfulness meditation can take your hustle to the next level. By carving out time in your day for peace and quiet during meditation, you allow your mind to decompress. I've found that mornings and early evenings have been the most optimal times for meditation. This could spark flashes of creativity and innovation otherwise lost in a busy day.
            <br/><br/>  
            <strong>Communicate More Effectively Under Stress</strong>
            <br/><br/>  
            Another important trait of successful entrepreneurs is effective communication, especially under stress. If you can hone your communication skills, you will be sure to see more professional success and personal growth. What once kept us safe from physical, predatory threats is now making us stressed and anxious when dealing with mental threats, such as a client negotiation or investor oversight.
      		  <br/><br/>  
            Mindfulness meditation can turn this stress on its head and allow you to communicate effectively in any situation. Instead of avoiding daily issues, you can confront them with the cool-headedness needed to facilitate success. I've found that whenever I am in a difficult situation, it's easier to take a step back and meditate if possible before tackling it head-on. After all, if you can communicate your ideas effectively, they may never come to fruition.
            <br/><br/> 
            <strong>Make Failure a Positive Feeling</strong>
            <br/><br/> 
            Most entrepreneurs who are highly successful have failed and failed more than a few times. This is what makes entrepreneurship so unique and exciting. The fear of the unknown and the drive to succeed makes you who you are.
            <br/><br/> 
            However, not all you greatest ideas turn out how you might have originally envisioned them. It is how you deal with your failures that defines your next innovative project. Mindfulness meditation helps you make failure a positive feeling. It can teach your mind to manage your emotions more effectively and productively.
            <br/><br/> 
            You cannot change the past, other people or the outcomes you will face as an entrepreneur. However, the one thing you can do is be in control of yourself. In doing so, you will easily find that certain situations, your lifestyle and ultimately your business will prosper with a new mindset.
            <br/><br/>
            <i>Young Entrepreneur Council. (2018, February 2). How Mindfulness Can Drastically Improve Your Business. Retrieved from https://www.inc.com/young-entrepreneur-council/4-ways-mindfulness-meditation-makes-successful-entrepreneurs.html</i>
          </div>
      		<div className="article-like wow fadeIn">
      			<strong>Did you like this article?</strong>
      			<button className="like"></button>
      		</div>
      	</div>
      	<div className="topics wow fadeIn">
      		<div className="container">
      			<div className="topic title">Topics</div>
      			<div className="topic">Mindfulness</div>	
      			<div className="topic">Business</div>	
      			<div className="topic">Brain Function</div>	
      			<div className="topic">Meditation</div>	
      			<div className="topic">Focus</div>	
      			<div className="topic">Cortex</div>	
      		</div>
      	</div>
        <div className="wow fadeIn">
          <RelatedPosts />
        </div>
      </div>
    </Layout>
    );
  }
}

export default RelatedPost