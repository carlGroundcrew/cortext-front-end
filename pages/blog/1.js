import React from 'react'
import Link from 'next/link';  
import Layout from '../../components/Layout';
import PostCarousel from "../../components/PostCarousel";
import { getPost } from '../../api/posts';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class SinglePost extends React.Component {
  render() {
    return (
    <div className={"single-card large wow fadeIn fadeUp delay-" + this.props.delay + this.props.style}>
          <Link href={"/blog/" + this.props.id}>
            <div className="title-content">
              <strong className="overline">{this.props.category}</strong>
              <h3 className="h3">{this.props.title}</h3>
            </div>
          </Link>
          <img className="background" src={this.props.background} alt="Card"/>
          <Link href={"/blog/" + this.props.id}><div className="link arrow"><i className="fas fa-chevron-right"></i></div></Link>
    </div>
   );
  }
}

class RelatedPosts extends React.Component {
  render() {
    return (
    <div className="related-posts content-section background-grey no-min-height "> 
        <div className="container">
          <h3 className="container-title">Other articles you might like</h3>
          <div className="card-container resources">
            <SinglePost id={'2'} style={' light'} delay={'25'} title={'10 Sports Psychology Mental Training Tips'} category={'Mindfulness'} background={'/static/img/blog/blog-2.jpg'}/>
            <SinglePost id={'3'} style={' light'} delay={'50'} title={'How Mindfulness Can Drastically Improve Your Business'} category={'Performance'} background={'/static/img/blog/blog-3.jpg'}/>
            <SinglePost id={'4'} style={' light'} delay={'100'} title={'Mental Health Now An Issue In Australian Sport'} category={'podcast'} background={'/static/img/blog/blog-4.jpg'}/>
          </div>
        </div>
    </div>
   );
  } 
}

class RelatedPost extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout>
      <div className="page detailed-post">
      	<div className="container">
          <div className="wow fadeIn fadeUp">
      		  <PostCarousel background={'/static/img/blog/blog-1.jpg'} />
          </div>
	      	<div className="title-container ">
    	    	<div className="small-container wow fadeIn fadeUp">
    	    		<h3>Why Mindfulness is a Performance Superpower</h3>
    	    		<div className="post-date wow fadeIn fadeUp delay-50">16 May 2018</div>
    	    	</div>
      		</div>
      		<div className="body-content wow fadeIn">
            <strong>Written by Leanne Huggett</strong>
            <br/><br/>
      			<strong>
      			Mindfulness training has taken the performance world by storm. Everyone from high level business to olympians to elite military units have incorporated this into their daily life with priority. And with good reason: because its ability to train the brain for high performance is virtually unparalleled.
      			</strong>
      			<br/><br/>
            Mindfulness is a state of mind where the brain is attentive to the present moment without judgement.
            <br/><br/>
            This skill has become one of performance psychology’s greatest tools to enhance positive mental capabilities, and has been adopted by some of the world’s highest performers.
            <br/><br/>
            University of Miami researcher Dr Amisji Jha has shown that mindfulness meditation significantly improves soldiers responsiveness, resilience and performance. Research has also found mindfulness to improve cognitive and physical capabilities of soldiers, as well as response speed, memory functioning, situational awareness, communication, emotion management and decision making.
            <br/><br/>
            Mindfulness works because it creates a “mental space” between the situation and your response. This allows decisions to be made more objectively, rather than reacting to emotions, thoughts or situations.
            <br/><br/>
            So while it might seen counterintuitive that slowing down helps to improve response speed and decision making, it is the mental clarity gained from training mindfulness that yields results.
            <br/><br/>
            An elite military unit may be about as far flung from many people’s image of mindfulness but indeed for a growing number of military units mindfulness has become a structured part of their pre-deployment mental training. In a performance domain where even 1% of an edge could mean the difference between life and death - having an edge in mental acuity has become a battleground in itself.
            <br/><br/>
            While a relatively new practice in military domains, one area where mindfulness has been long embraced is sporting performance.
            <br/><br/>
            Those who credit mindfulness for their performance include Lebron James, Kobe Bryant, The Seattle Seahawks, Tom Daley and Kerri Walsh, among many more.
            <br/><br/>
            Research into the effects of mindfulness training found that mindfulness works on the neurological level to improve attention, focus and facilitate a “flow state” or “peak performance state”. Through practice it starts to rewire our neural connections away from ‘fight or flight’ impulses and toward our ‘executive control centre’.
            <br/><br/>
            And through the training of attention, athletes are better able to direct their attention to what their performance; and away from external distractions such as negative thoughts or a packed stadium at an away stadium.
            <br/><br/>
            This training of ‘mental space’ creates a greater sense of mental resilience to anxiety and pressure through the ability to see thoughts and emotions as mental events, which diminishes any obstructing effect they might have on performance.
            <br/><br/>
            Caroline Buchanan, who has eight BMX world championships and two Olympic Medals under her belt, explains how mindfulness impacts her performance.
            <br/><br/>
            She says, “When the fear starts coming in... that was when I really found that mindfulness was the best strategy for me to be able to make peace in those situations,”
            <br/><br/>
            "At that elite level, it can mean that 1 per cent difference which can mean you are a world champion or you're not.”
            The practice itself might involve paying attention to specific areas of the body such as your feet, knees, stomach or shoulders. Or it might be mindful breathing, which involves focus on a deep and rhythmic breathing.
            <br/><br/>
            Beyond the techniques, reflexes and core strength, the ability to stay calm and focused in the heat of competition is often the biggest edge.
            In the same way one would train the body in the gym, mindfulness training is being treated as a strengthening exercise for the mind to perform under pressure.
            <br/><br/>
            A free introduction to these exercises will be available on the Cortex Method Base Program in the upon Launch.
      		</div>
      		<div className="article-like wow fadeIn">
      			<strong>Did you like this article?</strong>
      			<button className="like"></button>
      		</div>
      	</div>
      	<div className="topics wow fadeIn">
      		<div className="container">
      			<div className="topic title">Topics</div>
      			<div className="topic">Mindfulness</div>	
      			<div className="topic">Performance</div>	
      			<div className="topic">Business</div>	
      			<div className="topic">Cortex</div>	
      			<div className="topic">Exercise</div>	
      			<div className="topic">Training</div>	
      		</div>
      	</div>
        <div className="wow fadeIn">
          <RelatedPosts />
        </div>
      </div>
    </Layout>
    );
  }
}

export default RelatedPost
