import React from 'react'
import Link from 'next/link'
import Layout from '../../components/Layout';
import PostCarousel from "../../components/PostCarousel";
import { getPost } from '../../api/posts';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class SinglePost extends React.Component {
  render() {
    return (
    <div className={"single-card large wow fadeIn fadeUp delay-" + this.props.delay + this.props.style}>
          <Link href={"/blog/" + this.props.id}>
            <div className="title-content">
              <strong className="overline">{this.props.category}</strong>
              <h3 className="h3">{this.props.title}</h3>
            </div>
          </Link>
          <img className="background" src={this.props.background} alt="Card"/>
          <Link href={"/blog/" + this.props.id}><div className="link arrow"><i className="fas fa-chevron-right"></i></div></Link>
    </div>
   );
  }
}

class RelatedPosts extends React.Component {
  render() {
    return (
    <div className="related-posts content-section background-grey no-min-height "> 
        <div className="container">
          <h3 className="container-title">Other articles you might like</h3>
          <div className="card-container resources">
            <SinglePost id={'5'} style={' light'} delay={'25'} title={'The Trap of Feeling “Good Enough”'} category={'Mental '} background={'/static/img/blog/blog-5.jpg'}/>
            <SinglePost id={'6'} style={' light'} delay={'50'} title={'Building Psychological Momentum'} category={'Mindfulness'} background={'/static/img/blog/blog-6.jpg'}/>
            <SinglePost id={'1'} style={' light'} delay={'100'} title={'Why Mindfulness is a Performance Superpower”'} category={'Performance'} background={'/static/img/blog/blog-1.jpg'}/>
          </div>
        </div>
    </div>
   );
  } 
}

class RelatedPost extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout>
      <div className="page detailed-post">
      	<div className="container">
          <div className="wow fadeIn fadeUp">
      		  <PostCarousel background={'/static/img/blog/blog-4.jpg'} />
          </div>
	      	<div className="title-container ">
    	    	<div className="small-container wow fadeIn fadeUp">
    	    		<h3>Mental Health Now An Issue In Australian Sport</h3>
    	    		<div className="post-date wow fadeIn fadeUp delay-50">15 July 2015</div>
    	    	</div>
      		</div>
      		<div className="body-content wow fadeIn">
      			Commentary: 'With the emphasis that sportspeople place on training their bodies, how much training are Australian sportspeople investing in training their mind?
            <br/><br/>
            Our mind determines everything we do, our choices, our responses and our emotions. It is a key muscle that needs to be build with the same priority as physical muscles if we are to perform and compete on the world stage.
            It's about building mental strength.
            <br/><br/>
            Read more at <a href="https://www.huffingtonpost.com.au/2015/10/10/mental-health-sport_n_8266914.html">here</a>
            <br/><br/>
            <i>Butler, J. (2016, July 15). Mental Health Now An Issue In Australian Sport. Retrieved from https://www.huffingtonpost.com.au/2015/10/10/mental-health-sport_n_8266914.html</i>
      		</div>
      		<div className="article-like wow fadeIn">
      			<strong>Did you like this article?</strong>
      			<button className="like"></button>
      		</div>
      	</div>
      	<div className="topics wow fadeIn">
      		<div className="container">
      			<div className="topic title">Topics</div>
      			<div className="topic">Sport</div>	
      			<div className="topic">Training</div>	
      			<div className="topic">Tips</div>	
      			<div className="topic">Mental Health</div>	
      			<div className="topic">Australia</div>	
      			<div className="topic">Fitness</div>	
      		</div>
      	</div>
        <div className="wow fadeIn">
          <RelatedPosts />
        </div>
      </div>
    </Layout>
    );
  }
}

export default RelatedPost