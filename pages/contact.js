import Link from 'next/link';
import Layout from '../components/Layout';
import Form from '../components/Form';
import FAQs from "../components/FAQs";
import CallBanner from "../components/CallBanner";
import ContactCard from "../components/contact/Card";

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: false};
    this.handler = this.handler.bind(this);
    this.openAccordion = this.openAccordion.bind(this);
  }
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:      	75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
    ).init();
  }
  handler() {
    this.setState({
      isToggleOn: true
    })
  }
  openAccordion() {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }
  render() {
    return (
	  <Layout page="contact"> 
	      <div className="page contact">
          <ContactCard />
	      	<div ref="accordion" className="banner panel accordion">
	      		<div className={this.state.isToggleOn ? 'accordion open' : 'accordion'}>
		      		<div className="container">
		      			<div className="accordion-title wow fadeIn" onClick={this.openAccordion}>
		      				<h3>Send us a message</h3>
		      			</div>
		      			<div className="internal">
		      				<Form />
		      			</div>
		      		</div>
		      	</div>
	      	</div>
	      	<div className="content-section slider-extended">
		      	<div className="container">
		      		<div className="title-area">
		            	<h3 className="h1 title">FAQ's</h3> 
		            	<Link  href="/method"><a className="link standard-link hide">See all <i className="fas fa-chevron-right"></i></a></Link>
		            </div>
		            <div className="wow fadeIn fadeLeft delay-100">
		            	<FAQs/> 
		            </div>
		    	</div>
		    </div>
		    <CallBanner handler = {this.handler} />
	      </div>
	    </Layout>

	);
  }
}

export default Contact