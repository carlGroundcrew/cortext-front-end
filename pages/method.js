import Layout from '../components/Layout';
import BannerCarousel from '../components/BannerCarousel';
import ContentTabs from '../components/ContentTabs';
import SignupBanner from '../components/SignupBanner'; 
import Pillars from "../components/cards/Pillars"; 
import MethodColumns from "../components/method/MethodColumns"; 
import Link from 'next/link'; 
 
const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Method extends React.Component {
	componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:      	75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
  	render() {
    
    return (
	  <Layout className="is-light" page="method"> 
	      <div className="page without-padding">
	   
	      	<MethodColumns />
	      	
	      	<div id="offering" className="content-section slider-extended">
	            <div className="container">
	                <div className="title-area wow fadeIn">
	                  <h3 className="h2 title">The Method</h3>
	                </div> 
	                <div className="content-with-intro">
	                	<div className="intro wow fadeIn">
	                		<h3 className="h3 title">Performance</h3>
	                		<div className="text">
	                			We’ll teach you fundamental principles of performance psychology — enabling you to stack probability in the favour of success.
	                		</div>
	                	</div>
	                	<div className="content wow fadeIn fadeLeft delay-50">
	                		<Pillars />
	                	</div>
	                </div>
	            </div>
	            <div className="content-section no-min-height no-padding mobile-no-padding-bottom">
	            	<div className="container">
		                <div className="content-with-intro">
		                	<div className="intro no-padding-tablet">
		                		<h3 className="h3 title">Mindfulness</h3>
		                		<div className="text">
		                			Mindfulness meditation is central to the Cortex Method because its ability to train our mind for high performance is unparalleled. It has been adopted with measurable success in everything from elite level sport, to multinational organisations and military special forces. And now to you.
		                		</div>
		                	</div>
		                	<div className="content">
		                		 <img className="single-image wow fadeIn delay-50" src="/static/img/method/mindfulness.jpg" alt="Profile Image"  />
		                	</div>
		                </div>
		            </div>
	            </div>
	        </div>	
	    	<div className="content-section wow fadeIn fadeUp background-grey" data-wow-offset="200">
	      	 	<div className="container">
	      	 	<div className="title-with-link wow fadeIn">
	            	<h3 className="h2 title">The program</h3>
	            	<Link href="/program"><button className="black">See pricing</button></Link>
	            </div> 
	      	 	<ContentTabs />
	    	   	</div>
	    	</div>
	    	<SignupBanner title={'Subscribe today and receive Our 20% discount. For life.'} background={'/static/img/banners/banner-6.jpg'}/>
	      </div>
	    </Layout>
);
  }
}

export default Method