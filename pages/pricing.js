import Layout from '../components/Layout';
import PricingCards from '../components/pricing/Cards';
import PricingList from '../components/pricing/List'; 
import SignupBanner from '../components/SignupBanner'; 
import Link from 'next/link'; 

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Pricing extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="pricing">
      <div className="page">
      	<div className="content-section no-min-height no-padding-bottom">
      	 	<div className="container">
      	 		<div className="container-sm left-align">
      	 			<h2 className="h1 title"><span className="wow fadeIn fadeUp">Our Plans</span></h2>
				    </div> 
            <PricingCards />
    	   	</div>
    	  </div>
        <div className="content-section text-banner no-min-height no-padding-top">
        	<div className="container">
            <PricingList />
      	  </div>
        </div>
        <SignupBanner title={'Interested in a Corporate Package?'} background={'/static/img/banners/banner-6.jpg'}/>
      </div>
    </Layout> 
    );
  }
}

export default Pricing