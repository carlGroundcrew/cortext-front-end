import Link from 'next/link'; 
import Layout from '../components/Layout';
import Unsubscribe from '../components/Unsubscribe';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class EmailUnsubscribe extends React.Component {
  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout title='Email Unsubscribe' page="email-unsubscribe">
      <div className="page">
        <div className="content-section no-overflow hubspot-form unsubscribe-form">
            <img className="background" src='/static/img/unsubscribe/background.png' alt="Background"/>
            <div className="container">
              <div className="container-rg center-align wow fadeIn fadeUp delay-100">
                  <Unsubscribe />
              </div>
            </div>
        </div>
      </div>
    </Layout> 
    );
  }
} 

export default EmailUnsubscribe