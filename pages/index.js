import Layout from '../components/Layout'; 
import Hero from "../components/hero/V2";  
import Pillars from "../components/cards/Pillars";  
import Promotion from "../components/resources/Promotion";
import Testimonials from "../components/Testimonials/Grid";
import SignupBanner from "../components/SignupBanner";
import TestimonialCarousel from '../components/Testimonials/Carousel';
import TiledBanner from "../components/banner/TileBanner";
import Link from 'next/link'; 

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Index extends React.Component {
	constructor(props) {
        super(props)
        this.myRef = React.createRef()
        this.state = {scrolling:false}
        this.state = {intervalId: 0 };
        this.handleScroll = this.handleScroll.bind(this)
        this.scrollToTop = this.scrollToTop.bind(this)
    }
	componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:      	75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();

        window.addEventListener('scroll', this.handleScroll);
    }
	componentWillUnmount() {
	    window.removeEventListener('scroll', this.handleScroll);
	}
	handleScroll(event) {
	    if (window.scrollY < 100 && this.state.scrolling === true) {
	        this.setState({scrolling: false});
	    }
	    else if (window.scrollY > 100 && this.state.scrolling !== true) {
	        this.setState({scrolling: true});
	    }
	    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        const windowBottom = Math.round(windowHeight + window.pageYOffset);
        if (windowBottom+100 >= docHeight) {
            this.setState({scrolling: false});
        } 
	}
	scrollStep() {
	    if (window.pageYOffset === 0) {
	        clearInterval(this.state.intervalId);
	    }
	    window.scroll(0, window.pageYOffset - 50);
	} 	 
	scrollToTop() {
	    let intervalId = setInterval(this.scrollStep.bind(this), 8);
	    this.setState({ intervalId: intervalId });
	}
  	render() {
    return (
	  <Layout>
	  	<div className={this.state.scrolling ? 'promotion-banner scrolling' : 'promotion-banner'}>
	  		<div className="container-lg">
	  			<p className="scroll" onClick={ () => { this.scrollToTop(); }}>
	  			<span>Scroll up <i className='fas fa-caret-up'></i></span></p>
	  			<p className="coming-soon"><span>Coming soon to</span> <i className="fab fa-apple"></i> <i className="fab fa-android"></i></p>
	  		</div>
	  	</div>
		<div className="home page"> 
	        <Hero state={this.state.loading} />
	        <div className="content-section no-min-height mobile-no-padding-top">
	        	<div className="container">
	        		<TiledBanner title={'Introducing Cortex Method.'} content={'A performance psychology app designed to help you succeed in every area of life.'} tagline={"Don't just aim for success. Take it."} />
	        	</div>
	        </div>
	        <div id="method" className="content-section slider-extended">
	            <div className="container">
	                <div className="title-area wow fadeIn">
	                  <h3 className="h2 title">Own every moment</h3>
	                  <p className="subtitle">There are 5 pillars which underpin your ability to perform and succeed</p>
	                </div>
	                <div className="wow fadeIn fadeLeft">
	                	<Pillars />
	                </div>
	            </div>
	        </div>	
	        <div id="resources" className="content-section banner resources mobile-no-padding-bottom">
	        	<div className="container">
		        	<div className="half padding-right content-area wow fadeIn fadeUp">
		        		<div className="inner">
		        			<h3>Mind over matter, made better.</h3>
		        			Success is easy when you know how to master your mind. With over 100 sessions, Cortex Method will teach you how to combine the art of mindfulness and performance psychology. 
		        		</div>
		        	</div>
		        	<div className="half wow fadeIn fadeUp delay-50">
		        		<Promotion />
		        	</div>
		        </div>
	        </div>
	        <div id="testimonials" className="content-section no-min-height wow fadeIn fadeUp mobile-no-padding-bottom">
	        	<div className="container">
	        		<div className="title-area centered">
	        			<h3 className="centered">Here’s what our users have said</h3>
	        		</div>
	        		<Testimonials />
	        	</div>	
	       	</div>
	       	<div id="pillars" className="content-section slider-extended mobile-no-padding-top">
	      	 	<div className="">
	      	 		<div className="wow fadeIn">
	      	 			<TestimonialCarousel />
	      	 		</div>
	    	   	</div>
	    	</div>
	        <SignupBanner className={'light'} title={'Get ready to level up.'} subtitle={'Subscribe for our exclusive pre-launch offer, 20% off for life.'} background={''}/>
      </div>
	</Layout>

	);
  }
}

export default Index