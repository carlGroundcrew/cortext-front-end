import Layout from '../../components/Layout';
import Cards from "../../components/cards/Cards";  
import Resources from "../../components/resources/Introduction";
import PerformanceBanner from "../../components/PerformanceBanner";
import SignupBannerLarge from "../../components/SignupBannerLarge";
import Link from 'next/link'; 

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Index extends React.Component {
	constructor(props) {
        super(props)
        this.myRef = React.createRef()
        this.state = {scrolling:false}
        this.state = {intervalId: 0 };
        this.handleScroll = this.handleScroll.bind(this)
        this.scrollToTop = this.scrollToTop.bind(this)
    }
	componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:      	75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();

        window.addEventListener('scroll', this.handleScroll);
    }
	componentWillUnmount() {
	    window.removeEventListener('scroll', this.handleScroll);
	}
	handleScroll(event) {
	    if (window.scrollY < 100 && this.state.scrolling === true) {
	        this.setState({scrolling: false});
	    }
	    else if (window.scrollY > 100 && this.state.scrolling !== true) {
	        this.setState({scrolling: true});
	    }
	}
	scrollStep() {
	    if (window.pageYOffset === 0) {
	        clearInterval(this.state.intervalId);
	    }
	    window.scroll(0, window.pageYOffset - 50);
	} 	 
	scrollToTop() {
	    let intervalId = setInterval(this.scrollStep.bind(this), 8);
	    this.setState({ intervalId: intervalId });
	}
  	render() {
    return (
	  <Layout>
	  	<div className={this.state.scrolling ? 'promotion-banner scrolling' : 'promotion-banner'}>
	  		<div className="container-lg">
	  			<p className="scroll" onClick={ () => { this.scrollToTop(); }}>
	  			<span>Scroll up <i className='fas fa-caret-up'></i></span></p>
	  			<p className="coming-soon"><span>Coming soon to</span> <i className="fab fa-apple"></i> <i className="fab fa-android"></i></p>
	  		</div>
	  	</div>
		<div className="home page"> 
	        <div id="banner" className="content-section">
	          <div className="container">
	              <div className="half">
	                <div className="banner-content">
	                	<h1 className="wow fadeIn fadeUp">Everything you do comes from your mind. <strong>Take ownership of it.</strong></h1>
	                  <p className="wow fadeIn fadeUp delay-50">Cortex Method is a performance methodology that helps optimise what you are capable of. <br/><br/>Subscribe for exclusive pre-launch offer.</p>
	                  <div className="wow fadeIn fadeUp delay-100">
	              	  	<Link  href="/#signup"><button className="mobile-hide" onClick={this.scrollToMyRef}>Subscribe</button></Link>
	              	  	<Link  href="/#signup"><button className="desktop-hide">Subscribe for 20% off</button></Link>
	              	  </div>
	                </div>
	              </div>
	              <div className="half wow fadeIn">
	                <div className="animating-container large">
	                  <div id="main-blob" className='blob'></div>  
	                </div>
	              </div>
	            </div>
	        </div>
	        <div id="introduction" className="content-section small">
	            <div className="container">
	                <div className="container-sm">
	                  <p className="h3 wow fadeIn fadeUp" data-wow-offset="200">We’ll teach you fundamental principles of performance psychology that are based on decades of research — enabling you to stack probability in the favour of success.</p> 
	                </div>
	            </div>
	        </div>
	        <div id="method" className="content-section slider-extended">
	            <div className="container">
	                <div className="title-area wow fadeIn">
	                  <h3 className="h2 title">The Method</h3> 
	                </div>
	                <div className="wow fadeIn fadeLeft">
	                	<Cards />
	                </div>
	            </div>
	        </div>
	        <div id="backed" className="content-section">
	          <div className="container-sm content-area">
	            <div className="image wow fadeIn fadeUp">
	            	<img className="" src="/static/img/home/content.png" alt="Our Content"  />
	            </div>
	            <h3 className="h2 title wow fadeIn fadeUp">Our content is backed by decades of science.</h3>
	            <div className="text wow fadeIn fadeUp ">
	              Cortex Method is based on research from the worlds most elite and brutal performance environments. 
	            </div>
	            <Link  href="/science">
	            <div className="wow fadeIn fadeUp">
	            	<button>See all</button>
	            </div>
	            </Link>
	          </div>
	        </div> 
	       
	        <Resources />
	        
	        <SignupBannerLarge title={'Subscribe today and receive Our 20% discount. For life.'} background={'/static/img/banners/banner-large.jpg'}/>
      </div>
	</Layout>

	);
  }
}

export default Index