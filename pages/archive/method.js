import Layout from '../../components/Layout';
import BannerCarousel from '../../components/BannerCarousel';
import ContentTabs from '../../components/ContentTabs';
import SignupBanner from '../../components/SignupBanner'; 
 
const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Method extends React.Component {
	componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:      	75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
  	render() {
    
    return (
	  <Layout>
	      <div className="page">
	      	<div className="content-section text-banner">
	      	 	<div className="container">
	      	 		<h2 className="h1">
	      	 		<span className="wow fadeIn fadeUp">We use Performance Psychology</span>
	      	 		<span className="wow fadeIn fadeUp delay-50">to <strong>maximise your potential.</strong></span>
	    	   		</h2>
	    	   	</div>
	    	</div>
	    	<div className="image-banner content-section">
	    		<div className="content">
	    			<div className="container">
	    				<div className="inner">
			    			<h3 className="h3 title wow fadeIn" data-wow-offset="375">
			    				Performance Psychology looks at factors that accelerate human development and create action to expand what’s possible. Its a science of stacking probability in favour of success.
			    			</h3>
			    			<div className="description wow fadeIn" data-wow-offset="300">
			    				Traditionally, performance psychology has been applied in more competitive, high consequence and hostile environments like the military or world class level sports; but we believe it is a science of everyday excellence that can be applied to us all. Whether you are a student, or a software developer, whether you are an athlete or work in mining – whatever your performance domain, the principles of excellence are the same.
			    			</div>
			    		</div>
		    		</div>
	    		</div>
	      		<img className="background" src="/static/img/method/banner-1.jpg" alt="Banner Image"  />
	      	</div>
	      	<div className="image-banner content-section alternative white">
	    		<div className="content">
	    			<div className="container">
	    				<div className="inner">
			    			<h3 className="h3 title wow fadeIn">
			    				Through decades of analysis on human performance research has found that the factors which predict success are not innate or predestined... they are trainable and they can be developed as a skill.
			    			</h3>
			    		</div>
		    		</div>
	    		</div>
	      		<img className="background" src="/static/img/method/banner-3.jpg" alt="Banner Image"  />
	      	</div>
	      	<div className="image-banner content-section">
	    		<div className="content">
	    			<div className="container">
	    				<div className="inner">
			    			<h3 className="h3 title wow fadeIn">
			    				The Cortex Method is about putting together the factors that we know contribute to human performance, allowing us to stack probability in the favour of success.
			    			</h3>
			    			<div className="description wow fadeIn">
			    				This means we utilise all the tools we know, and cover all the bases, to increase the likelihood of achieving what we want to achieve. Conditioning ourselves to create a process to deliver the highest performing versions of ourselves in all facets of our lives
			    			</div>
			    		</div>
		    		</div>
	    		</div>
	      		<img className="background" src="/static/img/method/banner-2.jpg" alt="Banner Image"  />
	      	</div>
	      	<div id="pillars" className="content-section slider-extended">
	      	 	<div className="container">
	      	 		<div className="wow fadeIn fadeLeft">
	      	 			<BannerCarousel />
	      	 		</div>
	    	   	</div>
	    	</div>
	    	<div className="image-banner content-section white">
	    		<div className="content">
	    			<div className="container">
	    				<div className="inner">
			    			<h3 className="h2 title wow fadeIn">
			    				Why Mindfulness
			    			</h3>
			    			<div className="description wow fadeIn small-description">
			    				Mindfulness meditation is central to the Cortex Method because its ability to train our mind for high performance really is unparalleled. It has been adopted with measurable success in everything from elite level sport, to multinational organizations and military special forces. It is like the Swiss Army knife of training your brain for high performance. There are just so many ways it can impact our psychology. That is why this practice is such a central pillar to our performance methodology.
			    			</div>
			    		</div>
		    		</div>
	    		</div>
	      		<img className="background" src="/static/img/method/banner-4.jpg" alt="Banner Image"  />
	      	</div>
	      	<div className="content-section text-banner title-right no-min-height">
	      	 	<div className="container">
		      	 	<p className="overline wow fadeIn">Our Vision</p>
	      	 		<h2 className="h1 title"> 
	      	 			<span className="wow fadeIn fadeUp">Empower <strong>people</strong> in</span>
	      	 			<span className="wow fadeIn fadeUp delay-100">their lives and <strong>pursuits.</strong></span>
	    	   		</h2>
	    	   	</div>
	    	</div>
	    	<div className="content-section tabs wow fadeIn fadeUp" data-wow-offset="300">
	      	 	<div className="container">
	      	 		<ContentTabs />
	    	   	</div>
	    	</div>
	    	<SignupBanner title={'Subscribe today and receive Our 20% discount. For life.'} background={'/static/img/banners/banner-6.jpg'}/>
	      </div>
	    </Layout>
);
  }
}

export default Method