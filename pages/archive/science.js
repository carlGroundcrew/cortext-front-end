import Layout from '../../components/Layout';
import Articles from '../../components/archive/Articles';
import SignupBanner from '../../components/SignupBanner';

const Science = () => 
  <Layout>
      <div className="page science">
        <div className="container">
        	<div className="title-container">
	        	<Articles />
    		</div>
        </div>
        <SignupBanner title={'Subscribe today and receive Our 20% discount. For life.'} background={'/static/img/banners/banner-6.jpg'}/>
      </div>
  </Layout>

export default Science;