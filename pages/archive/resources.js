import Layout from '../../components/Layout';
import ResourceTabs from '../../components/ResourceTabs';

const Resources = () =>
    <Layout page="resources">
      <div className="page">
      <div className="content-section text-banner with-image no-padding-bottom image-right">
      		<div className="container-lg">
      			<div className="half">
	      			<div className="internal-container">
		      	 		<h2 className="h1">
		      	 		<span className="wow fadeIn fadeUp">We want to help you</span>
		      	 		<span className="wow fadeIn fadeUp delay-50">achieve your goals</span>
		    	   		</h2>
		    	   		<p className="wow fadeIn fadeUp delay-100">In order to be sucessful you need to incorporate actions and resources to get to where you want to go.</p>
		    		</div>
		    	</div>
		    	<div className="half">
	    			<img src="/static/img/resources/banner.jpg" alt="Our Content"  />
	    		</div>
	    	</div>
      </div>
      <div className="content-section no-padding-bottom">
      		<ResourceTabs />
      	</div>
      </div>
    </Layout>

export default Resources