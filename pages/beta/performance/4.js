import Layout from '../../../components/beta/LayoutBeta';
import Promotion from "../../../components/beta/AudioClip";
import Link from 'next/link';
import HubspotForm from 'react-hubspot-form'

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

import {fetchBlogs} from '../../../store/actions/actions';

class Beta extends React.Component {

    static async getInitialProps ({ req, reduxStore, pathname, params, query }) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here

        // get blogs
        await reduxStore.dispatch( fetchBlogs ({}) );

        return { };
    }

  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="beta">
      <div className="page" >
        <div className="container-full">
        <div className="beta-session-info wow fadeIn">
            <h4>Component 3</h4>
            <h1>Social Capital</h1>
          </div>
          <Promotion
          titleone={'Building Your Social Capital'}
          audioone={'/static/audio/beta/performance/4-introduction-to-social-capital.mp3'}
          titletwo={'Mindfulness: Training Thought Awareness'}
          audiotwo={'/static/audio/beta/mindfulness/4-training-thought-awareness.mp3'}
           />
          <div className="beta-session-tiles">
            <div className="beta-image-tile wow fadeIn delay-50">
              <img src="../../static/img/beta/beta-hero-4.jpg" />
            </div>
            <div className="beta-content-tile wow fadeIn delay-100">
            <p>You have probably heard the sayings: &ldquo;you are the average of the five people you spend the most time around&rdquo; or &ldquo;show me your friends and I&rsquo;ll show you your future&rdquo;.</p>
<p>They are catchy, but their impact is actually underrated.</p>
<p>We humans are social creatures. We have evolved to unconsciously prioritise group membership and group cohesiveness beyond virtually anything else. The human kind is the predator with one of the highest survial rates, yet we have very small incisors, no claws, and we are not fast. Throughout evolution, we realised our strength is in our group - in our social capital. We realised we can survive because of our numbers and our ability to work together. Evolution has made us hard wired to be highly influenced by those we consider to be our ingroup.&nbsp;</p>
<p>For example, one of the most influential triggers to reduce your personal energy usage is the knowledge of how much energy your neighbour uses. It is natural for us to bring our behaviour in line with what those around us are doing.&nbsp;</p>
<p>You&rsquo;ve probably realised how it is easier to start doing something if those around you are doing it. Think of something simple, like exercise, if the people you spend most time around played football together every day, you would be very inclined to feel the pressure or desire to join.</p>
<p>This is a highly studied area in psychology and the effects go deep.</p>
<p>Research by Professor Alex Haslam suggests that our sense of who we are is based on the groups we associate ourselves to. These groups are dynamic and exist in a range: professional, educational, dog vs cat preference, sports team or friendship group.</p>
<p>Haslam says the groups we identify with &lsquo;furnish us with a sense of self&rsquo;, and therefore have significant influence in our attitudes, beliefs, and behaviour. They effect the way we think and our decision-making which ultimately determines the way we go through our lives.&nbsp;</p>
<p>There is a wealth of evidence that shows how lacking valued social groups or having negative social groups can contribute to conditions such as depression, stress, trauma and addiction.&nbsp;</p>
<p>Your identity is largely influenced through the collection of groups you belong to and the extent to which you identify yourself with them.</p>
<p>Note down for yourself: who are the top five people who inspire you? You may know them or not. They may be alive or not. Note down who they areand what inspires you about them. This will allow you to be more aware of the calibre of people you want to influence you.&nbsp;</p>
<p>With the power social influence has on us, it&rsquo;s important to be aware of and actively choose the groups of people we surround and associate ourselves with. They can either enable us to rise and push further, or they can encourage negative and damaging behaviours. You choose who you spend time with, so be wary of those who might drag you down, and choose people who you admire and want to resemble.</p>
<p>Ask yourself, when the stakes are high and you feel uncertainty and challenge setting in, who would you turn to?</p>
            </div>
            <div className="beta-content-tile wow fadeIn delay-150">
            
            <h3>Performance Protocols</h3>
            <HubspotForm
   portalId='4267461'
   formId='fda6d626-8212-4b78-ac36-a572b0201c09'
   onSubmit={() => console.log('Submit!')}
   onReady={(form) => console.log('Form ready!')}
   loading={<div>Loading...</div>}
   />
   <h3>Resources</h3>
   <ul className="resources">
      <li>
        <a href="https://www.ted.com/talks/nicholas_christakis_the_hidden_influence_of_social_networks?language=en" target="_blank" className="video">TED Talk: The hidden influence of social networks</a>
      </li>
   </ul>
   <h3>Scientific Evidence</h3>
   <ul className="evidence">
      <li>Ashforth, B. E., &amp; Mael, F. (1989). Social Identity Theory and the Organization. The Academy of Management Review, 14(1), 20. doi:10.2307/258189</li>
      <li>Hogg, M. A. (n.d.). Social Identity Theory. Encyclopedia of Leadership. doi:10.4135/9781412952392.n332</li>
      <li>Jabr, F. (2013, October 15). Why Your Brain Needs More Downtime. <a href="https://www.scientificamerican.com/article/mental-downtime/" target="_blank">Source</a></li>
      <li>Mummendey, A., Kessler, T., Klink, A., &amp; Mielke, R. (1999). Strategies to cope with negative social identity: Predictions by social identity theory and relative deprivation theory. Journal of Personality and Social Psychology, 76(2), 229-245. doi:10.1037//0022-3514.76.2.229</li>
      <li>Stets, J. E., &amp; Burke, P. J. (2000). Identity Theory and Social Identity Theory. Social Psychology Quarterly, 63(3), 224. doi:10.2307/2695870</li>
      <li>Tajfel, H. (2010). Social Identity and Intergroup Relations. Cambridge, England: Cambridge University Press.</li>
   </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="beta-tabs">
        <Link>
          <a href="/beta"><span className="session-text">Introduction</span> <span className="session-number">Intro</span></a>
        </Link>
        <Link>
          <a  href="/beta/performance/2"><span className="session-text">Performance Process</span> <span className="session-number">1</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/3"><span className="session-text">Physical Capital</span> <span className="session-number">2</span></a>
        </Link>
        <Link>
          <a className="active" href="/beta/performance/4"><span className="session-text">Social Capital</span> <span className="session-number">3</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/5"><span className="session-text">Mental Factors</span> <span className="session-number">4</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/6"><span className="session-text">Stress Management</span> <span className="session-number">5</span></a>
        </Link>
      </div>
    </Layout>
    );
  }
}


export default Beta