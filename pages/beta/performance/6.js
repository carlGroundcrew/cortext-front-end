import Layout from '../../../components/beta/LayoutBeta';
import Promotion from "../../../components/beta/AudioClip";
import Link from 'next/link'; 
import HubspotForm from 'react-hubspot-form'

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

import {fetchBlogs} from '../../../store/actions/actions';

class Beta extends React.Component {

    static async getInitialProps ({ req, reduxStore, pathname, params, query }) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here

        // get blogs
        await reduxStore.dispatch( fetchBlogs ({}) );

        return { };
    }

  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="beta">
      <div className="page" >
        <div className="container-full">
        <div className="beta-session-info wow fadeIn">
            <h4>Component 5</h4>
            <h1>Stress Management</h1>
          </div>
          <Promotion
          titleone={'Stress as a Good Thing'}
          audioone={'/static/audio/beta/performance/6-stress-as-a-good-thing.mp3'}
          titletwo={'Mindfulness: Training Resilience'}
          audiotwo={'/static/audio/beta/mindfulness/6-training-resilience.mp3'}
           />
          <div className="beta-session-tiles">
            <div className="beta-image-tile wow fadeIn delay-50">
              <img src="../../static/img/beta/beta-hero-6.jpg" />
            </div>
            <div className="beta-content-tile wow fadeIn delay-100">
            <p>Hi and welcome to Cortex on Stress.</p>
<p>Stress is a fundamental part of life, and if you know how to use it &ndash; it can be your greatest ally. This pillar teaches and trains a different approach to stress. Over the course of this pillar we will go through the specific perspectives, techniques and tools to allow you not only to manage stress, but also how to utilise it and ultimately embrace it.</p>
<p>This is important to learn because we have all experienced the ugly sides of stress &ndash; no one is completely immune. As a society, stress is largely viewed as an incredibly damaging thing that erodes our performance, as well as our mental and physical health. We have become hyper vigilant to view stress as a villain.</p>
<p>However, ultimately, stress is an unavoidable aspect of life. No matter how much we try and push it away, it is impossible to live a life without some kind of stress.</p>
<p>This is because stress is a biological response to a challenge &ndash; originally designed as a warning system.</p>
<p>When your brain perceives a stressor, it floods the body with chemicals which increases your blood pressure and heart rate but they also give you laser focus on whats important, drastically improving your memory functioning as well as attention span and motivation. Which can actually be good things. Depending on what you are focusing on directing that attention toward.</p>
<p>Think back to the last time you were feeling moderately stressed and see if you can identify these symptoms.</p>
<p>In fact, research has found that we need stress (or pressure) to perform. To a point, stress helps us, it focuses our behaviour, it gives us energy to act and it improves attention and memory.</p>
<p>With no stress or pressure, we experience boredom. It turns out boredom is equally as detrimental to performance (and health) as too much pressure (Yerkes-Dodson Law).</p>
<p>Boredom isn&rsquo;t just taxing on our performance, it aggravates emotional and physical pain too; Boredom makes you feel fatigued, apathetic, anxious and unfocused.</p>
<p>We, as a species are not designed to tolerate boredom, we are designed to continually improve and evolve. Content homo-erectus was eaten, our ancestors are the ones who kept moving forward.</p>
<p>So, where is it all going wrong? If stress can be a good thing why are 75 million people a year being prescribed medication for stress related disorders. There&rsquo;s got to be something else at play here.</p>
<p>Turns out, there is a tipping point where stress moves from motivating and focusing to harmful and damaging.</p>
<p>Prior to this point, when we are optimally challenged, we operate in a peak performance zone: where we can achieve optimal performance, happiness and health.</p>
<p>A decades long study out of Oxford University found that stress itself has a variety of health benefits. It found that reasonable levels of stress can improve immune functioning and also help improve memory and learning ability through cortisol&rsquo;s ability to focus the mind and improve short term memory.</p>
<p>However, the catch between this beneficial health and performance enhancing stress and the detrimental stress hinges on one big thing: your ability to manage it.</p>
<p>Manage your ability to stay at that peak without tipping over, or knowing how to bring yourself back to the productive side of the curve.</p>
<p>This pillar aims to teach you the way science, evidence and the elite utilise stress as a resource and manage their stress to keep themselves in the peak performance zone.</p>
<p>So, as you go through this kind of psychological training remember that the goal is to make you better at stress, not get rid of stress.</p>
<p>Don&rsquo;t worry at all if you find this challenging or it doesn&rsquo;t become automatic straight away, that&rsquo;s perfectly normal when you are learning something new &ndash; particularly when it comes to retraining your mind in this way. We still have a lot more to cover, introduce and develop. There is quite a bit of context we have to put around it to really get the best out of it. There&rsquo;s also some exercises for everyday life to try and bring some perspective and control to those difficult situations in life.</p>
<p>But we will get into that later.</p>
<p>I&rsquo;m looking forward to seeing how this journey impacts you. Until next time.</p>
            </div>
            <div className="beta-content-tile wow fadeIn delay-150">
            <h3>Performance Protocols</h3>
            <HubspotForm
   portalId='4267461'
   formId='03c38a4d-653f-436f-941c-734654368a0f'
   onSubmit={() => console.log('Submit!')}
   onReady={(form) => console.log('Form ready!')}
   loading={<div>Loading...</div>}
   />
   <h3>Resources</h3>
   <ul className="resources">
      <li>
        <a href="https://www.youtube.com/watch?v=RcGyVTAoXEU&t=147s" target="_blank" className="video">TED Talk: How to make stress your friend</a>
      </li>
   </ul>
   <h3>Scientific Evidence</h3>
   <ul className="evidence">
      <li>Berzin, R. (2016, July 14). The Stress Epidemic and the Search for the Modern Cure. <a href="https://www.huffpost.com/entry/the-stress-epidemic-and-the-search-for-the-modern-cure_b_7756940" target="_blank">Source</a></li>
      <li>Harvard Health Publishing. (2018, May 2). Understanding the stress response - Harvard Health. <a href="https://www.health.harvard.edu/staying-healthy/understanding-the-stress-response" target="_blank">Source</a></li>
      <li>Klein, S. (2013, April 19). The 3 Major Stress Hormones, Explained. <a href="https://www.huffingtonpost.com.au/2013/04/19/adrenaline-cortisol-stress-hormones_n_3112800.html?ec_carp=2690878253821043577" target="_blank">Source</a></li>
      <li>McGonigal, K. (n.d.). How to make stress your friend. <a href="http://www.yk.rim.or.jp/~akino/material/Howtomake.pdf" target="_blank">Source</a></li>
      <li>McIntyre, C., &amp; Roozendaal, B. (2007). Adrenal Stress Hormones and Enhanced Memory for Emotionally Arousing Experiences. Neural Plasticity and Memory, 265-283. doi:10.1201/9781420008418.ch13</li>
      <li>Parker, C. B. (2015, May 7). Embracing stress is more important than reducing stress, Stanford psychologist says. <a href="https://news.stanford.edu/2015/05/07/stress-embrace-mcgonigal-050715" target="_blank">Source</a></li>
   </ul>
            
            </div>
          </div>
        </div>
      </div>
      <div className="beta-tabs">
        <Link>
          <a href="/beta"><span className="session-text">Introduction</span> <span className="session-number">Intro</span></a>
        </Link>
        <Link>
          <a  href="/beta/performance/2"><span className="session-text">Performance Process</span> <span className="session-number">1</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/3"><span className="session-text">Physical Capital</span> <span className="session-number">2</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/4"><span className="session-text">Social Capital</span> <span className="session-number">3</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/5"><span className="session-text">Mental Factors</span> <span className="session-number">4</span></a>
        </Link>
        <Link>
          <a className="active" href="/beta/performance/6"><span className="session-text">Stress Management</span> <span className="session-number">5</span></a>
        </Link>
      </div>
    </Layout>
    );
  }
}


export default Beta