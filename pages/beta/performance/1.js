import Layout from '../../../components/beta/LayoutBeta';
import Promotion from "../../../components/beta/AudioClip";
import Link from 'next/link'; 

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

import {fetchBlogs} from '../../../store/actions/actions';

class Beta extends React.Component {

    static async getInitialProps ({ req, reduxStore, pathname, params, query }) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here

        // get blogs
        await reduxStore.dispatch( fetchBlogs ({}) );

        return { };
    }

  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="beta">
      <div className="page" >
        <div className="container-full">
          <Promotion audio={'/static/audio/beta/performance/1-introduction-to-cortex-method.mp3'} />
          <div className="beta-session-info wow fadeIn">
            <h4>Session 1</h4>
            <h1>Introduction to the Cortex Method 
 <br /> Performance Process</h1>
            <p>Session length: 3 minutes</p>
            <div className="session-toggle">
              <Link><a href="/beta" className="active">Performance</a></Link>
              <Link><a href="/beta/mindfulness/1">Mindfulness</a></Link>
            </div>
          </div>
          <div className="beta-session-tiles">
            <div className="beta-image-tile wow fadeIn delay-50">
              <img src="../../static/img/home/person.jpg" />
            </div>
            <div className="beta-content-tile wow fadeIn delay-100">
            <p>We are here to train your mind, perspectives and behaviour to stack probability in the favour of success.</p>
            <p>Performance Psychology is a field that looks at what it takes to maximize potential. It looks at what the factors are that accelerate human development and create action to expand on what is possible for an individual.</p>
            <p>Traditionally, performance psychology has been applied in more hostile, competitive and high consequence environments like high risk military or world class level sports but we believe it is a science of everyday excellence that can be applied to us all.&nbsp;Whether you are a student, or a software developer, whether you are an athlete or work in the mines &ndash; whatever your performance domain, the principles of excellence are the same.</p>
            <p>It&rsquo;s through decades of this analysis on human performance that research has found that the factors that predict success are not innate or predestined&hellip; they are trainable and they can be developed as a skill.</p>
            <p>This is the training the Cortex Method aims to give you; the knowledge of the science of high performance and the insight to apply it.</p>
            <p>We have developed a methodology to give guidance to those on the path of high performance. This methodology is designed to be holistic and ongoing - it is a practical way of approaching your ambitions armed with the tools and techniques to manage yourself along the way. It isn&rsquo;t designed as a once off education, it&rsquo;s a Constitution of Performance to regularly refer back to. Because as you evolve, your performance, demands and resources will evolve. And there will always be a way of levelling up.</p>
            <p>If you are on the path of high performance, you have to accept it will not be easy. Performance is supposed to be challenging. What we aim to do here is to ensure you have the mental arsenal to fight back when challenge comes your way.</p>
            <p>But this pursuit of performance will fail before its started without your unwavering and relentless commitment to excellence. Engagement, discipline and consistency is the only way that success is every achieved, in anything.</p>
            <p>So ask yourself, what is your purpose for wanting to improve your psychological resources and your performance? How committed are you, really, to training your mind and in doing so challenging your performance potential? How much mental energy are you willing to dedicate to it?</p>
            <p>It&rsquo;s a long and hard journey, in fact its a lifelong pursuit. But we hope we can be there to support and inspire you in any way we can.</p>
            </div>
            <div className="beta-content-tile wow fadeIn delay-150">
              <h3>Performance Protocols</h3>
            <table class="t1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="td1" valign="top">
<p class="p1"><span class="s1"><strong>Performance Protocols</strong></span></p>
</td>
<td class="td1" valign="top">
<ul class="ul1">
<li class="li1"><span class="s1">What your purpose for engaging with Cortex?</span></li>
<li class="li1"><span class="s1">What you are hoping to get out of it?</span></li>
<li class="li1"><span class="s1">What is your intention regarding your engagement. When will you engage with Cortex training? How will you incorporate these principles into your daily life?</span></li>
</ul>
<p class="p2">&nbsp;</p>
</td>
</tr>
<tr>
<td class="td2" valign="top">
<p class="p1"><span class="s1"><strong><em>Resources</em></strong></span></p>
</td>
<td class="td2" valign="top">
<p class="p1"><span class="s1"><em>The Compound Effect by Darren Hardy, TED Talk: How to achieve your most ambitious goals by Stephen Duneier </em></span></p>
</td>
</tr>
</tbody>
</table>
            </div>
          </div>
        </div>
      </div>
      <div className="beta-tabs">
        <Link>
          <a className="active" href="/beta"><span className="session-text">Session</span> <span className="session-number">1</span> <span className="beta-cat">Performance</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/2"><span className="session-text">Session</span> <span className="session-number">2</span> <span className="beta-cat">Performance</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/3"><span className="session-text">Session</span> <span className="session-number">3</span> <span className="beta-cat">Performance</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/4"><span className="session-text">Session</span> <span className="session-number">4</span> <span className="beta-cat">Performance</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/5"><span className="session-text">Session</span> <span className="session-number">5</span> <span className="beta-cat">Performance</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/6"><span className="session-text">Session</span> <span className="session-number">6</span> <span className="beta-cat">Performance</span></a>
        </Link>
      </div>
    </Layout>
    );
  }
}


export default Beta