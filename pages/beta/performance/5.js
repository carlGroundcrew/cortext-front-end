import Layout from '../../../components/beta/LayoutBeta';
import Promotion from "../../../components/beta/AudioClip";
import Link from 'next/link';
import HubspotForm from 'react-hubspot-form'

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

import {fetchBlogs} from '../../../store/actions/actions';

class Beta extends React.Component {

    static async getInitialProps ({ req, reduxStore, pathname, params, query }) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here

        // get blogs
        await reduxStore.dispatch( fetchBlogs ({}) );

        return { };
    }

  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="beta">
      <div className="page" >
        <div className="container-full">
        <div className="beta-session-info wow fadeIn">
            <h4>Component 4</h4>
            <h1>Mental Factors</h1>
          </div>
          <Promotion
          titleone={'Introduction to Mental Factors'}
          audioone={'/static/audio/beta/performance/5-Introduction-to-mental-factors.mp3'}
          titletwo={'Mindfulness: Training Motivation'}
          audiotwo={'/static/audio/beta/mindfulness/5-training-motivation.mp3'}
           />
          <div className="beta-session-tiles">
            <div className="beta-image-tile wow fadeIn delay-50">
              <img src="../../static/img/beta/beta-hero-5.jpg" />
            </div>
            <div className="beta-content-tile wow fadeIn delay-100">
            <p>Your mental state colours everything you experience, because your experiences are filtered through your mind. Therefore, to develop a strong mental constitution and to train our mental factors is essential if we want to be able to perform professionally and in every day life.</p>
<p>Anything you want to be and anything you want to achieve starts in your mind. Any and all decisions come from the mind. Our decisions determine our actions, and our actions determine our lives.</p>
<p>The strength of our psychological capital is defined by our:</p>
<ul>
  
  <li>Confidence to take on challenges and willingness to put in the necessary effort to succeed</li>
  <li>Optimism about our ability to succeed now and in the future</li>
  <li>Perseverance toward goals</li>
  <li>Ability to bounce back and grow from adversity to achieve success <br />(Luthans, Youssef et al., 2007, p. 3).</li>
</ul>
<p>A study out of Philadelphia University found that psychological capital accounted for up to 29% performance (Durval et al., 2016). It predicts productivity, resilience, wellbeing, and creativity.</p>
<p>Those with a high performance mindset and strong psychological capital tend to be more ambitious than those who lack mental strength. They aspire to be better than they currently are, and not only do they know they can improve, but they are also proactive and relentless in their pursuit of improvement.</p>
<p>Most importantly, psychological capital is NOT a trait we are born with. Research suggests it is a collection of skills and perspectives that can be trained and learned (Luthans &amp; Morgan, 2017).</p>
<p>This training will be delivered in this pillar. You will gain an understanding of the psychological factors necessary to build mental strength, and the tactical ability to apply them.</p>
            </div>
            <div className="beta-content-tile wow fadeIn delay-150">
            
            <h3>Performance Protocols</h3>
            <HubspotForm
   portalId='4267461'
   formId='87561ea3-9228-4adb-acf1-56bbc42094ee'
   onSubmit={() => console.log('Submit!')}
   onReady={(form) => console.log('Form ready!')}
   loading={<div>Loading...</div>}
   />
   <h3>Resources</h3>
   <ul className="resources">
      <li>
        <a href="https://www.youtube.com/watch?v=LNHBMFCzznE" target="_blank" className="video">TED Talk: After watching this, your brain will not be the same</a>
      </li>
   </ul>
   <h3>Scientific Evidence</h3>
   <ul className="evidence">
      <li>Ardichvili, A. (2011). Invited reaction: Meta-analysis of the impact of psychological capital on employee attitudes, behaviors, and performance. Human Resource Development Quarterly, 22(2), 153-156. doi:10.1002/hrdq.20071</li>
      <li>Levesque, C., &amp; Brown, K. W. (2007). Mindfulness as a moderator of the effect of implicit motivational self-concept on day-to-day behavioral motivation. Motivation and Emotion, 31(4), 284-299. doi:10.1007/s11031-007-9075-8</li>
      <li>Livi, S., Alessandri, G., Caprara, G. V., &amp; Pierro, A. (2015). Positivity within teamwork: Cross-level effects of positivity on performance. Personality and Individual Differences, 85, 230-235. doi:10.1016/j.paid.2015.05.015</li>
      <li>Losada, M., &amp; Heaphy, E. (2004). The Role of Positivity and Connectivity in the Performance of Business Teams. American Behavioral Scientist, 47(6), 740-765. doi:10.1177/0002764203260208</li>
      <li>Slagter, H. A., Lutz, A., Greischar, L. L., Francis, A. D., Nieuwenhuis, S., Davis, J. M., &amp; Davidson, R. J. (2007). Mental Training Affects Distribution of Limited Brain Resources. PLoS Biology, 5(6), e138. doi:10.1371/journal.pbio.0050138</li>
   </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="beta-tabs">
        <Link>
          <a href="/beta"><span className="session-text">Introduction</span> <span className="session-number">Intro</span></a>
        </Link>
        <Link>
          <a  href="/beta/performance/2"><span className="session-text">Performance Process</span> <span className="session-number">1</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/3"><span className="session-text">Physical Capital</span> <span className="session-number">2</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/4"><span className="session-text">Social Capital</span> <span className="session-number">3</span></a>
        </Link>
        <Link>
          <a className="active" href="/beta/performance/5"><span className="session-text">Mental Factors</span> <span className="session-number">4</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/6"><span className="session-text">Stress Management</span> <span className="session-number">5</span></a>
        </Link>
      </div>
    </Layout>
    );
  }
}


export default Beta