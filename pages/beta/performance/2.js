import Layout from '../../../components/beta/LayoutBeta';
import Promotion from "../../../components/beta/AudioClip";
import Link from 'next/link'; 
import HubspotForm from 'react-hubspot-form'

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

import {fetchBlogs} from '../../../store/actions/actions';

class Beta extends React.Component {

    static async getInitialProps ({ req, reduxStore, pathname, params, query }) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here

        // get blogs
        await reduxStore.dispatch( fetchBlogs ({}) );

        return { };
    }

  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="beta">
      <div className="page" >
        <div className="container-full">
          <div className="beta-session-info wow fadeIn">
            <h4>Component 1</h4>
            <h1>Performance Process</h1>
          </div>
          <Promotion
          titleone={'Developing a direction'}
          audioone={'/static/audio/beta/performance/2-developing-a-direction-pt1.mp3'}
          titletwo={'Mindfulness: Training Focus'}
          audiotwo={'/static/audio/beta/mindfulness/2-training-focus.mp3'}
           />
          
          <div className="beta-session-tiles">
            <div className="beta-image-tile wow fadeIn delay-50">
              <img src="../../static/img/beta/beta-hero-2.jpg" />
            </div>
            <div className="beta-content-tile wow fadeIn delay-100">
            <p>Today, we are going to start the conversation about developing a direction.</p>
<p>This is always a good place to start because without knowing where you are trying to get to and what you are trying to achieve, the likelihood of you getting there is negligible. And even if you did get there, you wouldn&rsquo;t know it.</p>
<p>Without goals, we are floating through life subject to wherever the breeze blows us &ndash; or wherever anyone around us, leads us. We can end up feeling demotivated, trapped or worse still, waking up one day and realising your career or your life, is not one that you chose. With goals, our behaviour is targeted, meaningful and intentional. We become active agents of change in our lives.</p>
<p>Setting goals is like pushing the first domino in the success process, it creates this kind of momentum and increases motivation and effort. With increased effort then comes a greater likelihood of success, success builds self esteem and drive which leads to the desire to set bigger goals. (Hall &amp; Foster, 2017)</p>
<p>Would you imagine Olympians or special operations units not knowing what they are aiming for? Of course not, they know intricately what it feels like, looks like, tastes like when they are performing to their potential and they constantly focus on performing to that.</p>
<p>That for you, should be no different. You will not get anywhere meaningful unless you have a meaningful direction and a compelling image to work toward.</p>
<p>Really, it&rsquo;s not a matter of whether or not we just have goals &ndash; otherwise every new year we would hit every resolution we make. It&rsquo;s about whether our goals inspire us, and create emotion and drive inside of us. A great goal is something that you find meaning in.</p>
<p>Take time, think it through, adapt and change it as you grow.&nbsp;</p>
<p>You&rsquo;ll know it&rsquo;s a compelling goal if, even though it might seem impossible now, it inspires you enough to know you&rsquo;ll figure out how to make it happen and how to become the person capable of making it happen.</p>
<p>There is a huge amount of power that comes from designing a compelling direction.</p>
<p>You don&rsquo;t need to take my word for this, the effectiveness of having a vision or a direction is grounded in your biology.</p>
<p>There is a part of your brain called the Reticular Activating System and part of what it&rsquo;s responsible for is controlling what we pay attention to and what we don&rsquo;t. In our daily life we are exposed to an overwhelming amount of information, it is not possible for us to pay attention to it all. Your RAS acts as a filter or a net &ndash; and what filters through to your awareness rests largely on what is important to you. If it picks up on something important to you, it sends a signal to your conscious brain at which point it triggers your awareness.</p>
<p>This is why you can hear your name from across a crowded room., where all other conversations are filtered out.</p>
<p>Another example of this is when you buy a new car or a new shirt and suddenly you see that car everywhere! This isn&rsquo;t because coincidentally the whole of the world around you went out and bought that car on the same weekend you did &ndash; it&rsquo;s because that car is now an important part of your identity and therefore your brain is primed to pick up on it.</p>
<p>The more we know about what is important to us, what we want to achieve and our values, the more we will be aware of opportunities to bring us closer to achieving it.</p>
<p>You need to develop a concrete and specific understanding of the outcome you want to achieve but more importantly an understanding of how you want to achieve it, what you are doing, how you feel and who you are with.</p>
<p>You are capable of changing your life and your performance on every level in every area in any direction, but first you must know where you want to go.</p>
<p>Through continually imagining and reinforcing what success looks like for you and what is important for you &ndash; you are priming your brain to create that. To enact those behaviours and to be more aware of opportunities to lead you there.</p>
<p>So taking a moment to practice this now. Close your eyes, and take 3 slow deep breaths.<br />Let everything else fade into the background of your mind, and feel yourself sinking into the chair &ndash; all your muscles relaxing, your mind softening. Letting go of anything you are holding on to.</p>
<p>Bringing to the forefront of your mind, the best possible version of yourself. Achieving what you want to achieve, feeling optimally challenged but stepping up to that challenge.</p>
<p>This might be in 10 years time or more generally in your life now.</p>
<p>And just picture this unfolding in your mind. Almost as though you were watching it as a movie on a screen in front of you.</p>
<p>And take a moment to notice: what are you doing? Who are you with? How do you feel? Just exploring this for a few moments.</p>
<p>When you are ready, opening your eyes.</p>
<p>So before you jump back into daily life. Take a moment to write down what it was that came to your mind. You at your most optimal.</p>
<p>Taking a mental note, and developing greater awareness of what is important to you in your performance in your career. Taking that first step in gaining your bearings and developing your direction.</p>
<p>This exercise is an imagery technique that is highly utilised in sport. In fact it&rsquo;s one of the defined psychological characteristics of developing excellence. Imagery is a way of mentally rehearsing habits, perspectives or developing skills, whenever you need, as often as you need. Impacting your understanding, preparedness and consistency of your performance.</p>
<p>It uses your brains circuitry to enable your preparedness to perform. It&rsquo;s something to come back to regularly. Checking in and reflecting on what your optimal performance looks like, and then relentlessly moving toward that. Relentless reflection and understanding of what you are aiming for, and then relentless improvement toward that.</p>
<p>For the next 7 days, try to do this imagery exercise daily. Just take 1 or 2 minutes out of your day, to imagine the best possible version of yourself &ndash; you at your optimal.</p>
            </div>
            <div className="beta-content-tile wow fadeIn delay-150">
            <h3>Performance Protocols</h3>
            <HubspotForm
   portalId='4267461'
   formId='c47b3063-2c52-43ef-b61c-94996fb35f6e'
   onSubmit={() => console.log('Submit!')}
   onReady={(form) => console.log('Form ready!')}
   loading={<div>Loading...</div>}
   />
   <h3>Resources</h3>
   <ul className="resources">
      <li>
        <a href="https://www.ted.com/talks/larry_smith_why_you_will_fail_to_have_a_great_career" target="_blank" className="video">TED Talk: Why you will fail to have a great career by Larry Smith</a>
      </li>
   </ul>
   <h3>Scientific Evidence</h3>
   <ul className="evidence">
      <li>Hall, D. T., &amp; Foster, L. W. (1977). A Psychological Success Cycle and Goal Setting: Goals, Performance, and Attitudes. Academy of Management Journal, 20(2), 282-290. doi:10.2307/255401</li>
      <li>Martin, K. A., &amp; Hall, C. R. (1995). Using Mental Imagery to Enhance Intrinsic Motivation. Journal of Sport and Exercise Psychology, 17(1), 54-69. doi:10.1123/jsep.17.1.54</li>
      <li>Murphy, S. M. (2012). The Oxford Handbook of Sport and Performance Psychology. New York, NY: Oxford University Press.</li>
      <li>Raúl, H. (2012). Reticular Mechanisms of Sensory Control. Sensory Communication, 497-520. doi:10.7551/mitpress/9780262518420.003.0026</li>
      <li>Schultheiss, O. C., &amp; Brunstein, J. C. (1999). Goal Imagery: Bridging the Gap Between Implicit Motives and Explicit Goals. Journal of Personality, 67(1), 1-38. doi:10.1111/1467-6494.00046</li>
      <li>Sheldon, K. M., &amp; Lyubomirsky, S. (2006). How to increase and sustain positive emotion: The effects of expressing gratitude and visualizing best possible selves. The Journal of Positive Psychology, 1(2), 73-82. doi:10.1080/17439760500510676</li>
   </ul>
            
            </div>
          </div>
        </div>
      </div>
      <div className="beta-tabs">
        <Link>
          <a href="/beta"><span className="session-text">Introduction</span> <span className="session-number">Intro</span></a>
        </Link>
        <Link>
          <a className="active" href="/beta/performance/2"><span className="session-text">Performance Process</span> <span className="session-number">1</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/3"><span className="session-text">Physical Capital</span> <span className="session-number">2</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/4"><span className="session-text">Social Capital</span> <span className="session-number">3</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/5"><span className="session-text">Mental Factors</span> <span className="session-number">4</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/6"><span className="session-text">Stress Management</span> <span className="session-number">5</span></a>
        </Link>
      </div>
    </Layout>
    );
  }
}


export default Beta