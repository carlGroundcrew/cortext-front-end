import Layout from '../../../components/beta/LayoutBeta';
import Promotion from "../../../components/beta/AudioClip";
import Link from 'next/link';
import HubspotForm from 'react-hubspot-form'

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

import {fetchBlogs} from '../../../store/actions/actions';

class Beta extends React.Component {

    static async getInitialProps ({ req, reduxStore, pathname, params, query }) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here

        // get blogs
        await reduxStore.dispatch( fetchBlogs ({}) );

        return { };
    }

  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="beta">
      <div className="page" >
        <div className="container-full">
        <div className="beta-session-info wow fadeIn">
            <h4>Component 2</h4>
            <h1>Physical Capital</h1>
          </div>
          <Promotion
          titleone={'Introduction to Physical Capital'}
          audioone={'/static/audio/beta/performance/3-introduction-to-physical-capital.mp3'}
          titletwo={'Mindfulness: Training Emotion Regulation'}
          audiotwo={'/static/audio/beta/mindfulness/3-training-emotion-regulation.mp3'}
           />
          <div className="beta-session-tiles">
            <div className="beta-image-tile wow fadeIn delay-50">
              <img src="../../static/img/beta/beta-hero-3.jpg" />
            </div>
            <div className="beta-content-tile wow fadeIn delay-100">
            <p>Hi and welcome to Cortex Physical Capital.</p>
<p>This pillar will look at how to build and maintain your energy.</p>
<p>You could hold the greatest potential in your field but if you do not have the energy to apply it and develop it &ndash; you will fall short.</p>
<p>Don&rsquo;t think of this pillar as an obvious optional &ldquo;if I feel like it/ if I have time&rdquo;. Think of this pillar as one of the most, if not the most important ingredient in your life.</p>
<p>A good way of thinking of this is building a strong capital base &ndash; if you have strong capital, you will have a greater capacity to apply it to areas you care about; if you have a weak capital base, you will struggle.</p>
<p>The Cortex Method is all about putting together the things that we know contribute to human performance to stack probability in the favour of success. This means we utilise all the tools we know and cover all the bases to increase the likelihood of achieving what we want to achieve. We condition ourselves to create a process to deliver the highest performing versions of ourselves in all facets of our lives.</p>
<p>I can guarantee you with a level of scientific certainty that your life will be better, easier, more enjoyable, more passionate, more creative and more fulfilling if you make your physical capital a priority where you haven&rsquo;t before. How could I know that about you? Because it&rsquo;s a quantifiable fact.</p>
<p>More than this, drawdowns in physical capital often impact things like mood and resilience.&nbsp;</p>
<p>Build your physical capital. If you have enough physical capital in the bank, everything else you could dream of is available to you. But if your physical capital is depleted, everything you try to do (getting out of bed, turning on the kettle &ndash; everything) will be an uphill battle.&nbsp;</p>
<p>How could we aspire to be the best high performance versions of ourselves if we don&rsquo;t even have the physical capital to spend on daily living? Let alone creativity, patience, growth, improvement, passion and positivity?</p>
<p>The body and the mind are inextricably integrated, one feeds off the other. So it is very important to pay close attention to caring for and building our physical capital.</p>
<p class="p1"><span class="s1">If you have poor physical energy, it shows up as:</span></p>
<ul class="ul1">
<li class="li1"><span class="s1">Increased fatigue and irritability</span></li>
<li class="li1"><span class="s1">Reduced ability to cope under stress</span></li>
<li class="li1"><span class="s1">Increased susceptibility to depression</span></li>
<li class="li1"><span class="s1">Increased issues with concentration, attention and focus</span></li>
<li class="li1"><span class="s1">Reduced resilience, proactivity, perseverance and creativity</span></li>
<li class="li1"><span class="s1">Increased issues with social connections, relationships and communication</span></li>
<li class="li1"><span class="s1">As well as increased issues with problem solving, complex thought, emotional intelligence, self esteem, critical thinking, planning, organisation, discipline, judgement, anxiety as well as long and short term memory&nbsp;</span></li>
</ul>
<p>If that sounds familiar to you, you need to make your physical capital a priority &ndash; or you are simply working against yourself.</p>
<p>For the next 5 days, I challenge you to take note of your energy. It might be useful to score your physical, emotional and mental energy out of 10. Then reflect on what you did that day, and the day preceding. What did you eat? Did you exercise? How was your sleep? And did you have any down time.</p>
<p>Start to notice the patterns that occur. What leads you to greater physical capital and what depletes it. It&rsquo;s about developing a strong process you can lean on to support your physical and mental functioning. To fuel you.</p>
<p>Investing in the health of your body is not an optional component of performance &ndash; how you treat your body will bear repercussions to the state of your mind, your ability to regulate your emotions and the quality of the decisions you will make.</p>
<p>It&rsquo;s not about restriction and guilt &ndash; it&rsquo;s about investing in your body to enhance what you are capable of and how you experience the world. It&rsquo;s about seeing food as fuel for you to experience your life, it&rsquo;s about developing good habits and about all you are going to gain for yourself.</p>
<p>NOTHING will impact your mindset as dramatically (on a biological and a chemical level) as what you eat, how you exercise and the quality of your rest.</p>
<p>It&rsquo;s time to step up your game. </p>
            </div>
            <div className="beta-content-tile wow fadeIn delay-150">

<h3>Performance Protocols</h3>
            <HubspotForm
   portalId='4267461'
   formId='a5c82b97-78af-4ccf-b23d-5905c0ea88f1'
   onSubmit={() => console.log('Submit!')}
   onReady={(form) => console.log('Form ready!')}
   loading={<div>Loading...</div>}
   />
   <h3>Resources</h3>
   <ul className="resources">
      <li>
        <a href="https://hbr.org/2001/01/the-making-of-a-corporate-athlete" target="_blank" className="article">The Making of a Corporate Athlete</a>
      </li>
      <li>
        <a className="video" href="https://www.youtube.com/watch?v=XOU2ubWkoPw" target="_blank">TED Talk: how to fix the exhausted brain by Brady Wilson</a>
      </li>
      <li>
        <a className="video" href="https://www.youtube.com/watch?v=BHY0FxzoKZE&feature=youtu.be" target="_blank">TED Talk: The brain-changing benefits of exercise | Wendy Suzuki</a>
      </li>
   </ul>
   <h3>Scientific Evidence</h3>
   <ul className="evidence">
      <li>Get active for mental wellbeing. (2017, December 21). <a href="https://www.nhs.uk/conditions/stress-anxiety-depression/mental-benefits-of-exercise/">Source</a></li>
      <li>Childs, E., &amp; De Wit, H. (2014). Regular exercise is associated with emotional resilience to acute stress in healthy adults. Frontiers in Physiology, 5. doi:10.3389/fphys.2014.00161</li>
      <li>Fox, K. R. (1999). The influence of physical activity on mental well-being. Public Health Nutrition, 2(3a). doi:10.1017/s1368980099000567</li>
      <li>Hartfiel, E. (2009). The Effectiveness of Yoga for Reducing Stress and Enhancing Emotional Well-Being in the Workplace. EXPLORE, 5(3), 149. doi:10.1016/j.explore.2009.03.018</li>
      <li>Maddi, S. R. (2006). Hardiness: The courage to grow from stresses. The Journal of Positive Psychology, 1(3), 160-168. doi:10.1080/17439760600619609</li>
   </ul>
            
            
            </div>
          </div>
        </div>
      </div>
      <div className="beta-tabs">
        <Link>
          <a href="/beta"><span className="session-text">Introduction</span> <span className="session-number">Intro</span></a>
        </Link>
        <Link>
          <a  href="/beta/performance/2"><span className="session-text">Performance Process</span> <span className="session-number">1</span></a>
        </Link>
        <Link>
          <a className="active" href="/beta/performance/3"><span className="session-text">Physical Capital</span> <span className="session-number">2</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/4"><span className="session-text">Social Capital</span> <span className="session-number">3</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/5"><span className="session-text">Mental Factors</span> <span className="session-number">4</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/6"><span className="session-text">Stress Management</span> <span className="session-number">5</span></a>
        </Link>
      </div>
    </Layout>
    );
  }
}


export default Beta