import Layout from '../../../components/beta/LayoutBeta';
import Promotion from "../../../components/beta/AudioClip";
import Link from 'next/link'; 

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

import {fetchBlogs} from '../../../store/actions/actions';

class Beta extends React.Component {

    static async getInitialProps ({ req, reduxStore, pathname, params, query }) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here

        // get blogs
        await reduxStore.dispatch( fetchBlogs ({}) );

        return { };
    }

  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="beta">
      <div className="page" >
        <div className="container-full">
          <Promotion audio={'/static/audio/beta/mindfulness/4-training-thought-awareness.mp3'} />
          <div className="beta-session-info wow fadeIn">
            <h4>Session 6</h4>
            <h1>Mindfulness: 
 <br />Training Resilience </h1>
            <p>Session length: 6 minutes</p>
            <div className="session-toggle">
              <Link><a href="/beta/performance/6">Performance</a></Link>
              <Link><a href="/beta/mindfulness/6" className="active">Mindfulness</a></Link>
            </div>
          </div>
          <div className="beta-session-tiles">
            <div className="beta-image-tile wow fadeIn delay-50">
              <img src="../../static/img/beta/beta-hero-6.jpg" />
            </div>
            <div className="beta-content-tile wow fadeIn delay-100">
            
            </div>
            
          </div>
        </div>
      </div>
      <div className="beta-tabs">
        <Link>
          <a href="/beta"><span className="session-text">Session</span> <span className="session-number">1</span> <span className="beta-cat">Mindfulness</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/2"><span className="session-text">Session</span> <span className="session-number">2</span> <span className="beta-cat">Mindfulness</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/3"><span className="session-text">Session</span> <span className="session-number">3</span> <span className="beta-cat">Mindfulness</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/4"><span className="session-text">Session</span> <span className="session-number">4</span> <span className="beta-cat">Mindfulness</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/5"><span className="session-text">Session</span> <span className="session-number">5</span> <span className="beta-cat">Mindfulness</span></a>
        </Link>
        <Link>
          <a className="active" href="/beta/performance/6"><span className="session-text">Session</span> <span className="session-number">6</span> <span className="beta-cat">Mindfulness</span></a>
        </Link>
      </div>
    </Layout>
    );
  }
}


export default Beta