import Layout from '../../components/beta/LayoutBeta';
import Promotion from "../../components/beta/AudioClip";
import Link from 'next/link'; 
import HubspotForm from 'react-hubspot-form'

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null

import {fetchBlogs} from '../../store/actions/actions';

class Beta extends React.Component {

    static async getInitialProps ({ req, reduxStore, pathname, params, query }) {
        // All data that will be used for SSR (needed for SEO) needs to be fetch here

        // get blogs
        await reduxStore.dispatch( fetchBlogs ({}) );

        return { };
    }

  componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
    render() {
    
    return (
    <Layout page="beta">
      <div className="page" >
        <div className="container-full">
        <div className="beta-session-info wow fadeIn">
            <h1>Introduction to <br />Cortex Method</h1>
          </div>
          <Promotion
          titleone={'Introduction to Cortex Method'}
          audioone={'/static/audio/beta/performance/1-introduction-to-cortex-method.mp3'}
          titletwo={'Introduction to Mindfulness'}
          audiotwo={'/static/audio/beta/mindfulness/1-introduction-to mindfulness.mp3'}
           />
          
          <div className="beta-session-tiles">
            <div className="beta-image-tile wow fadeIn delay-50">
              <img src="../../static/img/home/person.jpg" />
            </div>
            <div className="beta-content-tile wow fadeIn delay-100">
            <p>We are here to train your mind, perspectives and behaviour to stack probability in the favour of success.</p>
            <p>Performance Psychology is a field that looks at what it takes to maximize potential. It looks at what the factors are that accelerate human development and create action to expand on what is possible for an individual.</p>
            <p>Traditionally, performance psychology has been applied in more hostile, competitive and high consequence environments like high risk military or world class level sports but we believe it is a science of everyday excellence that can be applied to us all.&nbsp;Whether you are a student, or a software developer, whether you are an athlete or work in the mines &ndash; whatever your performance domain, the principles of excellence are the same.</p>
            <p>It&rsquo;s through decades of this analysis on human performance that research has found that the factors that predict success are not innate or predestined&hellip; they are trainable and they can be developed as a skill.</p>
            <p>This is the training the Cortex Method aims to give you; the knowledge of the science of high performance and the insight to apply it.</p>
            <p>We have developed a methodology to give guidance to those on the path of high performance. This methodology is designed to be holistic and ongoing - it is a practical way of approaching your ambitions armed with the tools and techniques to manage yourself along the way. It isn&rsquo;t designed as a once off education, it&rsquo;s a Constitution of Performance to regularly refer back to. Because as you evolve, your performance, demands and resources will evolve. And there will always be a way of levelling up.</p>
            <p>If you are on the path of high performance, you have to accept it will not be easy. Performance is supposed to be challenging. What we aim to do here is to ensure you have the mental arsenal to fight back when challenge comes your way.</p>
            <p>But this pursuit of performance will fail before it&rsquo;s started without your unwavering and relentless commitment to excellence. Engagement, discipline and consistency is the only way that success is ever achieved, in anything.</p>
            <p>So ask yourself, what is your purpose for wanting to improve your psychological resources and your performance? How committed are you, really, to training your mind and in doing so challenging your performance potential? How much mental energy are you willing to dedicate to it?</p>
            <p>It&rsquo;s a long and hard journey, in fact it&rsquo;s a lifelong pursuit. But we hope we can be there to support and inspire you in any way we can.</p>
            </div>
            <div className="beta-content-tile wow fadeIn delay-150">
            <h3>Performance Protocols</h3>
            <HubspotForm
   portalId='4267461'
   formId='18d7bdc0-ff50-42ec-a676-487153d7aecb'
   onSubmit={() => console.log('Submit!')}
   onReady={(form) => console.log('Form ready!')}
   loading={<div>Loading...</div>}
   />
   <h3>Resources</h3>
   <ul className="resources">
      <li>
        <a className="book" href="https://www.amazon.com.au/Compound-Effect/dp/159315724X/ref=asc_df_159315724X/?tag=googleshopdsk-22&linkCode=df0&hvadid=341744699688&hvpos=1o1&hvnetw=g&hvrand=9341031692440693584&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9069171&hvtargid=pla-404289632070&psc=1" target="_blank">The Compound Effect by Darren Hardy</a>
      </li>
      <li>
        <a href="https://www.youtube.com/watch?v=TQMbvJNRpLE" target="_blank" className="video">TED Talk: How to achieve your most ambitious goals by Stephen Duneier</a>
      </li>
   </ul>
   <h3>Scientific Evidence</h3>
   <ul className="evidence">
      <li>Hansford, B. C., &amp; Hattie, J. A. (1982). The Relationship between Self and Achievement/Performance Measures. Review of Educational Research, 52(1), 123. doi:10.2307/1170275</li>
      <li>Raab, M., Lobinger, B., Hoffmann, S., Pizzera, A., &amp; Laborde, S. (2015). Performance Psychology: Perception, Action, Cognition, and Emotion. Cambridge, MA: Academic Press.</li>
      <li>Ward, P., Farrow, D., Harris, K. R., Williams, A. M., Eccles, D. W., &amp; Ericsson, K. A. (2008). Training Perceptual-Cognitive Skills: Can Sport Psychology Research Inform Military Decision Training? Military Psychology, 20(sup1), S71-S102. doi:10.1080/08995600701804814</li>
      <li>Zinsser, N., Perkins, L. D., Gervais, P. D., &amp; Burbelo, G. A. (2004, September). Military Application of Performance-Enhancement Psychology. <a href="http://www.au.af.mil/au/awc/awcgate/milreview/zinsser.pdf" target="_blank">Source</a></li>
   </ul>
            
            </div>
          </div>
        </div>
      </div>
      <div className="beta-tabs">
        <Link>
          <a className="active" href="/beta"><span className="session-text">Introduction</span> <span className="session-number">Intro</span></a>
        </Link>
        <Link>
          <a  href="/beta/performance/2"><span className="session-text">Performance Process</span> <span className="session-number">1</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/3"><span className="session-text">Physical Capital</span> <span className="session-number">2</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/4"><span className="session-text">Social Capital</span> <span className="session-number">3</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/5"><span className="session-text">Mental Factors</span> <span className="session-number">4</span></a>
        </Link>
        <Link>
          <a href="/beta/performance/6"><span className="session-text">Stress Management</span> <span className="session-number">5</span></a>
        </Link>
      </div>
    </Layout>
    );
  }
}


export default Beta