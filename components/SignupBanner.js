import Signup from "./Signup";

class SignupBanner extends React.Component {
    render() {
    var disabled =false;
    return (
	<div id="signup" className={"content-section signup-banner no-min-height "+this.props.className}>
    	<div className="container-md content-area">
        	<h3 className="h1 title wow fadeIn">{this.props.title}</h3>
            {this.props.subtitle && <p className="subtitle wow fadeIn delay-25">{this.props.subtitle}</p>}
			<div className="signup-form wow fadeIn delay-50">
				<Signup />
			</div> 
    	</div>
    	<div className="background-container">	
        	{this.props.background && <img  className="image" src={this.props.background} alt="Signup Background"  />}
    	</div> 
    </div>
    );
  }
}

export default SignupBanner