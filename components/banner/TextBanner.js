class TextBanner extends React.Component {
  render() {
    return (
    <div id="introduction" className="content-section small">
        <div className="container">
            <div className="container-sm">
              <p className="h3 wow fadeIn fadeUp" data-wow-offset="200">{this.props.title}</p> 
            </div>
        </div>
    </div>
    );
  }
}

export default TextBanner