class TileBanner extends React.Component {
  render() {
    return (
    <div id="introduction" className="banner tile-banner wow fadeIn fadeUp">
        <div className="inner">
          <div className="half">
            <p className="title h3">{this.props.title}</p> 
            <p className=" content h3">{this.props.content}</p> 
            <p className="tagline h3">{this.props.tagline}</p> 
          </div>
          <p className="coming-soon"><span>Coming soon</span> <i className="fab fa-apple"></i> <i className="fab fa-android"></i></p>
        </div>
    </div>
    );
  }
}

export default TileBanner