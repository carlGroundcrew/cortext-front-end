class Article extends React.Component {
  render() {
    return (
    	<div className={this.props.className+" article"}>
        	<div className="half wow fadeIn fadeUp tablet-show" data-wow-offset="300">
    			<div className="image-container">
    				<img className="image" src={this.props.image} alt="Method Image"/>
    			</div>
    		</div>
        	<div className="half content wow fadeIn fadeUp" data-wow-offset="300">
        		<div className="inner">
        			{this.props.title && <h3 className="h3 title">{this.props.title}</h3>}
	        		<div className="excerpt">
	        			{this.props.subtitle && <strong>{this.props.subtitle}</strong>}
                        {this.props.highlight && <strong className="bold">{this.props.highlight}</strong>}
	        			{this.props.content && <div><br/>{this.props.content}</div>}
	        		</div>
	        	</div>
	        </div>
    		<div className="half wow fadeIn fadeUp tablet-hide delay-50" data-wow-offset="300">
    			<div className="image-container">
    				<img className="image" src={this.props.image} alt="Method Image"/>
    			</div>
    		</div>
		</div>
    );
  }
}
class MethodColumns extends React.Component {
  render() {
    return (
    <div className="content-section no-min-height background-purple method-columns padding-top-extra margin-bottom-mobile">	
	    <div className="container method-container">
	        <Article title={'Human performance'} image={'/static/img/method/shadow.jpg'} subtitle={'Performance Psychology looks at factors that accelerate human development and create action to expand what’s possible for you.'} content={'Traditionally, performance psychology has been applied in more competitive, high consequence and hostile environments like the military or world class level sports; but we believe it is a science of everyday excellence that can be applied to us all. Whether you are a student, or a software developer, whether you are an athlete or work in mining – whatever your performance domain, the principles of excellence are the same.'}/>
	    	<Article className={'reverse content-padding-1'}  image={'/static/img/method/eye.jpg'} subtitle={'Through decades of analysis on human performance research has found that the factors which predict success are not innate or predestined but '} highlight={'they are trainable and they can be developed as a skill.'}/>
	    	<Article className={'content-padding-2'} image={'/static/img/method/app.jpg'} subtitle={'The Cortex Method is about putting together the factors that we know contribute to human performance to allow us to stack probability in the favour of success.'} content={'This means we utilise all the tools we know to increase the likelihood of achieving what we want to achieve. We condition ourselves to create a process to deliver the highest performing versions of ourselves in all facets of our lives.'}/>
	    </div>
	    <img className="background" src='/static/img/method/background.jpg' alt="Method Image"/>
	</div>
    );
  }
}

export default MethodColumns