import Slider from "react-slick"; 
import Link from 'next/link';
import {connect} from 'react-redux';

import {getCategoryNames} from '../store/functions';

class Post extends React.Component {
  render() {
    return (
    <div className="single-card medium banner">
    	<div className="half with-image">
    		<Link href={"/blog/"+this.props.blogID}>
    			<img className="image parent-height" src={this.props.background} alt="Banner Image"  />
    		</Link>
    	</div>
    	<div className="half with-content">
			<div className="content">
				<div className="header">
					<div className="title-container">
						<div className="overline">
							<p className="subtitle">{this.props.type}</p>
		        			{/*<p className="comments"><i className="fas fa-comment-alt"></i>{this.props.comments}</p>
		        			<p className="likes"><i className="fas fa-heart"></i>{this.props.likes}</p>*/}
		        		</div>
		        		<h3 className="h2">{this.props.title}</h3>
		        	</div>
		        	<div className="excerpt">
		        		{this.props.excerpt}
		        	</div>
		        	<Link href={"/blog/"+this.props.blogID}><button>Read</button></Link>
		        </div>
		    </div>
        </div>
	</div>
   );
  }
}

class BlogCarousel extends React.Component {
	render() {
	    var settings = {
	      dots: false,
	      slidesToShow: 1,
	      speed: 500,
	      arrows: true,
	      slidesToScroll: 1,
	      variableWidth: true,
	      focusOnSelect: true,
	      autoplay:false,
  	  	  autoplaySpeed:3000,
	      responsive: [
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        speed:300,
		        slidesToScroll: 1
		      }
		    } 
		  ]
	    };

	    let slider = this.props.slider.map(item => {
            return (
                <Post title={item.title} type={getCategoryNames(item.categories)} blogID={item.slug} background={item.image} excerpt={item.sub_title} />
            )
        })

	return (
	    <div className="card-container blog-carousel arrows-bottom mobile-arrows-top"> 
	      <Slider {...settings}>

			  { slider }

	      </Slider>
	    </div>
	    );
	}
}

const mapStateToProps = state => ({
    // Only map state needed in this component,
    slider: state.blog.slider,
})

// add comments functionality later
const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(BlogCarousel)

