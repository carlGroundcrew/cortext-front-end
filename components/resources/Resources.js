import React from 'react'

class Resources extends React.Component {
  render() {
    return (
    <div className="resources">
		<div className="single-resource">
			<img className="background" src="/static/img/home/book.jpg" alt="Our Content"  />
			<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
			<span className="read-more"></span>
			<div className="overlay"></div>
		</div>	
		<div className="single-resource half">
			<img className="background" src="/static/img/home/tiles.jpg" alt="Our Content"  />
			<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
			<span className="read-more"></span>
			<div className="overlay"></div>
		</div>	
		<div className="single-resource half">
			<img className="background" src="/static/img/home/person.jpg" alt="Our Content"  />
			<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
			<span className="read-more"></span>
			<div className="overlay"></div>
		</div>	
	</div>
    );
  }
}

export default Resources