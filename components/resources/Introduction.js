import React from 'react'
import Resources from "./Resources";
import Link from 'next/link'; 
class Introduction extends React.Component {
  render() {
    return (
    <div id="resources" className="content-section with-animation">
        <div className="container">
        	<div className="third content">
              <div className="container-sm content-area">
                <h3 className="h2 title wow fadeIn fadeUp">Resources</h3>
                <div className="text wow fadeIn fadeUp">
                Our program encourages you to dig deeper and learn more about our method and the communities of people who inspire, influence and educate us.
                </div>
                <div className="wow fadeIn fadeUp">
                	<Link  href="/pricing"><button>Join now for access</button></Link>
              	</div>
              </div>
            </div>
            <div className="two-thirds">
            	<div className="wow fadeIn fadeLeft">
              		<Resources />
              	</div>
            </div>
        </div>
    </div> 
    );
  }
}

export default Introduction