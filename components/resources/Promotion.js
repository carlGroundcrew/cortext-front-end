import React from 'react'
import ReactAudioPlayer from 'react-audio-player';
import Resources from "./Resources";
import Link from 'next/link'; 

class Video extends React.Component {
  render() {
    return (
    <div id={"video-"+this.props.id}className="video play-container">
        <div className="content" data-featherlight="#video-popup-container">
            <h3 className="title h3">{this.props.title}</h3>
            <div className="play"></div>
        </div>
        <img className="background" src={this.props.background} alt="Signup Background"  />
        <div id="video-popup-container" className="video-popup">
            <div className="internal">
                <iframe frameBorder="0" allowFullScreen="" src={'https://onelineplayer.com/player.html?autoplay=false&loop=true&autopause=true&muted=false&url=https%3A%2F%2Fvimeo.com%2F'+this.props.videoID+'&poster=&time=true&progressBar=true&playButton=true&overlay=true&muteButton=true&fullscreenButton=true&style=light&logo=false&quality=1080p'}></iframe>
            </div>
        </div>
    </div>
   );
  }
}

class Audio extends React.Component {
  constructor(props) {
      super(props);
      this.playAudio = this.playAudio.bind(this);
      this.setDuration = this.setDuration.bind(this);
      this.state = {time: '0:00', duration:'0:00', isPlaying:'notPlaying'};
  }
  setDuration() {
    var player =  this.rap.audioEl;
    var time = Number((player.duration).toFixed(0));
    var minutes = Math.floor(time / 60);
    var seconds = time - minutes * 60;
    if(seconds < 10) {
      seconds = '0'+seconds;
    }
    var duration = minutes+':'+seconds;

    this.setState({ duration: duration }); 

  }
  setCount() {

    if(this.rap) {
        var player =  this.rap.audioEl;
        var time = Number((player.currentTime).toFixed(0));
        var minutes = Math.floor(time / 60);
        var seconds = time - minutes * 60;
        if(seconds < 10) {
            seconds = '0'+seconds;
        }
        var duration = minutes+':'+seconds;
        this.setState({ time: duration });
    }
  }
  playAudio(){
    var player =  this.rap.audioEl;
    this.setDuration();
    if(player.paused) {
        player.play();
        this.setState({ isPlaying: 'isPlaying' });
    } else {
        player.pause();
        this.setState({ isPlaying: 'notPlaying' });
    }
    this.interval = setInterval(this.setCount.bind(this), 1000);

  } 
  render() {
    return (
    <div id={"video-"+this.props.id}className="video play-container">
        <div className="content"  onClick={this.playAudio}>
            <h3 className="title h3">{this.props.title}</h3>
            <div className={"play "+this.state.isPlaying }></div>
            <div className={"time "+this.state.isPlaying }>{this.state.time} / {this.state.duration}</div>
        </div>
        <img className="background" src={this.props.background} alt="Signup Background"  />
        <ReactAudioPlayer ref={(element) => { this.rap = element; }} src={this.props.audio} style={{background:'transparent', position:'absolute', left:'50%', bottom:'10px', transform:'translateX(-50%)'}} />
    </div>
   );
  }
}

class Introduction extends React.Component {
  render() {
    return (
    <div className="animation-container video-player">
        <Audio id={'1'} title={'Performance Session'} audio={'/static/audio/audio-1.mp3'} background={'/static/img/videos/video-2.jpg'} />
        <Audio id={'2'} audio={'/static/audio/audio-2.mp3'} title={'Mindfulness Session'} background={'/static/img/videos/video-1.jpg'} />
        <img className="image mobile-hide" src="/static/img/home/device-2.png" alt="Device 2"  />
        <img className="image mobile-showing" src="/static/img/home/device-1.png" alt="Device 1"  />
    </div> 
    );
  }
}

export default Introduction