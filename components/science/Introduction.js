import React from 'react'
import Link from 'next/link'; 
class Introduction extends React.Component {
  render() {
    return (
    <div id="backed" className="content-section">
        <div className="container-sm content-area">
          <div className="image wow fadeIn fadeUp">
            <img className="" src="/static/img/home/content.png" alt="Our Content"  />
          </div>
          <h3 className="h2 title wow fadeIn fadeUp">Our content is backed by decades of science.</h3>
          <div className="text wow fadeIn fadeUp ">
            Cortex Method is based on research from the worlds most elite and brutal performance environments. 
          </div>
          <Link  href="/science">
          <div className="wow fadeIn fadeUp">
            <button>See all</button>
          </div>
          </Link>
        </div>
      </div> 
    );
  }
}

export default Introduction