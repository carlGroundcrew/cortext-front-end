
class TableRow extends React.Component {
  render() {
    return (
    <div className="table-row wow fadeIn fadeUp">
        <div className="content">
            <div className="inner">
                {this.props.content}
            </div>
        </div>
        <div className="pricing right">
            <div className="col">
                <div className="inner">
                    <p className={"price "+this.props.basic}>{this.props.basic}</p>
                </div>
            </div>
            <div className="col">
                <div className="inner">
                    <p className="price highlight">{this.props.premium}</p>
                </div>
            </div>
            <div className="col"> 
                <div className="inner">
                    <p className="price">{this.props.premiumMonthly}</p>
                </div>
            </div>
        </div>
    </div>
   );
  }
}

class PricingList extends React.Component { 
  render() {
    return (
    <div className="card-container pricing-list wow fadeIn fadeUp" >
        <div className="title-area table-layout">
            <h3 className="left">Pricing Features</h3>
            <div className="pricing right mobile-hide">
                <div className="col">
                    <p className="h3 price">0</p>
                </div>
                <div className="col">
                    <p className="h3 price highlight">7.99</p>
                </div>
                <div className="col"> 
                    <p className="h3 price">9.99</p>
                </div>
            </div>
        </div>
        <div className="feature-list table-layout">
            <TableRow basic={'no'} premium={'yes'} premiumMonthly={'yes'} content={'Access to ‘performance profile’. Analysis of your current psychological makeup in terms of performance and guidance on a more bespoke program'} />
            <TableRow basic={'yes'} premium={'yes'} premiumMonthly={'yes'} content={'Basic training performance psychology sessions introducing the 5 pillars of performance'} />
            <TableRow basic={'yes'} premium={'yes'} premiumMonthly={'yes'} content={'Basic training on mindfulness for performance sessions'} />
            <TableRow basic={'no'} premium={'yes'} premiumMonthly={'yes'} content={'60+ in depth performance psychology training sessions based around emotion regulation, decision making, energy management, psychological skills training and utilising stress.'} />
            <TableRow basic={'no'} premium={'yes'} premiumMonthly={'yes'} content={'60+ mindfulness for performance training sessions targeted to train the mental skills needed to perform'} />
            <TableRow basic={'no'} premium={'yes'} premiumMonthly={'yes'} content={'20+ cortex moments to get you in the right headspace in different situations'} />
            <TableRow basic={'no'} premium={'yes'} premiumMonthly={'yes'} content={'Access to performance sessions as they update'} />
            <TableRow basic={'yes'} premium={'yes'} premiumMonthly={'yes'} content={'Digital goal setting functionality'} />
            <TableRow basic={'no'} premium={'yes'} premiumMonthly={'yes'} content={'Goal setting support and integration with your calendar'} />
            <TableRow basic={'yes'} premium={'yes'} premiumMonthly={'yes'} content={'Performance reflection functionality'} />
            <TableRow basic={'yes'} premium={'yes'} premiumMonthly={'yes'} content={'Daily quotes'} />
            <TableRow basic={'no'} premium={'yes'} premiumMonthly={'yes'} content={'Daily Call to Action (goal support)'} />
            <TableRow basic={'yes'} premium={'yes'} premiumMonthly={'yes'} content={'Access to “Cortex approved” resources'} />
            <TableRow basic={'yes'} premium={'yes'} premiumMonthly={'yes'} content={'Access to Cortex Podcast'} />
            <TableRow basic={'no'} premium={'yes'} premiumMonthly={'yes'} content={'Access to Cortex Members Community'} />
        </div>
    </div>
    );
  }
}

export default PricingList