import Slider from "react-slick"; 
import Link from 'next/link'; 

class SinglePanel extends React.Component { 
    constructor(props) {
        super(props); 
        this.state = {isOpen: false};
        this.toggleOpen = this.toggleOpen.bind(this);
    }
    toggleOpen() {
        this.setState(prevState => ({
            isOpen: !prevState.isOpen
        }));
    }
    render() {
    return (
    <div className={"single-panel wow fadeIn fadeUp delay-50 "+this.props.className}>
        <div className="header">
            <div className="pricing free">
                <div className="price">0</div>
            </div>
            <div className="inner">
                <h3 className="title">Base Program</h3>
                <p className="description">Introduction to Methodology</p>
            </div>
            <img className="background" src="/static/img/pricing/heading-1.jpg" alt="Card"/>
        </div>
        <div className={this.state.isOpen ? 'content open' : ' content'} >
            <header onClick={this.toggleOpen}>
                <p className="title">Features</p>
            </header>
            <div className="feature-list">
                <ul className="unordered-list">
                    <li>Introductory Performance sessions across the 5 Performance Pillars</li>
                    <li>Introductory mindfulness training program</li>
                    <li>Performance reflections</li>
                    <li>Digital goal setting</li>
                    <li>Daily quotes</li>
                    <li>Access to Cortex Podcast</li>
                    <li>Access to Cortex Members Community</li>
                    <li>Access to “Cortex Approved” resources</li>
                </ul>
            </div>
            <Link  href="/#signup"><button className='button'>Subscribe now</button></Link>
        </div>
    </div>
    );
  }
}

class MultiPanel extends React.Component { 
    constructor(props) {
        super(props);
        this.state = {isToggleOn: false};
        this.toggleClass = this.toggleClass.bind(this);
        this.setFalse = this.setFalse.bind(this);
        this.setTrue = this.setTrue.bind(this);
        this.state = {isOpen: false};
        this.toggleOpen = this.toggleOpen.bind(this);
    }
    toggleOpen() {
        this.setState(prevState => ({
            isOpen: !prevState.isOpen
        }));
    }
    toggleClass() {
        this.setState(prevState => ({
            isToggleOn: !prevState.isToggleOn
        }));
    }
    setFalse() {
        this.setState({isToggleOn: false})
    }
    setTrue() {
        this.setState({isToggleOn: true})
    }
    render() {
    return (
    <div className="single-panel wow fadeIn fadeUp delay-100 ">
        <div className="header second">
            <nav className="tab-nav">
                <div className={this.state.isToggleOn ? 'tab popular' : 'tab active popular'} onClick={this.setFalse}><span>Annual<br /><strong>Most popular</strong></span></div>
                <div className={this.state.isToggleOn ? 'active tab' : 'tab'} onClick={this.setTrue}><span>Monthly</span></div>
            </nav>
            <div className="inner margin-mobile">
                <h3 className="title">Full Program</h3>
                <p className="description dark">The Complete Methodology</p>
            </div>
            <div className="pricing pos-2">
                <div className="price">{this.state.isToggleOn ? '9.99' : '7.99'}</div>
                <p className="tagline">{this.state.isToggleOn ? 'Per Month' : 'Per Month'}</p>
                <p className="breakdown">{this.state.isToggleOn ? '$0.33 per day / $119.88 annually' : '$0.26 per day / $95.88 annually'}</p>
            </div>
            <img className="background" src="/static/img/pricing/heading-2.jpg" alt="Card"/>
        </div>
        <div className={this.state.isOpen ? 'content open' : ' content'} >
            <header onClick={this.toggleOpen}>
                <p className="title">Features</p>
            </header>
            <div className="tabs">
                <div className='active'>
                    <div className="feature-list">
                        <ul className="unordered-list">
                            <li>50+ performance psychology sessions across the 5 Performance Pillars</li>
                            <li>50+ corresponding mindfulness training sessions specific to the 5 Performance Pillars</li>
                            <li>20+ “ Cortex Moments” specific to a variety of situations and emotions</li>
                            <li>Access to new content in real time</li>
                            <li>Full Base Program for introductory training</li>
                            <li>Personalised 'Performance Profile'</li>
                            <li>Performance reflections</li>
                            <li>Digital goal setting</li>
                            <li>Daily reminders</li>
                            <li>Daily quotes</li>
                            <li>Access to Cortex Podcast</li>
                            <li>Access to Cortex Members Community</li>
                            <li>Access to “Cortex Approved” Resources</li>
                        </ul>
                    </div>
                </div>
            </div>
            <Link  href="/#signup"><button className='button'>Subscribe now</button></Link>
        </div>
    </div>
    );
  }
}

class Panels extends React.Component { 
  render() {
    return (
    <div className="panel-container pricing-panels">
        <SinglePanel className="mobile-hide"/>
        <MultiPanel />
        <SinglePanel className="mobile-show"/>
    </div>
    );
  }
}

export default Panels