import Slider from "react-slick"; 

class Cards extends React.Component { 
  render() {
    return (
    <div className="card-container pricing-cards wow fadeIn fadeUp">
        <div className="single-card large wow fadeIn fadeUp ">
			<div className="inner">
                <div className="title">
                    <h3 className="h2">Basic</h3>
                    <p className="subtitle">Free</p>
                </div>
        		<div className="pricing free">
        			<div className="price">0</div>
        		</div>
        	</div>
		</div>
        <div className="single-card large double highlight wow fadeIn fadeUp delay-50">  
            <div className="inner">
                <div className="title">
                    <h3 className="h2">Premium - Most Popular</h3>
                    <p className="subtitle">When paid annually</p>
                </div>
                <div className="pricing free">
                    <div className="price">7.99</div>
                </div>
            </div>
            <img className="background" src="/static/img/pricing/card-4.jpg" alt="Card"/>
        </div>
        <div className="single-card large  wow fadeIn fadeUp delay-100 highlight">
            <div className="inner">
                <div className="title">
                    <h3 className="h2">Premium</h3>
                    <p className="subtitle">When paid monthly</p>
                </div>
                <div className="pricing free">
                    <div className="price">9.99</div>
                </div>
            </div>
            <img className="background" src="/static/img/pricing/card-5.jpg" alt="Card"/>
        </div>
    </div>
    );
  }
}

export default Cards