class Testimonial extends React.Component {
  render() {
    return (
    <div className={" testimonial wow fadeIn "+this.props.color} style={{transitionDelay:this.props.delay+'s'}}>
        <p className='percentage h3'>{this.props.percent}</p>
        <div className='description'>
          {this.props.description}
        </div>
        <div className="indicator wow"><span style={{width: this.props.percent, transitionDelay:this.props.delay+'s'}}></span></div>
    </div>
    );
  }
}

class Testimonials extends React.Component {
  render() {
    
    return (
    <div className="testimonials">
        <Testimonial delay={'0.25'} percent={'76.9%'} color={'aqua'} description={'Significant impact on work performance'}  /> 
        <Testimonial delay={'0.5'} percent={'85%'} color={'yellow'} description={'Significant impact on ability to manage stress'}  /> 
        <Testimonial delay={'0.75'} percent={'85%'} color={'purple'} description={'Significant impact to understanding of goals'}  /> 
        <Testimonial delay={'1'} percent={'77%'} color={'orange'} description={'Significant impact on resilience'}  />
        <Testimonial delay={'1.25'} percent={'77%'} color={'purple'} description={'Significant impact on motivation'}  />
        <Testimonial delay={'1.5'} percent={'92.3%'} color={'orange'} description={'Significant impact on mindset'}  />
        <Testimonial delay={'1.75'} percent={'84.6%'} color={'aqua'} description={'Significant impact on well-being'}  />
        <Testimonial delay={'2'} percent={'92.3%'} color={'aqua'} description={'Would recommend to others'}  />
    </div>
    );
  }
}

export default Testimonials