import Slider from "react-slick"; 

class Testimonial extends React.Component {
  render() {
    return (
    <div className="single-card x-large banner ">
		<div className="content">
			<div className="header">
	        	<div className="description">
	        		<div className="testimony h3">{this.props.testimony}</div>
	        		<p className="author">{this.props.author}</p>
	        	</div>
	        </div>
        </div>
		<img className="background" src={this.props.background} alt="Banner Image"  />
	</div>
   );
  }
}

class Carousel extends React.Component {
	constructor(props) {
	    super(props);
	    this.openSlide = this.openSlide.bind(this);
  	}
  	componentDidMount() {
		var url = window.location.href; 
		if(url.includes('?')) {
			var slide = url.split('?')[1];
			var active = slide.split('#')[0];
			this.openSlide(active);
		}
  	}
	state = {
    	slideIndex: 0,
    	updateCount: 0
  	};
  	openSlide(slide) {
  		this.slider.slickGoTo(slide);
  	}
	render() {
	    var settings = {	
	      dots: false,
	      slidesToShow: 1,
	      speed: 500,
	      arrows: true,
	      slidesToScroll: 1,
	      variableWidth: true,
	      focusOnSelect: true,
	      autoplay:false,
  	  	  autoplaySpeed:3000,
  	  	  centerMode: true,
	      responsive: [
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        speed:300
		      }
		    } 
		  ], 
		  afterChange: () =>
        	this.setState(state => ({ updateCount: state.updateCount + 1 })),
      		beforeChange: (current, next) => this.setState({ slideIndex: next })
    	};
		return (
	    
	    <div className="card-container testimonial-carousel arrow-type-two centered"> 
	     	      
	      <Slider ref={slider => (this.slider = slider)}  {...settings}>

			<Testimonial  author={'Director of Marketing and Sales'} testimony={'It has been very helpful with my stress management & my positivity. That is the best thing I could have ever received as it has changed how I work & my outlook on life.'} background={'/static/img/testimonials/image-1.png'} />
			
			<Testimonial  author={'Finance Manager'} testimony={'I am more aware of my moods and when I need to step up certain areas of my performance. I am able to recognise stress better and regulate my mood more effectively.'} background={'/static/img/testimonials/image-2.png'} />			
			
			<Testimonial  author={'Strategic Investment Adviser'} testimony={'Helped me to prioritize and become more self aware'} background={'/static/img/testimonials/image-1.png'} />

			<Testimonial  author={'COO'} testimony={"I think the application of peak performance psych is worldwide across every industry and every business. I speak to so many mates who say it is incredible that your work offers this to each and every person that works under our roof."} background={'/static/img/testimonials/image-2.png'} />

			<Testimonial  author={'Strategic Investment Advisor'} testimony={"I couldn't imagine working without Cortex training and direction during my work week. Thankyou Thankyou Thankyou."} background={'/static/img/testimonials/image-1.png'} />

			<Testimonial  author={'Director'} testimony={"If you haven't fully investigated performance psychology and the impact that it can have on success at both an individual and organisational level - it would be very surprising if you were as effective as you possibly could be."} background={'/static/img/testimonials/image-2.png'} />
	      </Slider>
	    </div>
	    );
	}
}

export default Carousel