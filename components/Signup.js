import React from 'react'  
import HubspotForm from 'react-hubspot-form'

const Signup = () => (
	<div>
		<div id="signup-newsletter" className="hubspotForm">
			<HubspotForm
			   portalId='4267461'
			   formId='3857e95c-9d74-438d-a55f-f622a4274051'
			   onSubmit={() => console.log('Submit!')}
			   loading={<div>Loading...</div>}
			/>
		</div>
		<span>Get updates</span>
	</div>
)

export default Signup