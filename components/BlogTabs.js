import Link from 'next/link';
import {connect} from "react-redux";
import {getCategoryNames, inArray} from "../store/functions";

class BlogPost extends React.Component {
  render() {
    return (
    <div className={"single-card large " + this.props.style}>
          <Link href={"/blog/" + this.props.id}>
            <div className="title-content">
              <strong className="overline">{this.props.category}</strong>
              <h3 className="h3">{this.props.title}</h3>
            </div>
          </Link>
          <img className="background" src={this.props.background} alt="Card"/>
          <Link href={"/blog/" + this.props.id}><div className="link arrow"><i className="fas fa-chevron-right"></i></div></Link>
    </div>
   );
  }
}

class TabbedContent extends React.Component {
    constructor(props) {
        super(props);

        let categories = [];
        categories.push({'category_id': 0, 'name': 'All'});
        if (props.categories) {
            for (let i=0;i<props.categories.length;i++) {
                categories.push(props.categories[i]);
            }
        }

        this.state = {
            categories: categories,
            blogs: this.getCategoryBlogs(categories),
        }
    }

    getCategoryBlogs = (categories) => {
        let blogs = [];

        if (categories) {
            for (let k = 0; k<categories.length;k++) {
                blogs[k] = [];
                if (categories[k].category_id === 0) {
                    for (let i = 0; i<this.props.blogList.length;i++) {
                        blogs[k].push(this.props.blogList[i]);
                    }
                } else {
                    for (let i = 0; i<this.props.blogList.length;i++) {
                        let category_ids = this.props.blogList[i].categories.map(item => {
                            return item.category_id;
                        })

                        if (inArray(categories[k].category_id, category_ids)) {
                            blogs[k].push(this.props.blogList[i]);
                        }
                    }
                }
            }
        }
        return blogs;
    }

    render() {
        let { categories, blogs } = this.state;
        let i =0;
        let tabs = categories.map((item, key) => {
            if (i === 0) {
                i = 1;
                return (
                    <li className="active">
                        <a href={ '#tab' + key } data-toggle="tab">{ item.name }</a>
                    </li>
                )
            } else {
                return (
                    <li>
                        <a href={ '#tab' + key } data-toggle="tab">{ item.name }</a>
                    </li>
                )
            }
        })

        i =0;
        let tab_content = blogs.map((item, key) => {
            let is_active = false;
            if (i === 0) {
                i = 1;
                is_active = true;
            }


            if (item.length) {

                let last2 = null;
                let last = null;
                let blog_class = 'double';
                let blog_content = item.map(b => {

                    if (last2 === null && last === null) {
                        blog_class = 'double';
                    } else if (last2 === null && last !== null) {
                        blog_class = '';
                    } else {
                        if (last === '') {
                            if (last2 === '') {
                                blog_class = 'double';
                            } else {
                                blog_class = '';
                            }
                        } else {
                            if (last2 === '') {
                                blog_class = 'double';
                            } else {
                                blog_class = '';
                            }
                        }
                    }

                    last2 = last;
                    last = blog_class;

                    return (
                        <BlogPost id={b.slug} style={blog_class} title={b.title} category={getCategoryNames(b.categories)} background={b.image}/>
                    )
                })

                return (
                    <div className={(is_active) ? 'tab-pane active' : 'tab-pane'} id={'tab'+key}>
                        <div className="content container">
                            <div className="card-container resources">
                                { blog_content }
                            </div>
                        </div>
                    </div>
                )
            } else {
                return (
                    <div className={(is_active) ? 'tab-pane active' : 'tab-pane'} id={'tab'+key}>
                        <div className="content container">
                            <div className="card-container resources">
                                <h1 className="text-center">Blog not available for this category.</h1>
                            </div>
                        </div>
                    </div>
                )
            }
        })


  return (
      <div className="tabbed-posts"> 
        <ul  className="nav navigation-tabs container-rg">
            { tabs }
        </ul>
        <div className="tab-content fadeIn fadeUp wow clearfix">
            { tab_content }
        </div>
      </div>
      );
  }
}

const mapStateToProps = state => ({
    // Only map state needed in this component,
    blogList: state.blog.blogList,
    categories: state.blog.categories,
})

// add comments functionality later
const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(TabbedContent)