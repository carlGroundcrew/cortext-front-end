import Link from 'next/link'; 

class CorporateBanner extends React.Component { 
  render() {
    return (
    <div className="content-section no-padding-top">
        <div className="image-banner contained white with-borders">
            <div className="content">
                <div className="inner">
                    <h2 className="h1 title wow fadeIn fadeUp">Interested in a <br/><strong>Corporate Package?</strong></h2>
                    <p className="wow fadeIn fadeUp delay-50">Contact us to find out more about corporate memberships.</p>
          <Link href="/contact">
            <button className="wow fadeIn fadeUp delay-100">Contact</button>
          </Link>
                </div>
            </div>
            <img className="background" src="/static/img/pricing/banner-1.jpg" alt="Banner Image"  />
        </div>
    </div>
    );
  }
}

export default CorporateBanner