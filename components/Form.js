 
import HubspotForm from 'react-hubspot-form'

class Form extends React.Component {
  render() {
    return (
    <div id="contact-form" className="form">
	    <HubspotForm
		   portalId='4267461'
		   formId='a7d3f0ed-9167-4baa-b227-4541b33ede00'
		   onSubmit={() => console.log('Submit!')}
		   onReady={(form) => console.log('Form ready!')}
		   loading={<div>Loading...</div>}
		/>
	</div>
    );
  }
}

export default Form