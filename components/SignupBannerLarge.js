import Signup from "./Signup";
import AudioPlayer from "react-h5-audio-player";


class Video extends React.Component {
  render() {
    return (
    <div className="video play-container">
        <div className="content" data-featherlight="#video-popup-container">
            <h3 className="title h2">{this.props.title}</h3>
            <div className="play"></div>
        </div>
        <img className="background" src={this.props.background} alt="Signup Background"  />
        <div id="video-popup-container" className="video-popup">
            <div className="internal">
                <iframe frameBorder="0" allowFullScreen="" src={'https://onelineplayer.com/player.html?autoplay=false&loop=true&autopause=true&muted=false&url=https%3A%2F%2Fvimeo.com%2F'+this.props.videoID+'&poster=&time=true&progressBar=true&playButton=true&overlay=true&muteButton=true&fullscreenButton=true&style=light&logo=false&quality=1080p'}></iframe>
            </div>
        </div>
    </div>
   );
  }
}

class Audio extends React.Component {
  render() {
    return (
    <div className="video audio play-container">
        <div className="content" data-featherlight="#video-popup-container-2">
            <h3 className="title h2">{this.props.title}</h3>
            <div className="play"></div>
        </div>
        <img className="background" src={this.props.background} alt="Signup Background"  />
        <div id="video-popup-container-2" className="video-popup">
            <div className="internal">
                <iframe frameBorder="0" allowFullScreen="" type="video/mp3" src="/static/audio/audio-1.mp3"></iframe>
            </div>
        </div>
    </div>
   );
  }
}

class SignupBanner extends React.Component {
    render() {
    return (

	<div id="sign-up" className="content-section signup-banner no-min-height">
        <div className="container-md content-area introduction wow fadeIn fadeUp">
            <h3>Try Cortex Method for free</h3>
            <div className="information">
                Each session focuses on 2 primary areas; performance and mindfulness. In these sample sessions we’ll help you to start conceptualising how performance psychology will fit in to your life and start to understand your direction.
            </div>
        </div>
        <div className="container content-area video-container">
            <div className="video-player">
                <div className="half wow fadeIn fadeUp">
                    <Video title={'Performance Session'} videoID={'320679619'} background={'/static/img/videos/video-2.jpg'} />
                </div>
                <div className="half wow fadeIn fadeUp delay-50">
                    <Video videoID={'320679619'} title={'Mindfulness Session'} background={'/static/img/videos/video-1.jpg'} />
                </div>
            </div>
        </div>
        <div id="signup">
        	<div className="container-md content-area">
            	<h3 className="h1 title wow fadeIn">{this.props.title}</h3>
        			<div className="signup-form wow fadeIn delay-50">
        				<Signup />
        			</div> 
        	</div>
        </div>
    	<div className="background-container">	
        	<img className="image" src={this.props.background} alt="Signup Background"  />
    	</div> 
    </div>
    );
  }
}

export default SignupBanner