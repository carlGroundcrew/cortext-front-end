import React from 'react'

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class CallBanner extends React.Component {
    constructor(props) {
      super(props);
      this.scrollToTop = this.scrollToTop.bind(this);
    } 
    scrollToTop() {
        window.scrollTo(0, 550);
    }
    handleClick = () => {
        this.scrollToTop();
        this.props.handler();
    }
    render() {
    
    return (
	  <div id="call" className="content-section call-banner">
    	<div className="container content-area">
    		<div className="content">
        		<h3 className="h2 title wow fadeIn fadeUp">Haven’t found what you’re looking for?</h3>
        		<button onClick={this.handleClick} className="white wow fadeIn fadeUp delay-50">Send us a message</button>
        	</div>
    	</div>
    	<div className="background-container">	
        	<img className="image" src="/static/img/contact/call-background.jpg" alt="Signup Background"  />
    	</div> 
    </div>
    );
  }
}

export default CallBanner