import { Link } from '../routes';


const PostItem = ({ post }) => (
 	<div>
 		<Link route='post' params={{ slug: post.id }}>
	    <article>
	        <h3>{post.title}</h3>
	        <p>{post.content}</p>
	    </article> 
	    </Link>
	</div>
)

export default PostItem
