import Signup from "./Signup";
import Link from 'next/link'; 

const Footer = () => (
	<footer id="colophon">
		<div className="container">
			<div className="footer-content">
				<div className="half"> 
					<div className="branding-container">
						<Link href="/">
							<div className="blob-container">
								<div className="blob-dark"></div>
							</div>
						</Link>
					</div>
					<div className="vision">
						<div className="inner">
							<span>Our Vision</span>
							<h3 className="h2"><strong>Empower people</strong> in their lives and pursuits.</h3>
						</div>
					</div>
				</div>
				<div className="half">
					<div className="social-icons">
						<div className="inner">
							<Link href="https://www.facebook.com/cortexmethodau"><a><div className="icon facebook"><i className="fab fa-facebook-f"></i></div></a></Link>
							<Link href="https://www.instagram.com/cortex.method"><a>
								<div className="icon twitter">
									<i className="fab fa-instagram"></i>
								</div>
							</a></Link>
							<Link href="https://au.linkedin.com/company/cortex-method"><a>
								<div className="icon linkedin">
									<i className="fab fa-linkedin-in"></i>
								</div>
							</a></Link>
							<span>Follow us</span>
						</div>
					</div>
					<ul className="navigation">
						<div className="inner">
							<li><Link href="/method"><a>The Method</a></Link></li>
							<li><Link href="/program"><a>Program</a></Link></li>
							<li><Link href="/science"><a>The Science</a></Link></li>
							<li><Link href="/blog"><a>Blog</a></Link></li>
							<li><Link href="/contact"><a>Contact</a></Link></li>
						</div>
					</ul>
					<div className="signup-form">
						<Signup />
					</div>
				</div>
				<div className="bottom">
					<div className="copyright">
						© Copyright 2019 Cortex Method
					</div>
					<div className="website-by">
						Website design by <a href="https://groundcrew.com.au" target="_blank" rel="noopener noreferrer">Groundcrew</a>.
					</div>
				</div>
			 </div>
		</div>
	</footer>
)

export default Footer