import Link from 'next/link';
class TextBanner extends React.Component {
  render() {
    return (
    <div id="introduction" className="content-section grey-bg contact-card no-min-height">
        <div className="container">
            <div className="half">
              <h2 className="title double-height">Cortex Method has been developed to give people the mindset required to unlock their potential.</h2>
              <div className="social-icons">
                <Link href="https://www.facebook.com/cortexmethodau"><a><div className="icon facebook"><i className="fab fa-facebook-f"></i></div></a></Link>
                <Link href="https://www.instagram.com/cortex.method"><a>
                  <div className="icon twitter">
                    <i className="fab fa-instagram"></i>
                  </div>
                </a></Link>
                <Link href="https://au.linkedin.com/company/cortex-method"><a>
                  <div className="icon linkedin">
                    <i className="fab fa-linkedin-in"></i>
                  </div>
                </a></Link>
                <span>Follow us</span>
              </div>
            </div>
            <div className="half">
              <div className="contact-information">
                <div className="line">
                  <strong>HQ</strong> Brisbane Australia
                </div>
                <div className="line">
                  <strong>Phone</strong> 1300 368 166
                </div>
                <div className="line">
                  <strong>Email</strong> info@cortexmethod.com
                </div>
                <div className="line">
                  <strong>Founders</strong> Leanne Huggett, Drew Haupt & Brodie Haupt
                </div>
              </div>
            </div>
        </div>
    </div>
    );
  }
}

export default TextBanner