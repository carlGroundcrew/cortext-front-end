import Link from 'next/link'; 

class Hero extends React.Component {
  constructor(props) {
      super(props)
      this.myRef = React.createRef()
      this.state = {loading:'loading'}
  }
  componentDidMount() {
      this.setState({loading: 'loaded'});    
  }
  render() {
    return (
    <div id="banner" className="content-section hero">
      <div className="container">
          <div className="half">
            <div className="banner-content">
                <h1 className="wow fadeIn fadeUp">Everything you do comes from your mind. <strong>Take ownership of it.</strong></h1>
              <p className="wow fadeIn fadeUp delay-50">Subscribe for our exclusive pre-launch offer, <br/>20% off for life.</p>
              <div className="wow fadeIn fadeUp delay-100">
                <Link  href="/#signup"><button>Subscribe now</button></Link>
              </div>
            </div>
          </div>
          <div className="half wow fadeIn">
            <div className={"image-container container-loading large "+this.state.loading}>
              <img className="image floating" src="/static/img/home/iphone.png" alt="Our Content"  />
              <div className="blob"></div> 
            </div>
          </div>
        </div>
    </div>
    );
  }
}

export default Hero