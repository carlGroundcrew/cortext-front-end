import Link from 'next/link'; 

class Hero extends React.Component {
  render() {
    
    return (
    <div id="banner" className="content-section">
      <div className="container">
          <div className="half">
            <div className="banner-content">
                <h1 className="wow fadeIn fadeUp">Everything you do comes from your mind. <strong>Take ownership of it.</strong></h1>
              <p className="wow fadeIn fadeUp delay-50">Subscribe for our exclusive pre-launch offer, 20% off for life.</p>
              <div className="wow fadeIn fadeUp delay-100">
                <Link  href="/#signup"><button className="mobile-hide" onClick={this.scrollToMyRef}>Subscribe</button></Link>
                <Link  href="/#signup"><button className="desktop-hide">Subscribe for 20% off</button></Link>
              </div>
            </div>
          </div>
          <div className="half wow fadeIn">
            <div className="animating-container large">
              <div id="main-blob" className='blob'></div>  
            </div>
          </div>
        </div>
    </div>
    );
  }
}

export default Hero