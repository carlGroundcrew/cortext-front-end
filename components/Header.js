import Link from 'next/link';


class Header extends React.Component {
	componentDidMount() {
    	document.body.classList.remove('menu-open');
 	}
	constructor(props) {
	    super(props);
	    this.state = {isToggleOn: true};
	    this.openMenu = this.openMenu.bind(this);
	}
	openMenu() {
		document.body.classList.toggle('menu-open');
        this.setState(prevState => ({
      	  isToggleOn: !prevState.isToggleOn
    	}));
  	}
	render() {
    return (
	<header id="masthead" className={this.props.className}>
		<div id="mobile-menu" className={this.state.isToggleOn ? '' : 'open'}>
			<Link href="/"><div className="logo"></div></Link>
			<ul className="mobile-navigation">
		    <li className={this.props.page=="method" && 'active'}><Link href="/method"><a className="h2 white">The Method</a></Link></li>
		    <li className={this.props.page=="program" && 'active'}><Link href="/program"><a className="h2 white">Program</a></Link></li>
		    <li className={this.props.page=="science" && 'active'}><Link href="/science"><a className="h2 white">The Science</a></Link></li>
		    <li className={this.props.page=="blog" && 'active'}><Link href="/blog"><a className="h2 white">Blog</a></Link></li>
		    <li className={this.props.page=="contact" && 'active'}><Link href="/contact"><a className="h2 white">Contact</a></Link></li> 
		  </ul>
		</div>
		
		<div className="container">
		  <div id="mobile-menu-container">	
		  	<div className={this.state.isToggleOn ? 'mobile-menu-icon desktop-hide' : 'mobile-menu-icon desktop-hide open'} onClick={this.openMenu}>
		  		<span className="bar"></span>
		  	</div>
		  </div>
		  {!this.props.className && <Link href="/"><a className="main-link"><img className="logo" src='/static/img/logo.svg' alt="logo" /></a></Link>}
		  {this.props.className && <Link href="/"><a className="main-link"><img className="logo" src='/static/img/logo-100.svg' alt="logo" /></a></Link>}
		  <ul className="navigation mobile-hide">
		    <li className={this.props.page=="method" && 'active'}><Link href="/method"><a>The Method</a></Link></li>
		    <li className={this.props.page=="program" && 'active'}><Link href="/program"><a>Program</a></Link></li>
		    <li className={this.props.page=="science" && 'active'}><Link href="/science"><a>The Science</a></Link></li>
		    <li className={this.props.page=="blog" && 'active'}><Link href="/blog"><a>Blog</a></Link></li>
		    <li className={this.props.page=="contact" && 'active'}><Link href="/contact"><a>Contact</a></Link></li> 
		  </ul>
		  <div className="signup-container">
		  	{/*<Link href="/login"><a className="login">Log in</a></Link>
		  	<Link href="/pricing"><a className="signup tablet-hide">Sign up</a></Link>
		  	<div className="mobile-menu"></div>*/}
		  	<Link href="/#signup"><button className="mobile-hide">Subscribe for 20% off</button></Link>
		  </div>
		</div>
	</header>
	);
  }
}

export default Header