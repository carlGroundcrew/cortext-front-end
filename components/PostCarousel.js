import Slider from "react-slick"; 

class Banner extends React.Component {
  render() {
    return (
    <div className="single-card x-large banner">
		<img className="background" src={this.props.background} alt="Banner Image"  />
	</div>
   );
  }
}

class BannerCarousel extends React.Component {
	render() {
	    var settings = {
	      dots: false,
	      slidesToShow: 1,
	      speed: 500,
	      arrows: true,
	      slidesToScroll: 1,
	      variableWidth: false,
	      focusOnSelect: true,
	      autoplay:false,
  	  	  autoplaySpeed:3000,
	      responsive: [
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    } 
		  ]
	    };
	return (
	    <div className="card-container post-carousel"> 
	      <Slider {...settings}>
			<Banner background={this.props.background}/>			
	      </Slider>
	    </div>
	    );
	}
}

export default BannerCarousel