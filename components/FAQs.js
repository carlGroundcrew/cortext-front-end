import Slider from "react-slick"; 

class FAQs extends React.Component {
	
	render() {
	    var settings = {
	      dots: false,
	      slidesToShow: 3,
  		  slidesToScroll: 1,
	      speed: 500,
	      arrows: true,
	      slidesToScroll: 1,
	      variableWidth: true,
	      focusOnSelect: true,
	      autoplaySpeed: 3000,
	      responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        speed:300,
		        slidesToScroll: 1,
		        focusOnSelect: false
		      }
		    } 
		  ]
	    };
	return (
	    <div className="card-container faqs faqs-carousel  arrow-type-two"> 
	      <Slider {...settings}>
	        <div className="single-card large faq">
	        	<div className="content">
	        		<div className="header">
	        			<h3 className="h3">What does capital mean?</h3>
	        		</div>
	        		<div className="answer">
	        			We use the word capital to explain how mental strength works. When you have greater capital - you have more to use in the ways that are meaningful to you. The same is true of your mental capacity.

	        		</div>
	        	</div>
	        	<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
			</div>
			<div className="single-card large faq">
				<div className="content">
					<div className="header">
			        	<h3 className="h3"> How much does the full program cost?</h3>
			        </div>
			        <div className="answer">
	        			$9.99 per month or $7.99 paid annually.
	        		</div>
		        </div>
				<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
			</div>
			<div className="single-card large faq">				
				<div className="content">
					<div className="header">
		        		<h3 className="h3">How long does each session take?</h3>
		        	</div>
		        	<div className="answer">
	        			Sessions vary but are never longer then 10 minutes.
	        		</div>
		        </div>
				<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
			</div>
			<div className="single-card large faq">
				<div className="content">
					<div className="header">
		        		<h3 className="h3">How long will it take me to get through the whole program</h3>
		        	</div>
		        	<div className="answer">
	        			It can take as long or as short as you like, it can run at your own pace and in your own structure. However we recommend working at the pace of one full session (performance & mindfulness) per week.
	        		</div>
		        </div>
				<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
			</div> 
			<div className="single-card large faq">
				<div className="content">
					<div className="header">
		        		<h3 className="h3">Can I jump around and pick and choose which sessions I do?</h3>
		        	</div>
		        	<div className="answer">
	        			Absolutely! We have structured the cortex method program to work most effectively when completed sequentially however if it feels more relevant for you to work in a different way - go for it! You know yourself best, we are just here to support.
	        		</div>
		        </div>
				<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
			</div> 
			<div className="single-card large faq">
				<div className="content">
					<div className="header">
		        		<h3 className="h3">Why is it called a ‘Method’</h3>
		        	</div>
		        	<div className="answer">
	        			We have called this a method because it is a way of going through life. Performance and potential operate a little like an ecosystem, the factors and the principles are all interconnected and interdependent. So what we are trying to endorse is a way of life, that supports your mental wellbeing and your mental strength so you can tackle whatever is thrown your way.

	        		</div>
		        </div>
				<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
			</div> 
	      </Slider>
	    </div>
	    );
	}
}

export default FAQs