import Signup from "../Signup";
import Link from 'next/link'; 

const Footer = () => (
	<footer id="colophon">
		<div className="container">
			<div className="footer-content">
				
				<div className="bottom">
					<div className="copyright">
						© Copyright 2019 Cortex Method
					</div>
					<div className="website-by">
						Website design by <a href="https://groundcrew.com.au" target="_blank" rel="noopener noreferrer">Groundcrew</a>.
					</div>
				</div>
			 </div>
		</div>
	</footer>
)

export default Footer