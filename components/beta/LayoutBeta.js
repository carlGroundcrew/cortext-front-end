import Head from '../Head';
import Header from './HeaderBeta';
import Footer from './FooterBeta';
import '../../style/style.scss';

export default ({ children, title = 'Cortext Method', className, page}) => (
	<div>
		<Head />
		<Header className="beta" page={page} />
		<div className="beta-blob-outer">
			<div className="beta-blob-inner">
				<div className="blob"></div>
			</div>
		</div>
		<div className='body-wrapper beta'>
			{children}
		</div>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script src="//cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
	</div>

)