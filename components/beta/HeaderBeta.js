import Link from 'next/link';


class Header extends React.Component {
	componentDidMount() {
    	document.body.classList.remove('menu-open');
 	}
	constructor(props) {
	    super(props);
	    this.state = {isToggleOn: true};
	    this.openMenu = this.openMenu.bind(this);
	}
	openMenu() {
		document.body.classList.toggle('menu-open');
        this.setState(prevState => ({
      	  isToggleOn: !prevState.isToggleOn
    	}));
  	}
	render() {
    return (
	<header id="masthead" className={this.props.className}>
		
		<div className="container-full">
			<div className="welcome-beta">Welcome to Cortex Beta</div>
		  <Link href="/"><a className="main-link"><img className="logo" src='/static/img/logo-100.svg' alt="logo" /></a></Link>
		</div>
	</header>
	);
  }
}

export default Header