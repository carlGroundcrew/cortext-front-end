import React from 'react'
import ReactAudioPlayer from 'react-audio-player';
import Link from 'next/link'; 

class Audio extends React.Component {
  constructor(props) {
      super(props);
      this.playAudio = this.playAudio.bind(this);
      this.setDuration = this.setDuration.bind(this);
      this.state = {time: '0:00', duration:'0:00', isPlaying:'notPlaying'};
  }
  setDuration() {
    var player =  this.rap.audioEl;
    var time = Number((player.duration).toFixed(0));
    var minutes = Math.floor(time / 60);
    var seconds = time - minutes * 60;
    if(seconds < 10) {
      seconds = '0'+seconds;
    }
    var duration = minutes+':'+seconds;

    this.setState({ duration: duration }); 

  }
  setCount() {

    if(this.rap) {
        var player =  this.rap.audioEl;
        var time = Number((player.currentTime).toFixed(0));
        var minutes = Math.floor(time / 60);
        var seconds = time - minutes * 60;
        if(seconds < 10) {
            seconds = '0'+seconds;
        }
        var duration = minutes+':'+seconds;
        this.setState({ time: duration });
    }
  }
  playAudio(){
    var player =  this.rap.audioEl;
    this.setDuration();
    if(player.paused) {
        player.play();
        this.setState({ isPlaying: 'isPlaying' });
    } else {
        player.pause();
        this.setState({ isPlaying: 'notPlaying' });
    }
    this.interval = setInterval(this.setCount.bind(this), 1000);

  } 
  render() {
    return (
    <div id={"video-"+this.props.id}className="video play-container">
        <div className="content"  onClick={this.playAudio}>
            <h3 className="title h3">{this.props.title}</h3>
            <div className={"play "+this.state.isPlaying }></div>
            <div className={"time "+this.state.isPlaying }>{this.state.time} / {this.state.duration}</div>
        </div>
        <img className="background" src={this.props.background} alt="Signup Background"  />
        <ReactAudioPlayer ref={(element) => { this.rap = element; }} src={this.props.audio} style={{background:'transparent', position:'absolute', left:'50%', bottom:'10px', transform:'translateX(-50%)'}} />
    </div>
   );
  }
}

class Introduction extends React.Component {
  render() {
    return (
    <div className="animation-container video-player beta-wrap">
        <Audio id={'1'} title={this.props.titleone} audio={this.props.audioone} background={'/static/img/testimonials/image-1.png'} />
        <Audio id={'2'} audio={this.props.audiotwo} title={this.props.titletwo} background={'/static/img/testimonials/image-2.png'} />
    </div> 
    );
  }
}

export default Introduction