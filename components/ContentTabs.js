import Link from 'next/link';
class TabbedContent extends React.Component {
	render() {
	return (
	    <div className="tabbed-content"> 
			<ul  className="nav navigation-tabs">
				<li>
	        		<a  href="#base" data-toggle="tab">The Base Program</a>
				</li>
				<li className="active">
					<a href="#full" data-toggle="tab">The Full Program</a>
				</li>
			</ul>
			<div className="tab-content clearfix">
			  	<div className="tab-pane" id="base">
			  		<div className="wrapper white">
				  		<div className="inner">
							<img className="image" src="/static/img/method/base-program.png" alt="Full Program"  />
						</div>
					</div>
					<div className="inner">
						<div className="content">
							The base program will give you an initial understanding or a platform to begin exploring what could be possible for you. It goes through the central pillars integral to human performance and fulfilling potential.
							<br/><br/>
							Through this program we will teach you how to improve your management of stress, increase your understanding of your goals and how to get your behaviour in line with that; we aim to increase your sense of meaning your work, improve your experience of challenge, improve your energy, positivity, adaptability and resilience.
							<br/><br/>
							In addition the central pillars in the base Cortex Method, this program works in tandem with our base mindfulness performance program. It has been designed as an introduction to mindfulness training - specifically geared to train mindfulness for performance - to train focus, resilience, emotion regulation and self awareness.
							<br/><br/>
							<strong>So that you can start working with your mind, rather than against it.</strong>
							<br/><br/>
							<Link href="/program"><button className="black">See pricing</button></Link>
						</div>
					</div>
				</div>
				<div className="tab-pane active" id="full">
					<div className="wrapper white">
				  		<div className="inner">
							<img className="image" src="/static/img/method/full-program.png" alt="Full Program"  />
      					</div>
      				</div>
					<div className="inner">
						<div className="content">
							The Full Program has over 100 performance and mindfulness sessions across the 5 pillars of performance. This training specifically targets the way that science and the worlds elite performers train their mind, perspectives and behaviour to achieve greatness.
							<br/><br/>
							High performance is about understanding yourself, your triggers, obstacles, and aspirations and then building and relentlessly improving your process to achieve.							
							<br/><br/>
							We build a psychological arsenal to manage stress, build resilience, creativity, confidence and clarity.
							<br/><br/>
							<strong>It’s about exploring what is possible for you. And then building the road to get you there.</strong>
							<br/><br/>
							<Link href="/program"><button className="black">See pricing</button></Link>
						</div>
					</div>
				</div>
			</div>
 		</div>
	    );
	}
}

export default TabbedContent