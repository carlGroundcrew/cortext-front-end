import Slider from "react-slick"; 

class Banner extends React.Component {
  render() {
    return (
    <div className={"single-card large banner "+this.props.className}>
		<div className="content">
			<div className="header">
				<div className="title-container">
	        		<h3 className="h3">{this.props.title}</h3>
	        		<p className="subtitle">{this.props.subtitle}</p>
	        	</div>
	        	<div className="description">
	        		{this.props.description}
	        	</div>
	        </div>
        </div>
		<img className="background" src={this.props.background} alt="Banner Image"  />
	</div>
   );
  }
}

class BannerCarousel extends React.Component {
	constructor(props) {
	    super(props);
	    this.openSlide = this.openSlide.bind(this);
  	}
  	componentDidMount() {
		var url = window.location.href; 
		if(url.includes('?')) {
			var slide = url.split('?')[1];
			var active = slide.split('#')[0];
			this.openSlide(active);
		}
  	}
	state = {
    	slideIndex: 0,
    	updateCount: 0
  	};
  	openSlide(slide) {
  		this.slider.slickGoTo(slide);
  	}
	render() {
	    var settings = {	
	      dots: false,
	      slidesToShow: 1,
	      speed: 500,
	      arrows: true,
	      slidesToScroll: 1,
	      variableWidth: true,
	      focusOnSelect: true,
	      autoplay:false,
  	  	  autoplaySpeed:3000,
	      responsive: [
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    } 
		  ], 
		  afterChange: () =>
        	this.setState(state => ({ updateCount: state.updateCount + 1 })),
      		beforeChange: (current, next) => this.setState({ slideIndex: next })
    	};
		return (
	    
	    <div className="card-container banner-carousel arrows-bottom"> 
	     	      
	      <Slider ref={slider => (this.slider = slider)}  {...settings}>

			<Banner className={'dark-on-mobile'} title={'Our content is based on a strong scientific discipline through research into the worlds most hostile, competitive and elite arenas.'} subtitle={''} background={'/static/img/banners/banner-0.jpg'} description={''}/>

			<Banner title={'Performance Process'} subtitle={'Pillar no. 1'} background={'/static/img/banners/banner-1.jpg'} description={'This pillar is designed as the first step in understanding what your optimal performance looks like, conceptualise the process of getting there, and building genuine intrinsic motivation to power you through.'}/>
			
			<Banner title={'Physical Capital '} subtitle={'Pillar no. 2'} background={'/static/img/banners/banner-2.jpg'} description={'This pillar is learning how to capitalise on the mind body links to ensure you can deliver when the pressure is on. You can not deliver your best if you do not have the energy to take you through an average day. Step up your game.'}/>
			
			<Banner title={'Social Capital'} subtitle={'Pillar no. 3'} background={'/static/img/banners/banner-3.jpg'} description={'This pillar is really understanding the unavoidable impact that our networks and our connections have on our identity, our perspectives and, ultimately, our performance; and teaching how to capitalise and grow the right social capital.'}/>
			
			<Banner title={'Mental Factors'} subtitle={'Pillar no. 4'} background={'/static/img/banners/banner-4.jpg'} description={'Everything you do comes from your mind. Your perspectives, your response times, your decisions - everything. This pillar teaches how to train your mind, your perspectives and your attitudes for high performance.'}/>
			
			<Banner title={'Stress Management'} subtitle={'Pillar no. 5'} background={'/static/img/banners/banner-5.jpg'} description={'Stress is a fundamental part of life, and if you know how to use it - it can be your greatest ally. This pillar teaches and trains a different approach to stress - how to manage it, utilise it and embrace it.'}/>

	      </Slider>
	    </div>
	    );
	}
}

export default BannerCarousel