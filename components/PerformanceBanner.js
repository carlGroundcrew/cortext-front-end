import Signup from "./Signup";

class PerformanceBanner extends React.Component {
    render() {
    return (

	<div id="performance" className="content-section">
        <div className="float">
            <img className="wow fadeIn" src="/static/img/home/corner.png" alt="Our Content"  />
        </div>
        <div className="container-lg"> 
            <div className="half">
              <div className="video-container play-container wow fadeIn fadeRight">
                <img className="video" src="/static/img/home/background-video.png" alt="Our Content"  />
                <a target="_blank" href="https://player.vimeo.com/video/318322351?autoplay=1"><div className="play"></div></a>
              </div>
            </div> 
            <div className="half content">
              <div className="container-sm content-area">
                <h3 className="h2 title wow fadeIn fadeUp">Performance Session</h3>
                <div className="text wow fadeIn fadeUp">
                In this sample session we’ll help you discover your ‘why’. In order help you understnad what drives you and underlies All that you do?
                </div>
              </div>
            </div>
        </div>
    </div>
    );
  }
}

export default PerformanceBanner