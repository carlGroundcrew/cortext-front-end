import Slider from "react-slick"; 

class Cards extends React.Component { 
  render() {
    return (
    <div className="card-container pricing-cards wow fadeIn fadeUp">
        <div className="single-card large">
        	<div className="inner">
        		<div className="pricing free">
        			<div className="price">0</div>
        		</div>
        		<div className="price-label">
        			<h3>Free</h3>
        		</div>
        		<div className="feature-list">
        			<ul className="unordered-list single-column">
        				<li>1 User</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        			</ul>
        		</div>
        	</div>
        	<img className="background" src="/static/img/pricing/card-1.jpg" alt="Card"/>
		</div>
		<div className="single-card large wow fadeIn fadeUp delay-50">
			<div className="inner">
        		<div className="pricing free">
        			<div className="price">7.99</div>
        			<p className="subline">When paid annually</p>
        		</div>
        		<div className="price-label">
        			<h3>Personal</h3>
        		</div>
        		<div className="feature-list single-column">
        			<ul className="unordered-list">
        				<li>1 User</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        			</ul>
        		</div>
        	</div>
			<img className="background" src="/static/img/pricing/card-2.jpg" alt="Card"/>
		</div>
		<div className="single-card large wow fadeIn fadeUp delay-100">
			<div className="inner">
        		<div className="pricing free">
        			<div className="price">9.99</div>
        			<p className="subline">When paid monlthy</p>
        		</div>
        		<div className="price-label">
        			<h3>Personal</h3>
        		</div>
        		<div className="feature-list single-column">
        			<ul className="unordered-list">
        				<li>1 User</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        				<li>Feature One</li>
        			</ul>
        		</div>
        	</div>
			<img className="background" src="/static/img/pricing/card-3.jpg" alt="Card"/>
		</div>
    </div>
    );
  }
}

export default Cards