import Link from 'next/link';

const isServer = typeof window === 'undefined'
const WOW = !isServer ? require('wow.js') : null 

class Articles extends React.Component {
	componentDidMount() {
        new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:      	75,          // default
          mobile:       true,       // default
          live:         true        // default
        }
        ).init();
    }
  	render() {
    
    return (
    <div className="article-container">
        <div className="article">
        	<div className="content">
        		<div className="half image wow fadeIn fadeUp">
        			<img className="feature" src="/static/img/articles/image-1.png" alt="Article Image"/>
        		</div>
        		<div className="half content wow fadeIn fadeUp delay-100">
	        		<h3 className="h2 title">Military Application of Performance-Enhancement Psychology</h3>
	        		<div className="excerpt">
	        			(Zinsser et al.,2004).<br/>“Performance enhancement is the deliberate cultivation of an effective perspective on achievement and the systematic use of effective cognitive skills. A soldier can maximize performance by mastering thinking habits and emotional and physical states. These training methods, derived from applied sport psychology used in training professional and Olympic athletes, are also applicable in other human performance contexts...
	        		</div>
	        	</div>
        	</div>
		</div>

		
		<div className="article reverse">
        	<div className="content">
        		<div className="half image wow fadeIn fadeUp">
        			<img className="feature" src="/static/img/articles/image-2.png" alt="Article Image"/>
        		</div>
        		<div className="half content wow fadeIn fadeUp delay-100">
	        		<h3 className="h2 title">A Psychological Success Cycle And Goal Setting: Goals, Performance, And Attitudes</h3>
	        		<div className="excerpt">
						(Hall & Foster., 2017)<br />“Setting goals initiates a success cycle, engaging goals increase your efforts, efforts increase the likelihood of success, success improves self esteem, self efficacy and desire for involvement which leads to desire to set bigger goals”	        		
                    </div>
	        	</div>
        	</div>
		</div> 

		<div className="article with-background">
        	<div className="content">
        		<div className="half image wow fadeIn fadeUp delay-100">
        			<img className="feature desktop-hide" src="/static/img/articles/image-4.png" alt="Article Image"/>
        		</div>
        		<div className="half content wow fadeIn fadeUp delay-100">
	        		<h3 className="h2 title">Psychological skills training as a way to enhance an athlete’s performance in high-intensity sports</h3>
	        		<div className="excerpt">
                        (Birrer & Morgan., 2010)<br/>
						“In today's professional and semi‐professional sports, the thin line between winning and losing is becoming progressively thinner. At the Beijing Olympic Games in 2008, the difference between first and fourth places in the men's rowing events averaged 1.34%, with the equivalent for women being a mere 1.03%. This increasing performance density creates massive pressure. Thus, it is not surprising that in recent years, the importance of psychological skills training (PST) has been recognized, and the number of athletes using psychological training strategies has increased.”
	        		</div>
	        		<a target="_blank" href="https://onlinelibrary.wiley.com/doi/full/10.1111/j.1600-0838.2010.01188.x"><button>Read more</button></a>
	        	</div> 

	        	<img className="background-image mobile-hide wow fadeIn" src="/static/img/articles/image-4.png" alt="Article Image"/>
        	</div>
		</div> 

		<div className="article">
        	<div className="content">
        		<div className="half image wow fadeIn fadeUp">
        			<img className="feature" src="/static/img/articles/image-3.png" alt="Article Image"/>
        		</div>
        		<div className="half content wow fadeIn fadeUp delay-100">
	        		<h3 className="h2 title">The relation between social identity and test anxiety in university students</h3>
	        		<div className="excerpt">
                        (Zwettler et al., 2018)<br/>
						“Membership in groups is a significant feature of human existence. Belongingness to groups is not only an external factor that influences our behavior, but it also affects our experiences and cognition. Identification with a group can provide an individual with meaning, stability, and purpose, which supports their mental well-being (Haslam et al., 2009). This means that a person’s self-concept often depends on the state of the groups that define the self (i.e. in-groups).”
	        		</div>
	        	</div>
        	</div>
		</div> 

		<div className="article">
        	<div className="content">
        		<div className="half image wow fadeIn fadeUp">
        			<img className="feature" src="/static/img/articles/image-5.jpg" alt="Article Image"/>
        		</div>
        		<div className="half content wow fadeIn fadeUp delay-100">
	        		<h3 className="h2 title">Mindfulness to Enhance Athletic Performance</h3>
	        		<div className="excerpt">
                        (Birrer & Morgan., 2012)<br/>
						“Athletes with a higher degree in mindfulness practice and dispositional mindfulness will enhance the level of several required psychological skills including attention, experiential acceptance, decision making, self-regulation, negative emotion regulation, and behavioural flexibility.”
	        		</div>
                    <a target="_blank" href="https://link.springer.com/article/10.1007/s12671-012-0109-2"><button>Read more</button></a>
	        	</div>
        	</div>
		</div> 
    </div>
    );
  }
}

export default Articles