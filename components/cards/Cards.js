import Slider from "react-slick"; 
import Link from 'next/link'; 

class Cards extends React.Component {
  render() {
    var settings = {
      dots: false,
      arrows: true, 
      infinite: true,
      speed: 1500,
      autoplay:true,
      slidesToShow: 3,
      slidesToScroll: 1,
      focusOnSelect: true,
	  autoplaySpeed:2500,
	  variableWidth: true,
	  responsive: [
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        focusOnSelect: false
	      }
	    } 
	  ]
    };
    return (
    <div className="card-container arrow-type-two">
      <Slider {...settings}>
        <div className="single-card large">
        	<div className="content">
        		<strong>Pillar No.1</strong>
        		<h3 className="h2">Performance Process</h3>
        	</div>
        	<img className="background" src="/static/img/cards/card-1.jpg" alt="Card"/>
        	<Link  href="/method?1#pillars">
        		<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
        	</Link>
		</div>
		<div className="single-card large">
			<div className="content">
	        	<strong>Pillar No.2</strong>
	        	<h3 className="h2">Physical Capital</h3>
	        </div>
			<img className="background" src="/static/img/cards/card-2.jpg" alt="Card"/>
			<Link  href="/method?2#pillars">
        		<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
        	</Link>
		</div>
		<div className="single-card large">
			<div className="content">
	        	<strong>Pillar No.3</strong>
	        	<h3 className="h2">Social Capital</h3>
	        </div>
			<img className="background" src="/static/img/cards/card-3.jpg" alt="Card"/>
			<Link  href="/method?3#pillars">
        		<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
        	</Link>
		</div>
		<div className="single-card large">
			<div className="content">
	        	<strong>Pillar No.4</strong>
	        	<h3 className="h2">Mental Factors</h3>
	        </div>
			<img className="background" src="/static/img/cards/card-4.jpg" alt="Card"/>
			<Link  href="/method?4#pillars">
        		<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
        	</Link>
		</div> 
		<div className="single-card large">
			<div className="content">
	        	<strong>Pillar No.5</strong>
	        	<h3 className="h2">Stress Management</h3>
	        </div>
			<img className="background" src="/static/img/cards/card-5.jpg" alt="Card"/>
			<Link  href="/method?5#pillars">
        		<div className="link arrow"><i className="fas fa-chevron-right"></i></div>
        	</Link>
		</div> 
      </Slider>
    </div>
    );
  }
}

export default Cards