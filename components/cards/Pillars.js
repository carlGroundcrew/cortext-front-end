import Slider from "react-slick"; 
import Link from 'next/link'; 

class Card extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {isToggleOn: false};
	    this.toggleClass = this.toggleClass.bind(this);
	}
    toggleClass() {
    	this.setState(prevState => ({
      		isToggleOn: !prevState.isToggleOn
    	}));
  	}
	render() {
	    return (
	    <div className={this.state.isToggleOn ? 'single-card large dark open' : 'single-card large dark'} onClick={this.toggleClass}>
	    	<div className="title-content">
	    		<h3 className="h3">{this.props.title}</h3>
	    	</div>
	    	<img className="background" src={this.props.background} alt="Card"/>
	    	<div className="read-more"></div>
	    	<div  className="link arrow">
	    		<i className="fas fa-chevron-right"></i>
	    	</div>
	    	<div className="overlay">
	    		<div className="inner">
	    			<div className="text">
	    				{this.props.content}
	    			</div>
	    			<span className="read-less"></span>
	    			<div  className="link arrow dark">
			    		<i className="fas fa-chevron-left"></i>
			    	</div>
	    		</div>
	    	</div>
		</div>
	   );
	}
}

class Cards extends React.Component {
  render() {
    var settings = {
      dots: false,
      arrows: true, 
      infinite: true,
      speed: 1500,
      autoplay:true,
      slidesToShow: 3,
      slidesToScroll: 1,
      focusOnSelect: false,
	  autoplaySpeed:2500,
	  variableWidth: true,
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 1,
	        speed:300,
	        slidesToScroll: 1,
	        focusOnSelect: false
	      }
	    } 
	  ]
    };
    return (
    <div className="card-container arrow-type-two pillar-carousel">
      <Slider {...settings}>
      	
      	<Card id={'1'} title={'Performance Process'} background={'/static/img/cards/1.jpg'} content={'This pillar is designed as the first step in understanding what your optimal performance looks like, conceptualise the process of getting there, and building genuine intrinsic motivation to power you through.'}/>
        
        <Card id={'2'} title={'Physical Capital'} background={'/static/img/cards/2.jpg'} content={'This pillar is learning how to capitalise on the mind body links to ensure you can deliver when the pressure is on. You can not deliver your best if you do not have the energy to take you through an average day. Step up your game.'} />

        <Card id={'3'} title={'Social Capital'} background={'/static/img/cards/3.jpg'} content={'This pillar will help you understand the impact your network and connections have on your identity, perspectives and, ultimately, performance.'} />

        <Card id={'4'} title={'Mental Factors'} background={'/static/img/cards/4.jpg'} content={'Everything you do comes from your mind. Your perspectives, your response times, your decisions - everything. This pillar teaches how to train your mind, your perspectives and your attitudes for high performance.'} />

        <Card id={'5'} title={'Stress Management'} background={'/static/img/cards/5.jpg'} content={'Stress is a fundamental part of life, and if you know how to use it - it can be your greatest ally. This pillar teaches and trains a different approach to stress - how to manage it, utilise it and embrace it.'} /> 
	
      </Slider>
    </div>
    );
  }
}

export default Cards