class Resource extends React.Component {
  render() {
    return (
    <div className={"single-card large " + this.props.style}>
          <div className="content">
            <strong className="overline">{this.props.category}</strong>
            <h3 className="h2">{this.props.title}</h3>
          </div>
          <img className="background" src={this.props.background} alt="Card"/>
          <div className="link arrow"><i className="fas fa-chevron-right"></i></div>
    </div>
   );
  }
}

class TabbedContent extends React.Component {
  render() {
  return (
      <div className="tabbed-posts"> 
        <ul  className="nav navigation-tabs container-rg">
          <li className="active">
                <a  href="#all" data-toggle="tab">All</a>
          </li>
          <li>
            <a href="#podcasts" data-toggle="tab">Podcasts</a>
          </li>
          <li>
            <a href="#books" data-toggle="tab">Books</a>
          </li>
          <li>
            <a href="#articles" data-toggle="tab">Articles</a>
          </li>
          <li>
            <a href="#studies" data-toggle="tab">Studies</a>
          </li>
        </ul>
        <div className="tab-content fadeIn fadeUp wow clearfix">
            <div className="tab-pane active" id="all">
              <div className="content container">
                <div className="card-container resources">
                  <Resource style={'dark double'} title={'Title for the resource will go here'} category={'podcast'} background={'/static/img/resources/resource-1.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'book'} background={'/static/img/resources/resource-3.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'blog'} background={'/static/img/resources/resource-2.jpg'}/>
                  <Resource style={'double'} title={'Title for the blog will go here'} category={'podcast'} background={'/static/img/resources/resource-4.jpg'}/>
                  <Resource style={'dark double'} title={'Title for the resource will go here'} category={'podcast'} background={'/static/img/resources/resource-1.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'blog'} background={'/static/img/resources/resource-2.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'blog'} background={'/static/img/resources/resource-2.jpg'}/>
                  <Resource style={'double'} title={'Title for the blog will go here'} category={'podcast'} background={'/static/img/resources/resource-4.jpg'}/>
                </div>
              </div>
            </div>
            <div className="tab-pane" id="podcasts">
              <div className="content container">
                <div className="card-container resources">
                  <Resource style={'dark double'} title={'Title for the resource will go here'} category={'podcast'} background={'/static/img/resources/resource-1.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'podcast'} background={'/static/img/resources/resource-2.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'podcast'} background={'/static/img/resources/resource-3.jpg'}/>
                  <Resource style={'double'} title={'Title for the blog will go here'} category={'podcast'} background={'/static/img/resources/resource-4.jpg'}/>
                </div>
              </div>
            </div>
            <div className="tab-pane" id="books">
              <div className="content container">
                <div className="card-container resources">
                  <Resource style={'double'} title={'Title for the blog will go here'} category={'book'} background={'/static/img/resources/resource-4.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'book'} background={'/static/img/resources/resource-3.jpg'}/>                  
                  <Resource style={''} title={'Title for the blog will go here'} category={'podcast'} background={'/static/img/resources/resource-2.jpg'}/>
                  <Resource style={'dark double'} title={'Title for the resource will go here'} category={'podcast'} background={'/static/img/resources/resource-1.jpg'}/>
                </div>
              </div>
            </div>
            <div className="tab-pane" id="articles">
              <div className="content container">
                <div className="card-container resources">
                  <Resource style={'dark double'} title={'Title for the resource will go here'} category={'articles'} background={'/static/img/resources/resource-1.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'articles'} background={'/static/img/resources/resource-3.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'podcast'} background={'/static/img/resources/resource-2.jpg'}/>                  
                  <Resource style={'dark double'} title={'Title for the resource will go here'} category={'articles'} background={'/static/img/resources/resource-1.jpg'}/>
                </div>
              </div>
            </div>
            <div className="tab-pane" id="studies">
              <div className="content container">
                <div className="card-container resources">
                  <Resource style={'dark double'} title={'Title for the resource will go here'} category={'studies'} background={'/static/img/resources/resource-1.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'studies'} background={'/static/img/resources/resource-2.jpg'}/>
                  <Resource style={''} title={'Title for the blog will go here'} category={'studies'} background={'/static/img/resources/resource-3.jpg'}/>  
                  <Resource style={'double'} title={'Title for the blog will go here'} category={'studies'} background={'/static/img/resources/resource-4.jpg'}/>                
                </div>
              </div>
            </div>
        </div>
      </div>
      );
  }
}

export default TabbedContent