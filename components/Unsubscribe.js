import React from 'react'  
import HubspotForm from 'react-hubspot-form'

const Unsubscribe = () => (
	<div>
		<div id="unsubscribe-newsletter" className="hubspotForm unsubscribe-inner">
			<h2 className="title">Are you sure you want to opt out of all email marketing communications?</h2>
			<HubspotForm
			   portalId='4267461'
			   formId='fb96819a-5324-4f41-b0d6-aeb328d73b7c'
			   onSubmit={() => console.log('Submit!')}
			   loading={<div>Loading...</div>}
			/>
		</div>
	</div>
)

export default Unsubscribe