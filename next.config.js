const withSass = require('@zeit/next-sass')
module.exports = withSass({
    publicRuntimeConfig: {
        node_env: process.env.NODE_ENV || 'development'
        // add more variables if needed
    }
})
