const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes()

routes
    .add('index', '/')
    .add('about', '/about')
    .add('blogdetail', '/blog/:slug')
    .add('blogpreview', '/blog/preview/:id/:secret')

