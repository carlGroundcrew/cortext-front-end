import {
    GET_BLOG,
    GET_BLOGS,
    GET_CATEGORIES,
    GET_SLIDER
} from '../actionTypes';

const initialState = {
    blog: {},
    blogList: [],
    categories: [],
    slider: [],
}

const blog = (state = initialState, action) => {
    // For immutability, it's a must to pass a new object every time
    switch (action.type) {
        case GET_BLOG :
            return Object.assign({}, state, {
                blog: action.data
            });
        case GET_BLOGS :
            return Object.assign({}, state, {
                blogList: action.data
            });
        case GET_CATEGORIES :
            return Object.assign({}, state, {
                categories: action.data
            });
        case GET_SLIDER :
            return Object.assign({}, state, {
                slider: action.data
            });
        default:
            return state
    }


}

export default blog;