import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';
import config from './config';

// initialise initial state for the stores
// refine if needed
const initialState = { }

export function initializeStore(initialState = initialState) {
    if (config.env === "production") { //live
        return createStore(
            rootReducer,
            initialState,
            applyMiddleware(thunk)
        );
    } else {
        return createStore(
            rootReducer,
            initialState,
            composeWithDevTools(applyMiddleware(thunk))
        );
    }
}