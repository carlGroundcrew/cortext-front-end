import getConfig from 'next/config'

const {publicRuntimeConfig} = getConfig();

let env = publicRuntimeConfig.node_env.trim();
let api_base_url = 'http://localhost:8092/cortex/cms/backend/public/api/v1/web/'; //test or local pc url
if (env === "production") { //live
    api_base_url = 'https://cms.cortexmethod.com/backend/api/v1/web/'; //live server url
}

const config = {

    // api key for the api
    api_key : 'UjNRNeGyyN5K613CwCUYv9G4rD5EzLK1',

    env: env,

    api_base_url : api_base_url,

    //APIS
    getCategories: 'categories',

    getTopics: 'topics',

    getBlogs: 'blogs',
    viewBlog: 'blog/view',
    previewBlog: 'blog/view-preview',
};

export default config;