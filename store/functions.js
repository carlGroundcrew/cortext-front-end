
export function getCategoryNames(categories) {
    let cat = '';
    for (let i = 0; i < categories.length; i++) {
        if (i === 0) {
            cat += categories[i].name;
        } else {
            cat += ', ' + categories[i].name;
        }
    }
    return cat;
}


export function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}