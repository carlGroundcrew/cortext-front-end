import {
    getBlog,
    getBlogs, getPreviewBlog,
} from '../service';

import {
    GET_CATEGORIES,
    GET_BLOGS,
    GET_BLOG,
    GET_SLIDER
} from '../actionTypes';

export const fetchBlogs = ($filters) => async dispatch => {
    dispatch({
        type: GET_BLOGS,
        data: [],
    })
    dispatch({
        type: GET_CATEGORIES,
        data: [],
    })
    dispatch({
        type: GET_SLIDER,
        data: [],
    })
    const res = await getBlogs($filters);
    try {
        if (res.success) {
            dispatch({
                type: GET_BLOGS,
                data: res.data.list,
            })
            dispatch({
                type: GET_CATEGORIES,
                data: res.data.categories,
            })
            dispatch({
                type: GET_SLIDER,
                data: res.data.slider,
            })
        }
    } catch (err) {

    }
}

export const fetchBlog = ($filters) => async dispatch => {
    dispatch({
        type: GET_BLOG,
        data: {},
    })
    const res = await getBlog($filters);
    try {
        if (res.success) {
            dispatch({
                type: GET_BLOG,
                data: res.data.blog,
            })
        }
    } catch (err) {

    }
}

export const fetchPreviewBlog = ($filters) => async dispatch => {
    dispatch({
        type: GET_BLOG,
        data: {},
    })
    const res = await getPreviewBlog($filters);
    try {
        if (res.success) {
            dispatch({
                type: GET_BLOG,
                data: res.data.blog,
            })
        }
    } catch (err) {

    }
}