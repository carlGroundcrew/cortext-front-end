import config from './config';

const axios = require('axios');


export function getHeaders() {
    return {
        headers: {
            ApiKey: config.api_key,
        }
    };
}


//base service call
export const callService = (url, postData, headers) => {
    return axios.post(config.api_base_url + url, postData, headers)
        .then(function (response) {
            //redirect to the login screen if api token is invalid
            if (!response.data.success) {
                if (response.data.error) {
                    if (typeof response.data.error !== 'undefined' && typeof response.data.error.code !== 'undefined' &&
                        response.data.error.code === 401
                    ) {
                        console.log(response.data.error);
                    }
                }
            }
            // handle success
            return response.data;
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            return false;
        });
}

//get categories
export async function getCategories() {
    return callService(config.getCategories, {}, getHeaders());
}

//get all blog
export async function getBlogs(postData) {
    return callService(config.getBlogs, postData, getHeaders());
}

//view blog
export async function getBlog(postData) {
    return callService(config.viewBlog, postData, getHeaders());
}

//preview blog
export async function getPreviewBlog(postData) {
    return callService(config.previewBlog, postData, getHeaders());
}