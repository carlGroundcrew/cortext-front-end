import fetch from 'isomorphic-fetch'

export function getPosts () {
  return fetch('https://my-json-server.typicode.com/S4259328/demo/posts')
}

export function getPost (slug) {
  return fetch(`https://my-json-server.typicode.com/S4259328/demo/posts?id=${slug}`)
}
